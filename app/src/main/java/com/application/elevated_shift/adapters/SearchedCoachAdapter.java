package com.application.elevated_shift.adapters;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.elevated_shift.R;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.ui.activities.CoachProfileActivity;
import com.application.elevated_shift.ui.activities.ProfileActivity;
import com.application.elevated_shift.ui.activities.UserProfileActivity;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class SearchedCoachAdapter extends RecyclerView.Adapter<SearchedCoachAdapter.ViewHolder> {

    private List<Data> mList;

    public SearchedCoachAdapter(List<Data> mList) {

        this.mList = mList;
    }

    @NotNull
    @Override
    public SearchedCoachAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.searched_coach_item, viewGroup, false);

        return new ViewHolder(view);
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onBindViewHolder(@NotNull SearchedCoachAdapter.ViewHolder viewHolder, int position) {

        viewHolder.name.setText(mList.get(position).getName());

        Picasso.with(viewHolder.image.getContext())
                .load(mList.get(position).getDisplayImage())
                .placeholder(R.drawable.placeholder)
                .centerCrop()
                .resize(100, 100)
                .into(viewHolder.image);

        HashtagAdapter hashtagAdapter = new HashtagAdapter(mList.get(position).getCoachHashes());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(viewHolder.image.getContext(), LinearLayoutManager.HORIZONTAL, false);
        viewHolder.hashes.setLayoutManager(linearLayoutManager);
        viewHolder.hashes.setAdapter(hashtagAdapter);

        viewHolder.llUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mList.get(position).getId().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId())) {
                    if (!mList.get(position).getCoach()) {
                        viewHolder.image.getContext().startActivity(new Intent(viewHolder.image.getContext(), ProfileActivity.class)
                                .putExtra("user", mList.get(position)));
                    } else {
                        viewHolder.image.getContext().startActivity(new Intent(viewHolder.image.getContext(), CoachProfileActivity.class)
                                .putExtra("coach", mList.get(position)));
                    }
                } else {
                    viewHolder.image.getContext().startActivity(new Intent(viewHolder.image.getContext(), UserProfileActivity.class));
                }
            }
        });

    }

    public Data getItemView(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        ImageView image;
        LinearLayout llUser;
        RecyclerView hashes;

        public ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.text2);
            image = itemView.findViewById(R.id.dp);
            llUser = itemView.findViewById(R.id.ll_chat);
            hashes = itemView.findViewById(R.id.hashes);

        }
    }

    public void updateList(List<Data> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}
