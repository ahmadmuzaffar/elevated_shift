package com.application.elevated_shift.adapters;

import android.app.Activity;
import android.text.Html;
import android.text.Spannable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.elevated_shift.R;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.models.collection.Story;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.sharedPreference.SharedPreferencesEditor;
import com.application.elevated_shift.viewModels.activitesViewModels.MyStoriesViewModel;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;


public class StoriesAdapter extends RecyclerSwipeAdapter<StoriesAdapter.ViewHolder> {

    List<Story> mList;
    Activity activity;
    private SwipeLayout.SwipeListener swipeListener = null;

    public StoriesAdapter(List<Story> mList, Activity activity) {

        this.mList = mList;
        this.activity = activity;
    }

    @Override
    public StoriesAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.stories_item, viewGroup, false);

        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(StoriesAdapter.ViewHolder viewHolder, int position) {

        if (position == 0) {
            if (!new SharedPreferencesEditor(activity).isTutorialViewed(Constants.STORIES_ITEM)) {

                swipeListener = new SwipeLayout.SwipeListener() {
                    @Override
                    public void onStartOpen(SwipeLayout layout) {

                    }

                    @Override
                    public void onOpen(SwipeLayout layout) {
                        new GuideView.Builder(activity)
                                .setTitle("Delete")
                                .setContentSpan((Spannable) Html.fromHtml("<font color='red'>Tap on the trash button to delete this chat</p>"))
                                .setGravity(Gravity.center)
                                .setTargetView(viewHolder.delete)
                                .setDismissType(DismissType.outside)
                                .setGuideListener(new GuideListener() {
                                    @Override
                                    public void onDismiss(View view) {
                                        viewHolder.swipe.close();
                                        viewHolder.swipe.removeSwipeListener(swipeListener);
                                        new SharedPreferencesEditor(activity).setTutorialViewed(Constants.STORIES_ITEM, true);
                                    }
                                })
                                .build()
                                .show();
                    }

                    @Override
                    public void onStartClose(SwipeLayout layout) {

                    }

                    @Override
                    public void onClose(SwipeLayout layout) {

                    }

                    @Override
                    public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

                    }

                    @Override
                    public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

                    }
                };

                new GuideView.Builder(activity)
                        .setTitle("Left Swipe")
                        .setContentSpan((Spannable) Html.fromHtml("<font color='#0B304F'>Swipe left the container to view actions</p>"))
                        .setGravity(Gravity.center)
                        .setTargetView(viewHolder.rootContainer)
                        .setDismissType(DismissType.outside)
                        .setGuideListener(new GuideListener() {
                            @Override
                            public void onDismiss(View view) {
                                viewHolder.swipe.open();
                                viewHolder.swipe.addSwipeListener(swipeListener);
                            }
                        })
                        .build()
                        .show();
            }
        }

        viewHolder.username.setText(mList.get(position).getUser().getName());
        viewHolder.message.setText(mList.get(position).getStoryText());

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(Constants.FORMAT_DATE_TIME, Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(mList.get(position).getCreatedAt());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(TimeZone.getDefault());
        String formattedDate = df.format(Objects.requireNonNull(date));

        try {
            calendar.setTime(new SimpleDateFormat(Constants.FORMAT_DATE_TIME).parse(formattedDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.date.setText("Date: " + new SimpleDateFormat("hh:mm-dd/MM").format(calendar.getTime()));

        Picasso.with(viewHolder.picture.getContext())
                .load(mList.get(position).getUser().getDisplayImage())
                .error(R.drawable.placeholder)
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .resize(100, 100)
                .into(viewHolder.picture);

        if (TextUtils.isEmpty(mList.get(position).getImageUrl())) {
            viewHolder.storyPicture.setVisibility(View.GONE);
            viewHolder.storyPicture.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.placeholder));
        } else {
            Picasso.with(viewHolder.storyPicture.getContext())
                    .load(mList.get(position).getImageUrl())
                    .placeholder(R.drawable.placeholder)
                    .centerCrop()
                    .resize(400, 400)
                    .into(viewHolder.storyPicture);
            viewHolder.storyPicture.setVisibility(View.VISIBLE);
        }

        viewHolder.delete.setOnClickListener(v -> {

            new ServerHelper(activity).deleteStory(mList.get(position).getId(), new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        MyStoriesViewModel.instance.setAdapter();
                    }
                }
            });

        });

        viewHolder.swipe.setShowMode(SwipeLayout.ShowMode.PullOut);
        viewHolder.swipe.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipe.findViewById(R.id.bottom_wraper));

        mItemManger.bindView(viewHolder.itemView, position);

    }

    public Story getItemView(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView username, message, date;
        ImageView picture, delete, storyPicture;
        SwipeLayout swipe;
        LinearLayout llStory, rootContainer;

        public ViewHolder(View itemView) {
            super(itemView);

            username = itemView.findViewById(R.id.storyUsername);
            date = itemView.findViewById(R.id.date);
            message = itemView.findViewById(R.id.message);
            picture = itemView.findViewById(R.id.picture);
            storyPicture = itemView.findViewById(R.id.storyPicture);
            delete = itemView.findViewById(R.id.Delete);
            swipe = itemView.findViewById(R.id.swipe);
            llStory = itemView.findViewById(R.id.llStory);
            rootContainer = itemView.findViewById(R.id.rootContainer);

        }
    }

    public void updateList(List<Story> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}
