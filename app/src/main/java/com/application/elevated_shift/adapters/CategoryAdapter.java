package com.application.elevated_shift.adapters;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.ui.activities.CoachProfileActivity;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;


public class CategoryAdapter extends RecyclerSwipeAdapter<CategoryAdapter.ViewHolder> {

    List<Data> mList;
    Activity activity;

    public CategoryAdapter(List<Data> mList, Activity activity) {

        this.mList = mList;
        this.activity = activity;
    }

    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chats_item, viewGroup, false);

        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(CategoryAdapter.ViewHolder viewHolder, int position) {

        viewHolder.text.setText(mList.get(position).getName());
        if (!TextUtils.isEmpty(mList.get(position).getDisplayImage())) {
            Picasso.with(activity)
                    .load(mList.get(position).getDisplayImage())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .resize(100, 100)
                    .into(viewHolder.dp);
        } else {
            viewHolder.dp.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_upload_image_foreground));
        }

        viewHolder.ll_chat.setOnClickListener(v -> {
            if (!mList.get(position).getId().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId())) {
                activity.startActivity(new Intent(activity, CoachProfileActivity.class)
                        .putExtra("coach", mList.get(position)));
            } else
                Toast.makeText(activity, "Can not open your profile as coach", Toast.LENGTH_SHORT).show();
        });

//        viewHolder.swipe.setShowMode(SwipeLayout.ShowMode.PullOut);
//        viewHolder.swipe.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipe.findViewById(R.id.bottom_wraper));

        mItemManger.bindView(viewHolder.itemView, position);

    }

    public Data getItemView(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView text;
        SwipeLayout swipe;
        LinearLayout ll_chat;
        ImageView dp;

        public ViewHolder(View itemView) {
            super(itemView);

            text = itemView.findViewById(R.id.text2);
            ll_chat = itemView.findViewById(R.id.ll_chat);
            swipe = itemView.findViewById(R.id.swipe);
            dp = itemView.findViewById(R.id.dp);

        }
    }

    public void updateList(List<Data> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}
