package com.application.elevated_shift.adapters;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.application.elevated_shift.R;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.feedback.Comment;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.ui.activities.CoachProfileActivity;
import com.application.elevated_shift.ui.activities.ProfileActivity;
import com.application.elevated_shift.ui.activities.UserProfileActivity;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import androidx.recyclerview.widget.RecyclerView;


public class CommentsAdapter extends RecyclerSwipeAdapter<CommentsAdapter.ViewHolder> {

    public static CommentsAdapter instance;
    public Data data;
    List<Comment> mList;
    Activity activity;

    public CommentsAdapter(List<Comment> mList, Activity activity) {

        instance = this;
        this.mList = mList;
        this.activity = activity;
    }

    @Override
    public CommentsAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comment_item, viewGroup, false);

        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(CommentsAdapter.ViewHolder viewHolder, int position) {

        viewHolder.name.setText(mList.get(position).getCommentor().getName());
        viewHolder.text.setText(mList.get(position).getCommentText());

        Picasso.with(viewHolder.dp.getContext())
                .load(mList.get(position).getCommentor().getDisplayImage())
                .placeholder(R.drawable.placeholder)
                .centerCrop()
                .resize(100, 100)
                .into(viewHolder.dp);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(Constants.FORMAT_DATE_TIME, Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(mList.get(position).getCreatedAt());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(TimeZone.getDefault());
        String formattedDate = df.format(Objects.requireNonNull(date));
        try {
            calendar.setTime(Objects.requireNonNull(new SimpleDateFormat(Constants.FORMAT_DATE_TIME).parse(formattedDate)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.date.setText(new SimpleDateFormat("hh:mm - dd/MM").format(calendar.getTime()));

        viewHolder.dp.setOnClickListener(v -> {
            if (!mList.get(position).getCommentor().getId().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId())) {
                if (!mList.get(position).getCommentor().getCoach()) {
                    viewHolder.dp.getContext().startActivity(new Intent(viewHolder.dp.getContext(), ProfileActivity.class)
                            .putExtra("user", mList.get(position).getCommentor()));
                } else {
                    viewHolder.dp.getContext().startActivity(new Intent(viewHolder.dp.getContext(), CoachProfileActivity.class)
                            .putExtra("coach", mList.get(position).getCommentor()));
                }
            } else {
                viewHolder.dp.getContext().startActivity(new Intent(viewHolder.dp.getContext(), UserProfileActivity.class));
            }
        });

        mItemManger.bindView(viewHolder.itemView, position);

    }

    public Comment getItemView(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView text, name, date;
        ImageView dp;

        public ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            text = itemView.findViewById(R.id.comment);
            date = itemView.findViewById(R.id.date);
            dp = itemView.findViewById(R.id.dp);

        }
    }

    public void updateList(List<Comment> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}
