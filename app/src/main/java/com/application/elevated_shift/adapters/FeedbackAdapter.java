package com.application.elevated_shift.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.elevated_shift.R;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.models.feedback.Datum;
import com.application.elevated_shift.ui.activities.MyFeedbackDetailActivity;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import androidx.recyclerview.widget.RecyclerView;

public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolder> {

    List<Datum> mList;
    Activity activity;

    public FeedbackAdapter(List<Datum> mList, Activity activity) {

        this.mList = mList;
        this.activity = activity;
    }

    @Override
    public FeedbackAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.feedback_item, viewGroup, false);

        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onBindViewHolder(FeedbackAdapter.ViewHolder viewHolder, int position) {

        viewHolder.username.setText(mList.get(position).getFeedbackGivenBy().getName());
        viewHolder.message.setText(mList.get(position).getFeedbackText());
        String dateStr = mList.get(position).getCreatedAt();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(Constants.FORMAT_DATE_TIME, Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(TimeZone.getDefault());
        String formattedDate = df.format(Objects.requireNonNull(date));
        try {
            calendar.setTime(Objects.requireNonNull(new SimpleDateFormat(Constants.FORMAT_DATE_TIME).parse(formattedDate)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.date.setText(new SimpleDateFormat("hh:mm - dd/MM").format(calendar.getTime()));

        Picasso.with(viewHolder.picture.getContext())
                .load(mList.get(position).getFeedbackGivenBy().getDisplayImage())
                .placeholder(R.drawable.placeholder)
                .centerCrop()
                .resize(100, 100)
                .into(viewHolder.picture);

        if (!TextUtils.isEmpty(mList.get(position).getFeedbackImageUrl())) {
            viewHolder.feedbackPicture.setVisibility(View.VISIBLE);
            Picasso.with(viewHolder.feedbackPicture.getContext())
                    .load(mList.get(position).getFeedbackImageUrl())
                    .resize(500, 500)
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .into(viewHolder.feedbackPicture);
        } else {
            viewHolder.feedbackPicture.setVisibility(View.GONE);
        }

        viewHolder.llChat.setOnClickListener(v -> {
            viewHolder.llChat.getContext().startActivity(new Intent(viewHolder.llChat.getContext(), MyFeedbackDetailActivity.class)
                    .putExtra("feedback", mList.get(position)));
        });

    }

    public Datum getItemView(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView username, message, date;
        ImageView picture, feedbackPicture;
        LinearLayout llChat;

        public ViewHolder(View itemView) {
            super(itemView);

            username = itemView.findViewById(R.id.username);
            date = itemView.findViewById(R.id.date);
            message = itemView.findViewById(R.id.feedbackMessage);
            picture = itemView.findViewById(R.id.dp);
            feedbackPicture = itemView.findViewById(R.id.feedbackPicture);
            llChat = itemView.findViewById(R.id.ll_chat);

        }
    }

    public void updateList(List<Datum> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}
