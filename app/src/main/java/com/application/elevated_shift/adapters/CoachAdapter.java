package com.application.elevated_shift.adapters;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.ui.activities.CoachProfileActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;


public class CoachAdapter extends RecyclerView.Adapter<CoachAdapter.ViewHolder> {

    private List<Data> mList;
    private Activity activity;

    public CoachAdapter(List<Data> mList, Activity activity) {

        this.mList = mList;
        this.activity = activity;
    }

    @Override
    public CoachAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.coach_item, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CoachAdapter.ViewHolder viewHolder, int position) {

        viewHolder.name.setText(mList.get(position).getName());
        Picasso.with(viewHolder.picture.getContext())
                .load(mList.get(position).getDisplayImage())
                .placeholder(R.drawable.placeholder)
                .resize(200, 200)
                .centerCrop()
                .into(viewHolder.picture);

        viewHolder.coach.setOnClickListener(v -> {
            if (mList.get(position).getId().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId())) {
                Toast.makeText(activity, "Can not open your profile as a coach", Toast.LENGTH_SHORT).show();
                return;
            }
            activity.startActivity(new Intent(activity, CoachProfileActivity.class).putExtra("coach", mList.get(position)));
        });

    }

    public Data getItemView(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        ImageView picture;
        RelativeLayout coach;

        public ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            picture = itemView.findViewById(R.id.dp);
            coach = itemView.findViewById(R.id.coach);

        }
    }

    public void updateList(List<Data> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}
