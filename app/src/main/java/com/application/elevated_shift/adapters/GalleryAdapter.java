package com.application.elevated_shift.adapters;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.application.elevated_shift.R;
import com.application.elevated_shift.ui.activities.PhotoViewerActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;


public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    private List<String> mList;

    public GalleryAdapter(List<String> mList) {

        this.mList = mList;
    }

    @Override
    public GalleryAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gallery_item, viewGroup, false);

        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onBindViewHolder(GalleryAdapter.ViewHolder viewHolder, int position) {

        if (!TextUtils.isEmpty(mList.get(position))) {
            Picasso.with(viewHolder.image.getContext())
                    .load(mList.get(position))
                    .fit()
                    .into(viewHolder.image);
        } else {
            viewHolder.image.setImageDrawable(viewHolder.image.getContext().getResources().getDrawable(R.drawable.ic_upload_image_foreground));
        }

        viewHolder.image.setOnClickListener(v -> {
            viewHolder.image.getContext().startActivity(new Intent(viewHolder.image.getContext(), PhotoViewerActivity.class).putExtra("image", mList.get(position)));
        });

    }

    public String getItemView(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);

        }
    }

    public void updateList(List<String> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}