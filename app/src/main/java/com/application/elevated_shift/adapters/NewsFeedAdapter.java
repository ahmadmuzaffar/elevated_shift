package com.application.elevated_shift.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.text.Spannable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.Story;
import com.application.elevated_shift.models.collection.CollectionsResponse;
import com.application.elevated_shift.models.story.Datum;
import com.application.elevated_shift.models.story.Likes;
import com.application.elevated_shift.models.story.SingleStory;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.sharedPreference.SharedPreferencesEditor;
import com.application.elevated_shift.ui.activities.CoachProfileActivity;
import com.application.elevated_shift.ui.activities.PhotoViewerActivity;
import com.application.elevated_shift.ui.activities.ProfileActivity;
import com.application.elevated_shift.ui.activities.UserProfileActivity;
import com.daimajia.swipe.SwipeLayout;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;


public class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.ViewHolder> {

    public static NewsFeedAdapter instance;
    private List<Datum> mList;

    private Socket mSocket;
    private boolean isLiked;
    private boolean isShared;
    private Activity activity;
    private ArrayList<Object> collectionList;
    public CollectionsResponse collectionsResponse;
    public SingleStory singleStory;
    private String collectionId;
    private String storyText;
    private String storyId;

    {
        try {
            mSocket = IO.socket("https://gymbe.herokuapp.com/");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public NewsFeedAdapter(List<Datum> mList, Activity activity) {

        instance = this;
        this.activity = activity;
        mSocket.connect();
        this.mList = mList;
    }

    @NotNull
    @Override
    public NewsFeedAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_feed_item, viewGroup, false);

        return new ViewHolder(view);
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onBindViewHolder(NewsFeedAdapter.ViewHolder viewHolder, int position) {

        viewHolder.likePost.setText(mList.get(position).getLikes().size() + "");
        viewHolder.sharePost.setText(mList.get(position).getShares().size() + "");

        viewHolder.username.setText(mList.get(position).getUser().getFirstName() + " " + mList.get(position).getUser().getLastName());
        if (mList.get(position).getStoryText().contains("(shared)")) {

            viewHolder.sharePost.setVisibility(View.GONE);
            viewHolder.seperator.setVisibility(View.GONE);

            storyText = mList.get(position).getStoryText().replace("(shared)", "");
            storyId = storyText.substring(0, 24);
            storyText = storyText.replace(storyId, "");

            new ServerHelper(activity).getStory(storyId, new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        viewHolder.llStoryShared.setVisibility(View.VISIBLE);
                        viewHolder.storyUsernameShared.setText(singleStory.getData().getUser().getName());
                        viewHolder.message.setText(storyText);
                        viewHolder.messageShared.setText(singleStory.getData().getStoryText());

                        viewHolder.pictureShared.setOnClickListener(v -> {
                            if (!singleStory.getData().getUser().getId().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId())) {
                                if (!mList.get(position).getUser().getCoach()) {
                                    viewHolder.pictureShared.getContext().startActivity(new Intent(viewHolder.pictureShared.getContext(), ProfileActivity.class)
                                            .putExtra("user", singleStory.getData().getUser()));
                                } else {
                                    viewHolder.pictureShared.getContext().startActivity(new Intent(viewHolder.pictureShared.getContext(), CoachProfileActivity.class)
                                            .putExtra("coach", singleStory.getData().getUser()));
                                }
                            } else {
                                viewHolder.pictureShared.getContext().startActivity(new Intent(viewHolder.pictureShared.getContext(), UserProfileActivity.class));
                            }
                        });

                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat(Constants.FORMAT_DATE_TIME, Locale.ENGLISH);
                        df.setTimeZone(TimeZone.getTimeZone("UTC"));
                        Date date = null;

                        try {
                            date = df.parse(singleStory.getData().getCreatedAt());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        df.setTimeZone(TimeZone.getDefault());
                        String formattedDate = df.format(Objects.requireNonNull(date));

                        try {
                            calendar.setTime(Objects.requireNonNull(new SimpleDateFormat(Constants.FORMAT_DATE_TIME).parse(formattedDate)));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        viewHolder.dateShared.setText(new SimpleDateFormat("hh:mm-dd/MM").format(calendar.getTime()));

                        Picasso.with(activity)
                                .load(mList.get(position).getUser().getDisplayImage())
                                .placeholder(R.drawable.placeholder)
                                .centerCrop()
                                .resize(100, 100)
                                .into(viewHolder.image);

                        Picasso.with(activity)
                                .load(singleStory.getData().getUser().getDisplayImage())
                                .placeholder(R.drawable.placeholder)
                                .centerCrop()
                                .resize(100, 100)
                                .into(viewHolder.pictureShared);

                        if (!TextUtils.isEmpty(singleStory.getData().getImageUrl())) {
                            viewHolder.storyPictureShared.setVisibility(View.VISIBLE);
                            Picasso.with(activity)
                                    .load(singleStory.getData().getImageUrl())
                                    .placeholder(R.drawable.placeholder)
                                    .centerCrop()
                                    .resize(500, 500)
                                    .into(viewHolder.storyPictureShared);
                        } else {
                            viewHolder.storyPictureShared.setVisibility(View.GONE);
                            viewHolder.storyPictureShared.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.placeholder));
                        }

                        viewHolder.storyPictureShared.setOnClickListener(v -> {
                            viewHolder.storyPictureShared.getContext().startActivity(new Intent(viewHolder.storyPictureShared.getContext(), PhotoViewerActivity.class).putExtra("image", singleStory.getData().getImageUrl()));
                        });
                    }
                }
            });

        } else
            viewHolder.message.setText(mList.get(position).getStoryText());

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(Constants.FORMAT_DATE_TIME, Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;

        try {
            date = df.parse(mList.get(position).getCreatedAt());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        df.setTimeZone(TimeZone.getDefault());
        String formattedDate = df.format(Objects.requireNonNull(date));

        try {
            calendar.setTime(Objects.requireNonNull(new SimpleDateFormat(Constants.FORMAT_DATE_TIME).parse(formattedDate)));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        viewHolder.date.setText(new SimpleDateFormat("hh:mm-dd/MM").format(calendar.getTime()));

        Picasso.with(activity)
                .load(mList.get(position).getUser().getDisplayImage())
                .placeholder(R.drawable.placeholder)
                .centerCrop()
                .resize(100, 100)
                .into(viewHolder.image);

        if (!TextUtils.isEmpty(mList.get(position).getImageUrl())) {
            viewHolder.picture.setVisibility(View.VISIBLE);

            Picasso.with(viewHolder.picture.getContext())
                    .load(mList.get(position).getImageUrl())
                    .placeholder(R.drawable.placeholder)
                    .centerCrop()
                    .resize(500, 500)
                    .into(viewHolder.picture);
        } else {
            viewHolder.picture.setVisibility(View.GONE);
            viewHolder.picture.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.placeholder));
        }

        viewHolder.picture.setOnClickListener(v -> {
            viewHolder.picture.getContext().startActivity(new Intent(viewHolder.picture.getContext(), PhotoViewerActivity.class).putExtra("image", mList.get(position).getImageUrl()));
        });

        viewHolder.image.setOnClickListener(v -> {
            if (!mList.get(position).getUser().getId().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId())) {
                if (!mList.get(position).getUser().getCoach()) {
                    viewHolder.image.getContext().startActivity(new Intent(viewHolder.image.getContext(), ProfileActivity.class)
                            .putExtra("user", mList.get(position).getUser()));
                } else {
                    viewHolder.image.getContext().startActivity(new Intent(viewHolder.image.getContext(), CoachProfileActivity.class)
                            .putExtra("coach", mList.get(position).getUser()));
                }
            } else {
                viewHolder.image.getContext().startActivity(new Intent(viewHolder.image.getContext(), UserProfileActivity.class));
            }
        });

        viewHolder.likePost.setOnClickListener(v -> {
            isLiked = false;
            List<String> storyLikes = mList.get(position).getLikes();
            if (!storyLikes.contains(Globals.getUsage().sharedPreferencesEditor.getUser().getId())) {
                storyLikes.add(Globals.getUsage().sharedPreferencesEditor.getUser().getId());
                isLiked = true;
            } else {
                storyLikes.remove(Globals.getUsage().sharedPreferencesEditor.getUser().getId());
            }

            Likes likes = new Likes();
            likes.setLikeFrom(Globals.getUsage().sharedPreferencesEditor.getUser());
            likes.setLikeTo(mList.get(position).getUser());
            likes.setItemPosition(position);
            HashMap<String, List<String>> map = new HashMap<>();
            map.put("likes", storyLikes);
            new ServerHelper(viewHolder.likePost.getContext()).updateStory(map, mList.get(position).getId(), new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        if (isLiked)
                            mSocket.emit("like", new Gson().toJson(likes));
                        viewHolder.likePost.setText(storyLikes.size() + "");
                    }
                }
            });
        });

        viewHolder.sharePost.setOnClickListener(v -> {
            showDialog(position);
        });


    }

    public Datum getItemView(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView username, message, date, storyUsernameShared, dateShared, messageShared, likePost, sharePost;
        ImageView image, picture, pictureShared, storyPictureShared;
        LinearLayout llStoryShared;
        View seperator;

        public ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.picture);
            pictureShared = itemView.findViewById(R.id.pictureShared);
            date = itemView.findViewById(R.id.date);
            dateShared = itemView.findViewById(R.id.dateShared);
            picture = itemView.findViewById(R.id.storyPicture);
            storyPictureShared = itemView.findViewById(R.id.storyPictureShared);
            username = itemView.findViewById(R.id.storyUsername);
            storyUsernameShared = itemView.findViewById(R.id.storyUsernameShared);
            message = itemView.findViewById(R.id.message);
            messageShared = itemView.findViewById(R.id.messageShared);
            llStoryShared = itemView.findViewById(R.id.llStoryShared);
            likePost = itemView.findViewById(R.id.likePost);
            sharePost = itemView.findViewById(R.id.sharePost);
            seperator = itemView.findViewById(R.id.seperator);

        }
    }

    public void updateList(List<Datum> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

    private void showDialog(int position) {
        final Dialog d = new Dialog(activity);
        d.setContentView(R.layout.dialog_share_post);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setCancelable(false);
        d.setCanceledOnTouchOutside(false);

        WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();
        lWindowParams.copyFrom(d.getWindow().getAttributes());
        lWindowParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        d.show();
        d.getWindow().setAttributes(lWindowParams);

        TextView post, cancel;
        EditText message;
        MaterialSpinner collections;

        collections = d.findViewById(R.id.collection);
        message = d.findViewById(R.id.message);
        post = d.findViewById(R.id.okay);
        cancel = d.findViewById(R.id.cancel);

        new ServerHelper(activity).getCollections(new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                collectionList = new ArrayList<>();
                collectionList.add("Select Collection");
                for (com.application.elevated_shift.models.collection.Datum datum : collectionsResponse.getData()) {
                    if (datum.getUser().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId()))
                        collectionList.add(datum.getTitle());
                }
                collections.setItems(collectionList);
            }
        });

        collections.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                for (com.application.elevated_shift.models.collection.Datum datum : collectionsResponse.getData()) {
                    if (datum.getUser().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId()))
                        if (datum.getTitle().equals(collections.getText().toString())) {
                            collectionId = datum.getId();
                            break;
                        }
                }
            }
        });

        post.setOnClickListener(v -> {

            if (collections.getText().toString().equals("Select Collection") || collectionId.equals("")) {
                Toast.makeText(activity, "Please select collection from drop down or add it in collections page", Toast.LENGTH_SHORT).show();
                return;
            }
            if (message.length() == 0) {
                Toast.makeText(activity, "Please enter message to post story", Toast.LENGTH_SHORT).show();
                return;
            }

            isShared = false;
            List<String> storyShares = mList.get(position).getShares();
            if (!storyShares.contains(Globals.getUsage().sharedPreferencesEditor.getUser().getId())) {
                storyShares.add(Globals.getUsage().sharedPreferencesEditor.getUser().getId());
                isShared = true;
//                for (Drawable drawable : viewHolder.likePost.getCompoundDrawables()) {
//                    if (drawable != null) {
//                        drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(viewHolder.likePost.getContext(), R.color.colorLink), PorterDuff.Mode.SRC_IN));
//                    }
//                }
//                viewHolder.likePost.setTextColor(viewHolder.likePost.getContext().getResources().getColor(R.color.colorLink));
            }

            Likes likes = new Likes();
            likes.setLikeFrom(Globals.getUsage().sharedPreferencesEditor.getUser());
            likes.setLikeTo(mList.get(position).getUser());
            likes.setItemPosition(position);
            HashMap<String, List<String>> map = new HashMap<>();
            map.put("shares", storyShares);
            new ServerHelper(activity).updateStory(map, mList.get(position).getId(), new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        Story story = new Story();
                        story.setStoryText("(shared)" + mList.get(position).getId() + message.getText().toString());
                        story.setCollectionId(collectionId);
                        new ServerHelper(activity).saveStory(story, new HandlerDataSync() {
                            @Override
                            public void onSyncSuccessful(boolean success, boolean isDataError) {
                                if (success) {
                                    if (isShared)
                                        mSocket.emit("share", new Gson().toJson(likes));
                                }
                            }
                        });
                    }
                }
            });
            d.dismiss();
        });

        cancel.setOnClickListener(v -> {
            d.dismiss();
        });
    }

}
