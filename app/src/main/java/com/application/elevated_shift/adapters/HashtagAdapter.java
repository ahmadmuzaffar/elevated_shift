package com.application.elevated_shift.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.application.elevated_shift.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;


public class HashtagAdapter extends RecyclerView.Adapter<HashtagAdapter.ViewHolder> {

    private List<String> mList;

    public HashtagAdapter(List<String> mList) {

        this.mList = mList;
    }

    @NotNull
    @Override
    public HashtagAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hashes_item, viewGroup, false);

        return new ViewHolder(view);
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onBindViewHolder(@NotNull HashtagAdapter.ViewHolder viewHolder, int position) {

        viewHolder.textView.setText(mList.get(position));

    }

    public String getItemView(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.hash);

        }
    }

    public void updateList(List<String> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}
