package com.application.elevated_shift.adapters;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.elevated_shift.R;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.chat.Message;
import com.application.elevated_shift.ui.activities.PhotoViewerActivity;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    private List<Message> mList;

    public ChatAdapter(List<Message> mList) {

        this.mList = mList;
    }

    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chat_item, viewGroup, false);

        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onBindViewHolder(ChatAdapter.ViewHolder viewHolder, int position) {

        if (!mList.get(position).getUser().getId().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId())) {
            viewHolder.textLayout.setGravity(Gravity.LEFT);
            viewHolder.pictureLayout.setGravity(Gravity.LEFT);
            viewHolder.llText.setCardBackgroundColor(viewHolder.llText.getContext().getResources().getColor(R.color.colorTabBack));
            viewHolder.llPicture.setCardBackgroundColor(viewHolder.llPicture.getContext().getResources().getColor(R.color.colorTabBack));
        } else {
            viewHolder.textLayout.setGravity(Gravity.END);
            viewHolder.pictureLayout.setGravity(Gravity.END);
            viewHolder.llText.setCardBackgroundColor(viewHolder.llText.getContext().getResources().getColor(R.color.colorBlack));
            viewHolder.llPicture.setCardBackgroundColor(viewHolder.llPicture.getContext().getResources().getColor(R.color.colorBlack));
        }

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(Constants.FORMAT_DATE_TIME, Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(mList.get(position).getCreatedAt());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(TimeZone.getDefault());
        String formattedDate = df.format(Objects.requireNonNull(date));
        try {
            calendar.setTime(Objects.requireNonNull(new SimpleDateFormat(Constants.FORMAT_DATE_TIME).parse(formattedDate)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (mList.get(position).getType().equals("text")) {
            viewHolder.textLayout.setVisibility(View.VISIBLE);
            viewHolder.pictureLayout.setVisibility(View.GONE);
            viewHolder.text.setText(mList.get(position).getText());
            viewHolder.date1.setText(new SimpleDateFormat("hh:mm-dd/MM").format(calendar.getTime()));
        } else {
            viewHolder.pictureLayout.setVisibility(View.VISIBLE);
            viewHolder.textLayout.setVisibility(View.GONE);
            viewHolder.date2.setText(new SimpleDateFormat("hh:mm-dd/MM").format(calendar.getTime()));

            if (!TextUtils.isEmpty(mList.get(position).getText())) {
                Picasso.with(viewHolder.image.getContext())
                        .load(mList.get(position).getText())
                        .fit()
                        .into(viewHolder.image);
            } else {
                viewHolder.image.setImageDrawable(viewHolder.image.getContext().getResources().getDrawable(R.drawable.ic_upload_image_foreground));
            }

        }

        viewHolder.pictureLayout.setOnClickListener(v -> {
            viewHolder.pictureLayout.getContext().startActivity(new Intent(viewHolder.pictureLayout.getContext(), PhotoViewerActivity.class)
                    .putExtra("image", mList.get(position).getText()));
        });

    }

    public Message getItemView(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView text, date1, date2;
        ImageView image;
        CardView llText, llPicture;
        LinearLayout pictureLayout, textLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            text = itemView.findViewById(R.id.text);
            date1 = itemView.findViewById(R.id.date1);
            date2 = itemView.findViewById(R.id.date2);
            image = itemView.findViewById(R.id.image);
            llText = itemView.findViewById(R.id.ll_text);
            llPicture = itemView.findViewById(R.id.ll_picture);
            pictureLayout = itemView.findViewById(R.id.pictureLayout);
            textLayout = itemView.findViewById(R.id.textLayout);

        }
    }

    public void updateList(List<Message> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}
