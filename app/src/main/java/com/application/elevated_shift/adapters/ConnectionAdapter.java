package com.application.elevated_shift.adapters;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.application.elevated_shift.R;
import com.application.elevated_shift.models.user.SocialLinks;
import com.google.android.material.appbar.AppBarLayout;
import com.thefinestartist.finestwebview.FinestWebView;

import java.util.List;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


public class ConnectionAdapter extends RecyclerView.Adapter<ConnectionAdapter.ViewHolder> {

    private List<SocialLinks> mList;
    private String type;

    public ConnectionAdapter(List<SocialLinks> mList) {

        this.mList = mList;
    }

    @Override
    public ConnectionAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.connection_item, viewGroup, false);

        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onBindViewHolder(ConnectionAdapter.ViewHolder viewHolder, int position) {

        type = "link";
        if (mList.get(position).getName().toLowerCase().equals("whatsapp") || mList.get(position).getName().toLowerCase().equals("phone"))
            type = "phone";
        else if (mList.get(position).getName().toLowerCase().equals("email"))
            type = "email";

        if (mList.get(position).getName().toLowerCase().equals("phone"))
            viewHolder.image.setImageDrawable(ContextCompat.getDrawable(viewHolder.image.getContext(), R.drawable.ic_phone));
        else if (mList.get(position).getName().toLowerCase().equals("instagram"))
            viewHolder.image.setImageDrawable(ContextCompat.getDrawable(viewHolder.image.getContext(), R.drawable.ic_instagram));
        else if (mList.get(position).getName().toLowerCase().equals("twitter"))
            viewHolder.image.setImageDrawable(ContextCompat.getDrawable(viewHolder.image.getContext(), R.drawable.ic_twitter));
        else if (mList.get(position).getName().toLowerCase().equals("facebook"))
            viewHolder.image.setImageDrawable(ContextCompat.getDrawable(viewHolder.image.getContext(), R.drawable.ic_facebook));
        else if (mList.get(position).getName().toLowerCase().equals("youtube"))
            viewHolder.image.setImageDrawable(ContextCompat.getDrawable(viewHolder.image.getContext(), R.drawable.ic_youtube));
        else if (mList.get(position).getName().toLowerCase().equals("inkedin"))
            viewHolder.image.setImageDrawable(ContextCompat.getDrawable(viewHolder.image.getContext(), R.drawable.ic_linkedin));
        else if (mList.get(position).getName().toLowerCase().equals("whatsapp"))
            viewHolder.image.setImageDrawable(ContextCompat.getDrawable(viewHolder.image.getContext(), R.drawable.ic_whatsapp));
        else if (mList.get(position).getName().toLowerCase().equals("googleplus"))
            viewHolder.image.setImageDrawable(ContextCompat.getDrawable(viewHolder.image.getContext(), R.drawable.ic_google_plus));
        else if (mList.get(position).getName().toLowerCase().equals("email"))
            viewHolder.image.setImageDrawable(ContextCompat.getDrawable(viewHolder.image.getContext(), R.drawable.ic_email_1));
        else if (mList.get(position).getName().toLowerCase().equals("wechat"))
            viewHolder.image.setImageDrawable(ContextCompat.getDrawable(viewHolder.image.getContext(), R.drawable.ic_we_chat));
        else if (mList.get(position).getName().toLowerCase().equals("tumblr"))
            viewHolder.image.setImageDrawable(ContextCompat.getDrawable(viewHolder.image.getContext(), R.drawable.ic_tumblr));

        viewHolder.image.setOnClickListener(v -> {
            if (type.equals("phone")) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + mList.get(position).getLink()));
                viewHolder.image.getContext().startActivity(intent);
            } else if (type.equals("email")) {
                final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("plain/text");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{mList.get(position).getLink()});
                viewHolder.image.getContext().startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            } else {
                new FinestWebView.Builder(viewHolder.image.getContext())
                        .toolbarScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS)
                        .gradientDivider(false)
                        .dividerHeight(5)
//                        .toolbarColorRes(R.color.accent)
//                        .dividerColorRes(R.color.black_30)
//                        .iconDefaultColorRes(R.color.accent)
//                        .iconDisabledColorRes(R.color.gray)
//                        .iconPressedColorRes(R.color.black)
//                        .progressBarHeight(DipPixelHelper.getPixel(context, 3))
//                        .progressBarColorRes(R.color.accent)
//                        .backPressToClose(false)
                        .setCustomAnimations(R.anim.activity_open_enter, R.anim.activity_open_exit, R.anim.activity_close_enter, R.anim.activity_close_exit)
                        .show(mList.get(position).getLink());

//                new FinestWebView.Builder(mActivity).show("https://thecategory5.com/collections/all");
            }
        });

    }

    public SocialLinks getItemView(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);

        }
    }

    public void updateList(List<SocialLinks> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}