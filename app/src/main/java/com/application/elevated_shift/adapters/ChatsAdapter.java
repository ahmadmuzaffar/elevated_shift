package com.application.elevated_shift.adapters;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.text.Spannable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.elevated_shift.R;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.chat.Datum;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.sharedPreference.SharedPreferencesEditor;
import com.application.elevated_shift.ui.activities.ChatActivity;
import com.application.elevated_shift.viewModels.activitesViewModels.AllChatsViewModel;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;


public class ChatsAdapter extends RecyclerSwipeAdapter<ChatsAdapter.ViewHolder> {

    List<Datum> mList;
    Activity activity;
    private SwipeLayout.SwipeListener swipeListener = null;

    public ChatsAdapter(List<Datum> mList, Activity activity) {

        this.mList = mList;
        this.activity = activity;
    }

    @Override
    public ChatsAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chats_item, viewGroup, false);

        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }


    @Override
    public void onBindViewHolder(ChatsAdapter.ViewHolder viewHolder, int position) {

        if (position == 0) {
            if (!new SharedPreferencesEditor(activity).isTutorialViewed(Constants.CHAT_ITEMS)) {

                swipeListener = new SwipeLayout.SwipeListener() {
                    @Override
                    public void onStartOpen(SwipeLayout layout) {

                    }

                    @Override
                    public void onOpen(SwipeLayout layout) {
                        new GuideView.Builder(activity)
                                .setTitle("Delete")
                                .setContentSpan((Spannable) Html.fromHtml("<font color='red'>Tap on the trash button to delete this chat</p>"))
                                .setGravity(Gravity.center)
                                .setTargetView(viewHolder.delete)
                                .setDismissType(DismissType.outside)
                                .setGuideListener(new GuideListener() {
                                    @Override
                                    public void onDismiss(View view) {
                                        viewHolder.swipe.removeSwipeListener(swipeListener);
                                        viewHolder.swipe.close();
                                        new SharedPreferencesEditor(activity).setTutorialViewed(Constants.CHAT_ITEMS, true);
                                    }
                                })
                                .build()
                                .show();
                    }

                    @Override
                    public void onStartClose(SwipeLayout layout) {

                    }

                    @Override
                    public void onClose(SwipeLayout layout) {

                    }

                    @Override
                    public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

                    }

                    @Override
                    public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

                    }
                };

                new GuideView.Builder(activity)
                        .setTitle("Left Swipe")
                        .setContentSpan((Spannable) Html.fromHtml("<font color='#0B304F'>Swipe left the container to view actions</p>"))
                        .setGravity(Gravity.center)
                        .setTargetView(viewHolder.swipe)
                        .setDismissType(DismissType.outside)
                        .setGuideListener(new GuideListener() {
                            @Override
                            public void onDismiss(View view) {
                                viewHolder.swipe.open();
                                viewHolder.swipe.addSwipeListener(swipeListener);
                            }
                        })
                        .build()
                        .show();
            }
        }

        Data user;
        if (!Globals.getUsage().sharedPreferencesEditor.getUser().getId().equals(mList.get(position).getFrom().getId()))
            user = mList.get(position).getFrom();
        else
            user = mList.get(position).getTo();

        viewHolder.text.setText(user.getName());
        Picasso.with(viewHolder.dp.getContext())
                .load(user.getDisplayImage())
                .centerCrop()
                .resize(200, 200)
                .placeholder(R.drawable.placeholder)
                .into(viewHolder.dp);

        viewHolder.ll_chat.setOnClickListener(v -> {
            activity.startActivity(new Intent(activity, ChatActivity.class)
                    .putExtra("chatId", mList.get(position).getId())
            );
        });

        viewHolder.delete.setOnClickListener(v -> {

            new ServerHelper(activity).deleteChat(mList.get(position).getId(), new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        AllChatsViewModel.instance.getChats();
                    }
                }
            });

        });

        viewHolder.swipe.setShowMode(SwipeLayout.ShowMode.PullOut);
        viewHolder.swipe.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipe.findViewById(R.id.bottom_wraper));

        mItemManger.bindView(viewHolder.itemView, position);

    }

    public Datum getItemView(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView text;
        SwipeLayout swipe;
        LinearLayout ll_chat;
        ImageView dp, delete;

        public ViewHolder(View itemView) {
            super(itemView);

            text = itemView.findViewById(R.id.text2);
            ll_chat = itemView.findViewById(R.id.ll_chat);
            swipe = itemView.findViewById(R.id.swipe);
            dp = itemView.findViewById(R.id.dp);
            delete = itemView.findViewById(R.id.Delete);

        }
    }

    public void updateList(List<Datum> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}
