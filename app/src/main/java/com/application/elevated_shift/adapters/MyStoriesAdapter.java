package com.application.elevated_shift.adapters;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.text.Spannable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.elevated_shift.R;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.models.collection.Datum;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.sharedPreference.SharedPreferencesEditor;
import com.application.elevated_shift.ui.activities.MyStoriesActivity;
import com.application.elevated_shift.viewModels.activitesViewModels.MyStoriesMainViewModel;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;


public class MyStoriesAdapter extends RecyclerSwipeAdapter<MyStoriesAdapter.ViewHolder> {

    List<Datum> mList;
    Activity activity;
    private SwipeLayout.SwipeListener swipeListener = null;

    public MyStoriesAdapter(List<Datum> mList, Activity activity) {

        this.mList = mList;
        this.activity = activity;
    }

    @Override
    public MyStoriesAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mystories_item, viewGroup, false);

        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(MyStoriesAdapter.ViewHolder viewHolder, int position) {

        viewHolder.title.setText(mList.get(position).getTitle());

        viewHolder.title.setOnClickListener(v -> {
            activity.startActivity(new Intent(activity, MyStoriesActivity.class).putExtra("collection", mList.get(position)));
        });

        viewHolder.delete.setOnClickListener(v -> {

            new ServerHelper(activity).deleteCollection(mList.get(position).getId(), new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        MyStoriesMainViewModel.instance.setAdapter();
                    }
                }
            });

        });

        viewHolder.swipe.setShowMode(SwipeLayout.ShowMode.PullOut);
        viewHolder.swipe.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipe.findViewById(R.id.bottom_wraper));

        mItemManger.bindView(viewHolder.itemView, position);

        if (position == 0) {
            if (!new SharedPreferencesEditor(activity).isTutorialViewed(Constants.COLLECTION_ITEMS)) {

                swipeListener = new SwipeLayout.SwipeListener() {
                    @Override
                    public void onStartOpen(SwipeLayout layout) {

                    }

                    @Override
                    public void onOpen(SwipeLayout layout) {
                        new GuideView.Builder(activity)
                                .setTitle("Delete")
                                .setContentSpan((Spannable) Html.fromHtml("<font color='red'>Tap on the trash button to delete this chat</p>"))
                                .setGravity(Gravity.center)
                                .setTargetView(viewHolder.delete)
                                .setDismissType(DismissType.outside)
                                .setGuideListener(new GuideListener() {
                                    @Override
                                    public void onDismiss(View view) {
                                        viewHolder.swipe.close();
                                        viewHolder.swipe.removeSwipeListener(swipeListener);
                                        new SharedPreferencesEditor(activity).setTutorialViewed(Constants.COLLECTION_ITEMS, true);
                                    }
                                })
                                .build()
                                .show();
                    }

                    @Override
                    public void onStartClose(SwipeLayout layout) {

                    }

                    @Override
                    public void onClose(SwipeLayout layout) {

                    }

                    @Override
                    public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

                    }

                    @Override
                    public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

                    }
                };

                new GuideView.Builder(activity)
                        .setTitle("Left Swipe")
                        .setContentSpan((Spannable) Html.fromHtml("<font color='#0B304F'>Swipe left the container to view actions</p>"))
                        .setGravity(Gravity.center)
                        .setTargetView(viewHolder.rootContainer)
                        .setDismissType(DismissType.outside)
                        .setGuideListener(new GuideListener() {
                            @Override
                            public void onDismiss(View view) {
                                viewHolder.swipe.open();
                                viewHolder.swipe.addSwipeListener(swipeListener);
                            }
                        })
                        .build()
                        .show();

                Picasso.with(activity).load(R.drawable.placeholder).into(viewHolder.temp);
            }
        }

    }

    public Datum getItemView(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView delete, temp;
        SwipeLayout swipe;
        LinearLayout rootContainer;

        public ViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.storyTitle);
            delete = itemView.findViewById(R.id.Delete);
            swipe = itemView.findViewById(R.id.swipe);
            temp = itemView.findViewById(R.id.temp);
            rootContainer = itemView.findViewById(R.id.root);

        }
    }

    public void updateList(List<Datum> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}
