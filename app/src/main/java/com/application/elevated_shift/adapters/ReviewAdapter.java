package com.application.elevated_shift.adapters;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.application.elevated_shift.R;
import com.application.elevated_shift.models.review.Datum;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;


public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {

    List<Datum> mList;
    Activity activity;

    public ReviewAdapter(List<Datum> mList, Activity activity) {

        this.mList = mList;
        this.activity = activity;
    }

    @Override
    public ReviewAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.review_item, viewGroup, false);

        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ReviewAdapter.ViewHolder viewHolder, int position) {

        viewHolder.username.setText(mList.get(position).getReviewGiver().getName());
        viewHolder.ratingBar.setRating(Float.parseFloat(mList.get(position).getReviewText()));

        if (!TextUtils.isEmpty(mList.get(position).getReviewGiver().getDisplayImage())) {
            Picasso.with(viewHolder.image.getContext())
                    .load(mList.get(position).getReviewGiver().getDisplayImage())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .resize(100, 100)
                    .into(viewHolder.image);
        } else {
            viewHolder.image.setImageDrawable(viewHolder.image.getContext().getResources().getDrawable(R.drawable.placeholder));
        }

    }

    public Datum getItemView(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView username;
        RatingBar ratingBar;

        public ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.reviewDp);
            username = itemView.findViewById(R.id.reviewUsername);
            ratingBar = itemView.findViewById(R.id.ratingBar);

        }
    }

    public void updateList(List<Datum> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}
