package com.application.elevated_shift.adapters;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.elevated_shift.R;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.ui.activities.CoachProfileActivity;
import com.application.elevated_shift.ui.activities.ProfileActivity;
import com.application.elevated_shift.ui.activities.UserProfileActivity;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class SearchedUserAdapter extends RecyclerView.Adapter<SearchedUserAdapter.ViewHolder> {

    private List<Data> mList;

    public SearchedUserAdapter(List<Data> mList) {

        this.mList = mList;
    }

    @NotNull
    @Override
    public SearchedUserAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.searched_user_item, viewGroup, false);

        return new ViewHolder(view);
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onBindViewHolder(@NotNull SearchedUserAdapter.ViewHolder viewHolder, int position) {

        if (!mList.get(position).getCoach()) {

            viewHolder.llCoach.setVisibility(View.GONE);

            viewHolder.nameUser.setText(mList.get(position).getName());

            Picasso.with(viewHolder.imageUser.getContext())
                    .load(mList.get(position).getDisplayImage())
                    .placeholder(R.drawable.placeholder)
                    .centerCrop()
                    .resize(100, 100)
                    .into(viewHolder.imageUser);

            viewHolder.llUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mList.get(position).getId().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId())) {
                        viewHolder.imageUser.getContext().startActivity(new Intent(viewHolder.imageUser.getContext(), ProfileActivity.class)
                                .putExtra("user", mList.get(position)));
                    } else {
                        viewHolder.imageUser.getContext().startActivity(new Intent(viewHolder.imageUser.getContext(), UserProfileActivity.class));
                    }
                }
            });
        } else {

            viewHolder.llUser.setVisibility(View.GONE);

            viewHolder.nameCoach.setText(mList.get(position).getName());

            Picasso.with(viewHolder.imageCoach.getContext())
                    .load(mList.get(position).getDisplayImage())
                    .placeholder(R.drawable.placeholder)
                    .centerCrop()
                    .resize(100, 100)
                    .into(viewHolder.imageCoach);

            HashtagAdapter hashtagAdapter = new HashtagAdapter(mList.get(position).getCoachHashes());
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(viewHolder.imageCoach.getContext(), LinearLayoutManager.HORIZONTAL, false);
            viewHolder.hashes.setLayoutManager(linearLayoutManager);
            viewHolder.hashes.setAdapter(hashtagAdapter);

            viewHolder.llCoach.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mList.get(position).getId().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId())) {
                        viewHolder.imageCoach.getContext().startActivity(new Intent(viewHolder.imageCoach.getContext(), CoachProfileActivity.class)
                                .putExtra("coach", mList.get(position)));
                    } else {
                        viewHolder.imageCoach.getContext().startActivity(new Intent(viewHolder.imageCoach.getContext(), UserProfileActivity.class));
                    }
                }
            });
        }

    }

    public Data getItemView(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView nameUser, nameCoach;
        ImageView imageUser, imageCoach;
        LinearLayout llUser, llCoach;
        RecyclerView hashes;

        public ViewHolder(View itemView) {
            super(itemView);

            nameUser = itemView.findViewById(R.id.text2User);
            imageUser = itemView.findViewById(R.id.dpUser);
            llUser = itemView.findViewById(R.id.ll_user);
            nameCoach = itemView.findViewById(R.id.text2Coach);
            imageCoach = itemView.findViewById(R.id.dpCoach);
            llCoach = itemView.findViewById(R.id.ll_coach);
            hashes = itemView.findViewById(R.id.hashes);

        }
    }

    public void updateList(List<Data> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}
