package com.application.elevated_shift.adapters;

import android.app.Activity;
import android.text.Html;
import android.text.Spannable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.models.goal.Datum;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.sharedPreference.SharedPreferencesEditor;
import com.application.elevated_shift.viewModels.activitesViewModels.MyGoalsViewModel;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;


public class GoalAdapter extends RecyclerSwipeAdapter<GoalAdapter.ViewHolder> {

    List<Datum> mList;
    Activity activity;
    private SwipeLayout.SwipeListener swipeListener = null;

    public GoalAdapter(List<Datum> mList, Activity activity) {

        this.mList = mList;
        this.activity = activity;
    }

    @Override
    public GoalAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.goal_item, viewGroup, false);

        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(GoalAdapter.ViewHolder viewHolder, int position) {

        if (position == 0) {
            if (!new SharedPreferencesEditor(activity).isTutorialViewed(Constants.GOALS_ITEM)) {

                swipeListener = new SwipeLayout.SwipeListener() {
                    @Override
                    public void onStartOpen(SwipeLayout layout) {

                    }

                    @Override
                    public void onOpen(SwipeLayout layout) {
                        new GuideView.Builder(activity)
                                .setTitle("Complete")
                                .setContentSpan((Spannable) Html.fromHtml("<font color='green'>Tap on the tick button to complete this goal</p>"))
                                .setGravity(Gravity.center)
                                .setTargetView(viewHolder.complete)
                                .setDismissType(DismissType.outside)
                                .setGuideListener(new GuideListener() {
                                    @Override
                                    public void onDismiss(View view) {
                                        new GuideView.Builder(activity)
                                                .setTitle("Snooze")
                                                .setContentSpan((Spannable) Html.fromHtml("<font color='black'>Tap on the clock button to snooze this goal</p>"))
                                                .setGravity(Gravity.center)
                                                .setTargetView(viewHolder.snooze)
                                                .setDismissType(DismissType.outside)
                                                .setGuideListener(new GuideListener() {
                                                    @Override
                                                    public void onDismiss(View view) {
                                                        new GuideView.Builder(activity)
                                                                .setTitle("Cancel")
                                                                .setContentSpan((Spannable) Html.fromHtml("<font color='blue'>Tap on the cross button to cancel this goal</p>"))
                                                                .setGravity(Gravity.center)
                                                                .setTargetView(viewHolder.cancel)
                                                                .setDismissType(DismissType.outside)
                                                                .setGuideListener(new GuideListener() {
                                                                    @Override
                                                                    public void onDismiss(View view) {
                                                                        new GuideView.Builder(activity)
                                                                                .setTitle("Delete")
                                                                                .setContentSpan((Spannable) Html.fromHtml("<font color='red'>Tap on the trash button to delete this goal</p>"))
                                                                                .setGravity(Gravity.center)
                                                                                .setTargetView(viewHolder.delete)
                                                                                .setDismissType(DismissType.outside)
                                                                                .setGuideListener(new GuideListener() {
                                                                                    @Override
                                                                                    public void onDismiss(View view) {
                                                                                        viewHolder.swipeLayout.close();
                                                                                        viewHolder.swipeLayout.removeSwipeListener(swipeListener);
                                                                                        new SharedPreferencesEditor(activity).setTutorialViewed(Constants.GOALS_ITEM, true);
                                                                                    }
                                                                                })
                                                                                .build()
                                                                                .show();
                                                                    }
                                                                })
                                                                .build()
                                                                .show();
                                                    }
                                                })
                                                .build()
                                                .show();
                                    }
                                })
                                .build()
                                .show();
                    }

                    @Override
                    public void onStartClose(SwipeLayout layout) {

                    }

                    @Override
                    public void onClose(SwipeLayout layout) {

                    }

                    @Override
                    public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

                    }

                    @Override
                    public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

                    }
                };

                new GuideView.Builder(activity)
                        .setTitle("Left Swipe")
                        .setContentSpan((Spannable) Html.fromHtml("<font color='#0B304F'>Swipe left the container to view actions</p>"))
                        .setGravity(Gravity.center)
                        .setTargetView(viewHolder.swipeLayout)
                        .setDismissType(DismissType.outside)
                        .setGuideListener(new GuideListener() {
                            @Override
                            public void onDismiss(View view) {
                                viewHolder.swipeLayout.open();
                                viewHolder.swipeLayout.addSwipeListener(swipeListener);
                            }
                        })
                        .build()
                        .show();

                Picasso.with(activity).load(R.drawable.placeholder).into(viewHolder.temp);

            }
        }

        viewHolder.goalName.setText(mList.get(position).getGoalName());
        viewHolder.startDate.setText(mList.get(position).getStartDateTime());
        viewHolder.endDate.setText(mList.get(position).getEndDateTime());
        viewHolder.endTime.setText(mList.get(position).getEndTime() != null ? mList.get(position).getEndTime() : "");
        viewHolder.status.setText(mList.get(position).getStatus());

        viewHolder.delete.setOnClickListener(v -> {
            new ServerHelper(activity).deleteGoal(mList.get(position).getId(), new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        MyGoalsViewModel.instance.setAdapter();
                    }
                }
            });
        });

        viewHolder.snooze.setOnClickListener(v -> {
            if (mList.get(position).getStatus().equals("Cancelled")) {
                Toast.makeText(activity, "Can not snooze a cancelled goal", Toast.LENGTH_SHORT).show();
                return;
            }
            if (mList.get(position).getStatus().equals("Overdue")) {
                Toast.makeText(activity, "Can not snooze an overdue goal", Toast.LENGTH_SHORT).show();
                return;
            }
            if (mList.get(position).getStatus().equals("In Progress")) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("status", "Snoozed");

                new ServerHelper(activity).updateGoals(jsonObject, mList.get(position).getId(), new HandlerDataSync() {
                    @Override
                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                        if (success) {
                            MyGoalsViewModel.instance.setAdapter();
                        }
                    }
                });
            } else {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("status", "In Progress");

                new ServerHelper(activity).updateGoals(jsonObject, mList.get(position).getId(), new HandlerDataSync() {
                    @Override
                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                        if (success) {
                            MyGoalsViewModel.instance.setAdapter();
                        }
                    }
                });
            }
        });

        viewHolder.cancel.setOnClickListener(v -> {

            if (mList.get(position).getStatus().equals("Overdue")) {
                Toast.makeText(activity, "Can not cancel an overdue goal", Toast.LENGTH_SHORT).show();
                return;
            }

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("status", "Cancelled");

            new ServerHelper(activity).updateGoals(jsonObject, mList.get(position).getId(), new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        MyGoalsViewModel.instance.setAdapter();
                    }
                }
            });
        });

        viewHolder.complete.setOnClickListener(v -> {
            if (mList.get(position).getStatus().equals("Cancelled")) {
                Toast.makeText(activity, "Can not complete a cancelled goal", Toast.LENGTH_SHORT).show();
                return;
            }
            if (mList.get(position).getStatus().equals("Overdue")) {
                Toast.makeText(activity, "Can not complete an overdue goal", Toast.LENGTH_SHORT).show();
                return;
            }
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("status", "Completed");

            new ServerHelper(activity).updateGoals(jsonObject, mList.get(position).getId(), new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        MyGoalsViewModel.instance.setAdapter();
                    }
                }
            });
        });

        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeLayout.findViewById(R.id.bottom_wraper));

        mItemManger.bindView(viewHolder.itemView, position);

    }

    public Datum getItemView(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView goalName, startDate, endDate, status, endTime;
        ImageView delete, snooze, complete, cancel, temp;
        SwipeLayout swipeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            goalName = itemView.findViewById(R.id.goalName);
            startDate = itemView.findViewById(R.id.startDate);
            temp = itemView.findViewById(R.id.temp);
            status = itemView.findViewById(R.id.status);
            endDate = itemView.findViewById(R.id.endDate);
            endTime = itemView.findViewById(R.id.endTime);
            delete = itemView.findViewById(R.id.Delete);
            snooze = itemView.findViewById(R.id.Snooze);
            complete = itemView.findViewById(R.id.Complete);
            cancel = itemView.findViewById(R.id.Cancel);
            swipeLayout = itemView.findViewById(R.id.swipe);

        }
    }

    public void updateList(List<Datum> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}
