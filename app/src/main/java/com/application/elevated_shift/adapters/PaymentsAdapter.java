package com.application.elevated_shift.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.elevated_shift.R;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.payment.Datum;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import androidx.recyclerview.widget.RecyclerView;


public class PaymentsAdapter extends RecyclerView.Adapter<PaymentsAdapter.ViewHolder> {

    private List<Datum> mList;
    private Activity activity;

    public PaymentsAdapter(List<Datum> mList, Activity activity) {

        this.mList = mList;
        this.activity = activity;
    }

    @Override
    public PaymentsAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.payment_item, viewGroup, false);

        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @SuppressLint({"SimpleDateFormat", "SetTextI18n"})
    @Override
    public void onBindViewHolder(PaymentsAdapter.ViewHolder viewHolder, int position) {

        viewHolder.name.setText(mList.get(position).getUser().getName());
        viewHolder.date.setText(mList.get(position).getPaidTo().getName());

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(Constants.FORMAT_DATE_TIME, Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(mList.get(position).getCreatedAt());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(TimeZone.getDefault());
        String formattedDate = df.format(Objects.requireNonNull(date));
        try {
            calendar.setTime(Objects.requireNonNull(new SimpleDateFormat(Constants.FORMAT_DATE_TIME).parse(formattedDate)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.time.setText(new SimpleDateFormat("dd/MM/yyyy").format(calendar.getTime()) + "\n" + new SimpleDateFormat("hh:mm a").format(calendar.getTime()));
        try {
            JSONObject jsonObject = new JSONObject(mList.get(position).getStripeInvoice());
            double amount = jsonObject.getDouble("amount") / 100;
            viewHolder.amount.setText("$ " + amount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Datum getItemView(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, date, time, amount;
        LinearLayout root;

        public ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.receipentsName);
            date = itemView.findViewById(R.id.date);
            time = itemView.findViewById(R.id.time);
            amount = itemView.findViewById(R.id.amount);
            root = itemView.findViewById(R.id.root);

        }
    }

    public void updateList(List<Datum> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}
