package com.application.elevated_shift.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.services.MessagesService;
import com.application.elevated_shift.sharedPreference.SharedPreferencesEditor;
import com.application.elevated_shift.utils.ImageUtilities;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;

import java.io.File;
import java.util.Map;

import static androidx.constraintlayout.widget.StateSet.TAG;

public class AppFrontActivity extends AppCompatActivity implements GestureDetector.OnGestureListener {

    private ImageView imageUp, imageClose, displayPicture, btnLogout;
    private LinearLayout llUp, llAccount, llDiscover, llStories, llMental, llFeedback, llChats;
    private ConstraintLayout llMenu;
    private ObjectAnimator animUp;
    private ObjectAnimator animMenu;
    private String pictureURI;
    private String picturePath;
    private TextView fullname;
    private String imageUrl;
    private GestureDetector gestureDetector;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_front);

        gestureDetector = new GestureDetector(this);
        displayPicture = findViewById(R.id.dp);
        fullname = findViewById(R.id.fullname);
        btnLogout = findViewById(R.id.btnLogout);
        imageUp = findViewById(R.id.up);
        imageClose = findViewById(R.id.close);
        llUp = findViewById(R.id.ll_up);
        llMenu = findViewById(R.id.ll_menu);
        llAccount = findViewById(R.id.ll_account);
        llDiscover = findViewById(R.id.ll_discover);
        llStories = findViewById(R.id.ll_story);
        llMental = findViewById(R.id.ll_mental);
        llFeedback = findViewById(R.id.ll_feedback);
        llChats = findViewById(R.id.ll_chats);

        fullname.setText(Globals.getUsage().sharedPreferencesEditor.getUser().getFirstName() + " " + Globals.getUsage().sharedPreferencesEditor.getUser().getLastName());

        llAccount.setOnClickListener(v -> {
            startActivity(new Intent(AppFrontActivity.this, UserProfileActivity.class));
        });

        llChats.setOnClickListener(v -> {
            startActivity(new Intent(AppFrontActivity.this, AllChatsActivity.class));
        });

        llStories.setOnClickListener(v -> {
            startActivity(new Intent(AppFrontActivity.this, MyStoryMainActivity.class));
//            startActivity(new Intent(AppFrontActivity.this, MyStoriesActivity.class));
        });

        llMental.setOnClickListener(v -> {
            startActivity(new Intent(AppFrontActivity.this, MentalWellnessActivity.class));
        });

        llFeedback.setOnClickListener(v -> {
            startActivity(new Intent(AppFrontActivity.this, MyFeedbackActivity.class));
        });

        llDiscover.setOnClickListener(v -> {
            startActivity(new Intent(AppFrontActivity.this, MyNewsFeedActivity.class));
        });

        btnLogout.setOnClickListener(v -> {
//            Globals.getUsage().sharedPreferencesEditor.clearPreferences();
            Globals.getUsage().sharedPreferencesEditor.setUser(null);
            Globals.getUsage().sharedPreferencesEditor.setLoggedIn(false);
            stopService(new Intent(this, MessagesService.class));
            startActivity(new Intent(AppFrontActivity.this, StartActivity.class));
            finish();
        });

        imageUp.setOnClickListener(v -> {
            llMenu.setVisibility(View.VISIBLE);
            llUp.setVisibility(View.GONE);

            animUp.end();

            animMenu = ObjectAnimator.ofFloat(llMenu, "translationY", -100f, 0f);
//            animMenu.setDuration(300);//1sec
            animMenu.setInterpolator(new LinearInterpolator());
            animMenu.setRepeatCount(0);
            animMenu.start();
        });

        imageUp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (gestureDetector.onTouchEvent(event)) {
                    return false;
                }
                return true;
            }
        });

        imageClose.setOnClickListener(v -> {
            llMenu.setVisibility(View.GONE);
            llUp.setVisibility(View.VISIBLE);

            animMenu.end();

            animUp = ObjectAnimator.ofFloat(imageUp, "translationY", -100f, 0f);
            animUp.setDuration(2000);
            animUp.setInterpolator(new BounceInterpolator());
            animUp.setRepeatCount(5);
            animUp.start();
        });

        displayPicture.setOnClickListener(v -> {
            displayActionSheet();
        });

//        if (!new SharedPreferencesEditor(this).isTutorialViewed(Constants.MENU_SHOW)) {
//            showTutorials();
//        }

    }

    @Override
    protected void onResume() {

//        IntentFilter filter = new IntentFilter();
//        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
//        registerReceiver(new NetworkChangeReceiver(), filter);

        animUp = ObjectAnimator.ofFloat(imageUp, "translationY", -100f, 0f);
        animUp.setDuration(2000);
        animUp.setInterpolator(new BounceInterpolator());
        animUp.setRepeatCount(5);
        animUp.start();

        String displayImage = Globals.getUsage().sharedPreferencesEditor.getUser().getDisplayImage();
        Picasso.with(displayPicture.getContext())
                .load(displayImage)
                .placeholder(R.drawable.placeholder)
                .centerCrop()
                .resize(150, 150)
                .into(displayPicture);

        super.onResume();
    }

    private void showTutorials() {

        new GuideView.Builder(this)
                .setTitle("Swipe up")
                .setContentSpan((Spannable) Html.fromHtml("<font color='#0B304F'>Swipe up on arrow to view menu</p>"))
                .setGravity(smartdevelop.ir.eram.showcaseviewlib.config.Gravity.center)
                .setTargetView(imageUp)
                .setDismissType(DismissType.outside)
                .setGuideListener(new GuideListener() {
                    @Override
                    public void onDismiss(View view) {
                        new SharedPreferencesEditor(AppFrontActivity.this).setTutorialViewed(Constants.MENU_SHOW, true);
                    }
                })
                .build()
                .show();

    }

    private void displayActionSheet() {

        View view = getLayoutInflater().inflate(R.layout.bottom_action_sheet_picture, null);
        TextView btnOpen, btnUpload, btnCancel;

        btnOpen = view.findViewById(R.id.btnOpen);
        btnUpload = view.findViewById(R.id.btnUpload);
        btnCancel = view.findViewById(R.id.btnCancel);

        final Dialog mBottomSheetDialog = new Dialog(this,
                R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        btnOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
                if (Globals.getUsage().sharedPreferencesEditor.getUser().getDisplayImage() != null && !Globals.getUsage().sharedPreferencesEditor.getUser().getDisplayImage().equals("")) {
                    startActivity(new Intent(AppFrontActivity.this, PhotoViewerActivity.class).putExtra("image", Globals.getUsage().sharedPreferencesEditor.getUser().getDisplayImage()));
                } else if (imageUrl != null && !imageUrl.equals(""))
                    startActivity(new Intent(AppFrontActivity.this, PhotoViewerActivity.class).putExtra("image", imageUrl));
            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
                PickImageDialog.build(new PickSetup()
                        .setWidth(80)
                        .setHeight(80))
                        .setOnPickResult(r -> {

                            Uri uri = Uri.fromFile(new File(r.getPath()));
                            pictureURI = uri.toString();
                            picturePath = r.getPath();

                            Bitmap bitmapCompressed = ImageUtilities.getCompressedBitmap(r.getPath());
                            Uri imageUri = ImageUtilities.getImageUri(AppFrontActivity.this, bitmapCompressed);
                            picturePath = ImageUtilities.getRealPathFromURI(imageUri);

                            UploadCallback callback = new UploadCallback() {
                                @Override
                                public void onStart(String requestId) {

                                }

                                @Override
                                public void onProgress(String requestId, long bytes, long totalBytes) {

                                }

                                @Override
                                public void onSuccess(String requestId, Map resultData) {
                                    Log.d(TAG, "onSuccess: " + resultData.toString());
                                    Toast.makeText(AppFrontActivity.this, "Image Uploaded", Toast.LENGTH_SHORT).show();

                                    imageUrl = resultData.get("secure_url").toString();
                                    if (!TextUtils.isEmpty(imageUrl)) {

                                        JsonObject jsonObject = new JsonObject();
                                        jsonObject.addProperty("displayImage", imageUrl);

                                        new ServerHelper(AppFrontActivity.this).updateUser(jsonObject, Globals.getUsage().sharedPreferencesEditor.getUser().getId(), new HandlerDataSync() {
                                            @Override
                                            public void onSyncSuccessful(boolean success, boolean isDataError) {
                                                if (success) {
                                                    Picasso.with(displayPicture.getContext())
                                                            .load(imageUrl)
                                                            .fit()
                                                            .into(displayPicture);
                                                }
                                            }
                                        });

                                    } else {
                                        displayPicture.setImageDrawable(getResources().getDrawable(R.drawable.ic_upload_image_foreground));
                                    }

                                }

                                @Override
                                public void onError(String requestId, ErrorInfo error) {
                                    Toast.makeText(AppFrontActivity.this, "Image Uploading Failed", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onReschedule(String requestId, ErrorInfo error) {

                                }
                            };
                            String requestId = MediaManager.get().upload(picturePath).unsigned("mdczte2b").callback(callback).dispatch();

                        }).show(AppFrontActivity.this);
            }
        });

    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

        boolean result = false;
        float diffY = e2.getY() - e1.getY();
        float diffX = e2.getX() - e1.getX();
        if (Math.abs(diffX) < Math.abs(diffY)) {
            if (Math.abs(diffY) > 50 && Math.abs(velocityY) > 50) {
                if (diffY < 0)
                    onSwipeUp();
            }
        }

        return result;
    }

    private void onSwipeUp() {

        imageUp.performClick();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    public void onBackPressed() {

        if (Globals.isMyServiceRunning(MessagesService.class, this))
            stopService(new Intent(this, MessagesService.class));

        super.onBackPressed();

    }

}
