package com.application.elevated_shift.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityMentalWellnessBinding;
import com.application.elevated_shift.viewModels.activitesViewModels.MentalWellnessViewModel;

public class MentalWellnessActivity extends AppCompatActivity {

    ActivityMentalWellnessBinding activityMentalWellnessBinding;
    MentalWellnessViewModel mentalWellnessViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMentalWellnessBinding = DataBindingUtil.setContentView(this, R.layout.activity_mental_wellness);
        mentalWellnessViewModel = new MentalWellnessViewModel(this, activityMentalWellnessBinding);
        activityMentalWellnessBinding.setViewModel(mentalWellnessViewModel);

    }
}
