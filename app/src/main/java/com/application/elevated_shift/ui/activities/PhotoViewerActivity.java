package com.application.elevated_shift.ui.activities;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;

import com.application.elevated_shift.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AppCompatActivity;

public class PhotoViewerActivity extends AppCompatActivity {

    PhotoViewAttacher photoViewAttacher;
    private DisplayMetrics displayMetrics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_viewer);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int screenHeight = displayMetrics.heightPixels;
//        int percentage = (screenHeight / 100) * 75;

        PhotoView photoView = findViewById(R.id.photo_view);
//        photoView.setMaxHeight(percentage);
//        ImageView photoView = findViewById(R.id.photo_view);
//        photoViewAttacher = new PhotoViewAttacher(photoView);
        String image = getIntent().getStringExtra("image");

        if (!TextUtils.isEmpty(image)) {
            Picasso.with(photoView.getContext())
                    .load(image)
//                    .resize(photoView.getWidth(), photoView.getHeight())
//                    .fit()
                    .into(photoView);
        } else {
            photoView.setImageDrawable(null);
        }
//        photoViewAttacher.update();

//        ImageView btn_back = findViewById(R.id.btn_back);
//        btn_back.setOnClickListener(v -> {
//            finish();
//        });

    }
}
