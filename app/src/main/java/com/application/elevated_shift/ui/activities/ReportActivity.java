package com.application.elevated_shift.ui.activities;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityReportBinding;
import com.application.elevated_shift.viewModels.activitesViewModels.ReportViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

public class ReportActivity extends AppCompatActivity {

    ActivityReportBinding activityReportBinding;
    ReportViewModel reportViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityReportBinding = DataBindingUtil.setContentView(this, R.layout.activity_report);
        reportViewModel = new ReportViewModel(this, activityReportBinding);
        activityReportBinding.setViewModel(reportViewModel);

    }
}
