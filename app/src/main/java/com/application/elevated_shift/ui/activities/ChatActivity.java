package com.application.elevated_shift.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityChatBinding;
import com.application.elevated_shift.viewModels.activitesViewModels.ChatViewModel;

public class ChatActivity extends AppCompatActivity {

    ActivityChatBinding activityChatBinding;
    ChatViewModel chatViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityChatBinding = DataBindingUtil.setContentView(this, R.layout.activity_chat);
        chatViewModel = new ChatViewModel(this, activityChatBinding);
        activityChatBinding.setViewModel(chatViewModel);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        chatViewModel.disconnectSocket();
    }

}
