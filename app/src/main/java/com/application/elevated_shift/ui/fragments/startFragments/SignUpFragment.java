package com.application.elevated_shift.ui.fragments.startFragments;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.FragmentSignUpBinding;
import com.application.elevated_shift.viewModels.fragmentsViewModels.SignUpViewModel;

public class SignUpFragment extends Fragment {

    FragmentSignUpBinding fragmentSignUpBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentSignUpBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_up, container, false);
        SignUpViewModel signUpViewModel = new SignUpViewModel(fragmentSignUpBinding, getActivity());
        fragmentSignUpBinding.setViewModel(signUpViewModel);

        return fragmentSignUpBinding.getRoot();
    }
}
