package com.application.elevated_shift.ui.activities;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityMyPaymentsBinding;
import com.application.elevated_shift.viewModels.activitesViewModels.MyPaymentsViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

public class MyPaymentsActivity extends AppCompatActivity {

    ActivityMyPaymentsBinding activityMyPaymentsBinding;
    MyPaymentsViewModel myPaymentsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMyPaymentsBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_payments);
        myPaymentsViewModel = new MyPaymentsViewModel(this, activityMyPaymentsBinding);
        activityMyPaymentsBinding.setViewModel(myPaymentsViewModel);

    }

    @Override
    protected void onResume() {

        myPaymentsViewModel.onResume();

        super.onResume();
    }
}
