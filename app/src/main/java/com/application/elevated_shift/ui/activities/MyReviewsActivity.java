package com.application.elevated_shift.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityMyReviewsBinding;
import com.application.elevated_shift.viewModels.activitesViewModels.MyReviewsViewModel;

public class MyReviewsActivity extends AppCompatActivity {

    ActivityMyReviewsBinding activityMyReviewsBinding;
    MyReviewsViewModel myReviewsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMyReviewsBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_reviews);
        myReviewsViewModel = new MyReviewsViewModel(this, activityMyReviewsBinding);
        activityMyReviewsBinding.setViewModel(myReviewsViewModel);

    }

    @Override
    protected void onResume() {

        super.onResume();
    }
}
