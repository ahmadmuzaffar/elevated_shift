package com.application.elevated_shift.ui.activities;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivitySearchViewBinding;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.viewModels.activitesViewModels.SearchViewViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

public class SearchViewActivity extends AppCompatActivity {

    SearchViewViewModel searchViewViewModel;
    ActivitySearchViewBinding activitySearchViewBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activitySearchViewBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_view);
        searchViewViewModel = new SearchViewViewModel(this, activitySearchViewBinding);
        activitySearchViewBinding.setViewModel(searchViewViewModel);

    }

    @Override
    public void onBackPressed() {
        Globals.hideKeyboard(this);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        Globals.hideKeyboard(this);
//        this.overridePendingTransition(R.anim.leave,
//                R.anim.enter);
        super.onDestroy();
    }
}
