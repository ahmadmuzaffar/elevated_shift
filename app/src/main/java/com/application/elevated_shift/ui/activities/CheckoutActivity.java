package com.application.elevated_shift.ui.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.ClientIdResponse;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.CardInputWidget;

import org.jetbrains.annotations.NotNull;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import static com.application.elevated_shift.ui.activities.SplashScreen.publishableKey;

public class CheckoutActivity extends AppCompatActivity {

    public static CheckoutActivity instance;
    public ClientIdResponse clientIdResponse = new ClientIdResponse();
    private OkHttpClient httpClient = new OkHttpClient();
    private String paymentIntentClientSecret;
    private Stripe stripe;
    private TextView mAmount;
    Button btnPayAmount;
    CardInputWidget cardInputWidget;
    public static Data coach;
    ImageView btnBack;
    SpinKitView spin_kit;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        instance = this;
        btnBack = findViewById(R.id.btnBack);
        mAmount = findViewById(R.id.mAmount);
        spin_kit = findViewById(R.id.spin_kit);
        btnPayAmount = findViewById(R.id.btnPayAmount);
        cardInputWidget = findViewById(R.id.cardInputWidget);
        coach = (Data) getIntent().getSerializableExtra("coach");
        mAmount.setText("USD " + (coach.getServicesCharges() != null ? coach.getServicesCharges().toString() : "0.0"));

        stripe = new Stripe(
                getApplicationContext(),
                publishableKey()
        );

        btnBack.setOnClickListener(v -> {
            finish();
        });

        startCheckout();

    }

    private void startCheckout() {
        double amount = coach.getServicesCharges() != null ? coach.getServicesCharges() : 0.0;
        amount *= 100;

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("amount", (int) amount);
        new ServerHelper(this).getClientId(jsonObject, new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success) {
                    paymentIntentClientSecret = clientIdResponse.getClientSecret();
                }
            }
        });

        double finalAmount = amount;
        btnPayAmount.setOnClickListener(v -> {

            if (finalAmount == 0) {
                Toast.makeText(instance, "Amount should not be 0", Toast.LENGTH_SHORT).show();
                return;
            }
            btnPayAmount.setEnabled(false);
            spin_kit.setVisibility(View.VISIBLE);

            PaymentMethodCreateParams params = cardInputWidget.getPaymentMethodCreateParams();
            if (params != null) {
                ConfirmPaymentIntentParams confirmPaymentIntentParams = ConfirmPaymentIntentParams.createWithPaymentMethodCreateParams(params, paymentIntentClientSecret);
                stripe.confirmPayment(this, confirmPaymentIntentParams);
            } else
                btnPayAmount.setEnabled(true);
        });
    }

    public static final class PaymentResultCallback implements ApiResultCallback<PaymentIntentResult> {
        @NonNull
        private final WeakReference<CheckoutActivity> activityWeakReference;

        PaymentResultCallback(@NonNull CheckoutActivity activityWeakReference) {
            this.activityWeakReference = new WeakReference<>(activityWeakReference);
        }

        @Override
        public void onError(@NotNull Exception e) {
            final CheckoutActivity activity = activityWeakReference.get();
            if (activity == null)
                return;

            activity.displayAlert("Error", e.toString());
        }

        @Override
        public void onSuccess(@NotNull PaymentIntentResult paymentIntentResult) {
            final CheckoutActivity activity = activityWeakReference.get();
            if (activity == null)
                return;

            PaymentIntent paymentIntent = paymentIntentResult.getIntent();
            PaymentIntent.Status status = paymentIntent.getStatus();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            if (status == PaymentIntent.Status.Succeeded) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("stripeInvoice", gson.toJson(paymentIntent));
                jsonObject.addProperty("paidTo", CheckoutActivity.coach.getId());
                new ServerHelper(CheckoutActivity.instance).savePayment(jsonObject, new HandlerDataSync() {
                    @Override
                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                        if (success) {
//                            activity.displayAlert("Payment completed", gson.toJson(paymentIntent));

                            List<String> list = Globals.getUsage().sharedPreferencesEditor.getUser().getSubscribedCoaches();
                            list.add(coach.getId());

                            HashMap<String, List<String>> map = new HashMap<>();
                            map.put("subscribedCoaches", list);

                            new ServerHelper(CheckoutActivity.instance).updateUser(map, Globals.getUsage().sharedPreferencesEditor.getUser().getId(), new HandlerDataSync() {
                                @Override
                                public void onSyncSuccessful(boolean success, boolean isDataError) {
                                    if (success) {
                                        activity.spin_kit.setVisibility(View.GONE);
                                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(CheckoutActivity.instance, SweetAlertDialog.SUCCESS_TYPE);
                                        sweetAlertDialog.setTitle("Transaction Completed");
                                        sweetAlertDialog.setContentText("Congratulations! you have successfully purchased services for $USD " + paymentIntent.getAmount());
                                        sweetAlertDialog.setConfirmButton("OK", new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                Intent intent = new Intent();
                                                intent.putExtra("message", "done");
                                                CheckoutActivity.instance.setResult(125, intent);
                                                CheckoutActivity.instance.finish();
                                                sweetAlertDialog.dismissWithAnimation();
                                            }
                                        });
                                        sweetAlertDialog.showCancelButton(false);
                                        sweetAlertDialog.show();
                                    }
                                }
                            });
                        }
                    }
                });
            } else if (status == PaymentIntent.Status.RequiresPaymentMethod) {
                activity.btnPayAmount.setEnabled(true);
                activity.displayAlert("Payment failed", Objects.requireNonNull(paymentIntent.getLastPaymentError()).getMessage());
            }
        }
    }

    private void displayAlert(String payment_completed, String toJson) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(payment_completed)
                .setMessage(toJson);
        builder.setPositiveButton("Ok", null);
        builder.create().show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        stripe.onPaymentResult(requestCode, data, new PaymentResultCallback(this));

    }
}
