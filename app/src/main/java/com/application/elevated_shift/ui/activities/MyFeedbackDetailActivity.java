package com.application.elevated_shift.ui.activities;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityMyFeedbackDetailBinding;
import com.application.elevated_shift.viewModels.activitesViewModels.MyFeedbackDetailViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

public class MyFeedbackDetailActivity extends AppCompatActivity {

    ActivityMyFeedbackDetailBinding activityMyFeedbackDetailBinding;
    MyFeedbackDetailViewModel myFeedbackDetailViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMyFeedbackDetailBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_feedback_detail);
        myFeedbackDetailViewModel = new MyFeedbackDetailViewModel(this, activityMyFeedbackDetailBinding);
        activityMyFeedbackDetailBinding.setViewModel(myFeedbackDetailViewModel);

    }
}
