package com.application.elevated_shift.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityStartBinding;
import com.application.elevated_shift.interfaces.CallBackFragment;
import com.application.elevated_shift.ui.fragments.startFragments.LoginFragment;
import com.application.elevated_shift.viewModels.activitesViewModels.StartViewModel;

public class StartActivity extends AppCompatActivity implements CallBackFragment {

    ActivityStartBinding activityStartBinding;
    private StartViewModel startViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityStartBinding = DataBindingUtil.setContentView(this, R.layout.activity_start);
        startViewModel = new StartViewModel(this, activityStartBinding);
        activityStartBinding.setViewModel(startViewModel);

    }

    @Override
    public void changeFragment(int id) {
        startViewModel.replaceFragment(id);
    }

    @Override
    public void onBackPressed() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.fragmentContainer);

        if (currentFragment instanceof LoginFragment) {
            finish();
        } else {
            startViewModel.openLoginFragment();
        }
    }
}
