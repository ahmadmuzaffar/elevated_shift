package com.application.elevated_shift.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityMyFeedbackBinding;
import com.application.elevated_shift.viewModels.activitesViewModels.MyFeedbackViewModel;

public class MyFeedbackActivity extends AppCompatActivity {

    ActivityMyFeedbackBinding activityMyFeedbackBinding;
    MyFeedbackViewModel myFeedbackViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMyFeedbackBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_feedback);
        myFeedbackViewModel = new MyFeedbackViewModel(this, activityMyFeedbackBinding);
        activityMyFeedbackBinding.setViewModel(myFeedbackViewModel);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
