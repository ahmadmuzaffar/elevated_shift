package com.application.elevated_shift.ui.fragments.startFragments;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.FragmentLoginBinding;
import com.application.elevated_shift.viewModels.fragmentsViewModels.LoginViewModel;

public class LoginFragment extends Fragment {

    FragmentLoginBinding fragmentLoginBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentLoginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        LoginViewModel loginViewModel = new LoginViewModel(fragmentLoginBinding, getActivity());
        fragmentLoginBinding.setViewModel(loginViewModel);

        return fragmentLoginBinding.getRoot();
    }
}
