package com.application.elevated_shift.ui.activities;

import android.content.Intent;
import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityCoachProfileBinding;
import com.application.elevated_shift.viewModels.activitesViewModels.CoachProfileViewModel;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

public class CoachProfileActivity extends AppCompatActivity {

    ActivityCoachProfileBinding activityCoachProfileBinding;
    CoachProfileViewModel coachProfileViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityCoachProfileBinding = DataBindingUtil.setContentView(this, R.layout.activity_coach_profile);
        coachProfileViewModel = new CoachProfileViewModel(this, activityCoachProfileBinding);
        activityCoachProfileBinding.setViewModel(coachProfileViewModel);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 125) {
            if (data.hasExtra("message")) {
                String message = data.getStringExtra("message");
                if (message.equals("done"))
                    coachProfileViewModel.subscriptionWasDone();
            }
        }

    }
}
