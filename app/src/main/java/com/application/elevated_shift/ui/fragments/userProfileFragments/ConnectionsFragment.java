package com.application.elevated_shift.ui.fragments.userProfileFragments;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.FragmentConnectionsBinding;
import com.application.elevated_shift.viewModels.fragmentsViewModels.ConnectionsViewModel;

public class ConnectionsFragment extends Fragment {

    private FragmentConnectionsBinding fragmentConnectionsBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentConnectionsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_connections, container, false);
        ConnectionsViewModel connectionsViewModel = new ConnectionsViewModel(fragmentConnectionsBinding, getActivity());
        fragmentConnectionsBinding.setViewModel(connectionsViewModel);

        return fragmentConnectionsBinding.getRoot();

    }
}
