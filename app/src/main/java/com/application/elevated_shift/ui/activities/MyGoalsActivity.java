package com.application.elevated_shift.ui.activities;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityMyGoalsBinding;
import com.application.elevated_shift.viewModels.activitesViewModels.MyGoalsViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

public class MyGoalsActivity extends AppCompatActivity {

    ActivityMyGoalsBinding activityMyGoalsBinding;
    MyGoalsViewModel myGoalsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMyGoalsBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_goals);
        myGoalsViewModel = new MyGoalsViewModel(this, activityMyGoalsBinding);
        activityMyGoalsBinding.setViewModel(myGoalsViewModel);

    }

    @Override
    protected void onResume() {

        myGoalsViewModel.onResume();

        super.onResume();
    }
}
