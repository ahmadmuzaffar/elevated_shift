package com.application.elevated_shift.ui.activities;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityMyNewsFeedBinding;
import com.application.elevated_shift.viewModels.activitesViewModels.MyNewsFeedViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

public class MyNewsFeedActivity extends AppCompatActivity {

    ActivityMyNewsFeedBinding activityMyNewsFeedBinding;
    MyNewsFeedViewModel myNewsFeedViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMyNewsFeedBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_news_feed);
        myNewsFeedViewModel = new MyNewsFeedViewModel(this, activityMyNewsFeedBinding);
        activityMyNewsFeedBinding.setViewModel(myNewsFeedViewModel);

    }
}
