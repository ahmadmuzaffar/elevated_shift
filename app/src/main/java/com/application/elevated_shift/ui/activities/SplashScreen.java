package com.application.elevated_shift.ui.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import com.application.elevated_shift.R;
import com.application.elevated_shift.services.MessagesService;
import com.application.elevated_shift.dialogues.SweetAlertDialogs;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.models.user.LoginBody;
import com.application.elevated_shift.requestPermisson.RequestPermissionsHelper;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.orm.SugarContext;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class SplashScreen extends AppCompatActivity {

    public static final int PERMISSION_REQUEST_CODE = 0;


    boolean checkInResume = false;
    private int currentApiVersion;

    //     Used to load the 'native-lib' library onoldAPIsURL application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (checkInResume) {
            checkInResume = false;
//            requestPermission();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @SuppressLint("NewApi")
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (currentApiVersion >= Build.VERSION_CODES.KITKAT && hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

        currentApiVersion = Build.VERSION.SDK_INT;

        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        // This work only for android 4.4+
        if (currentApiVersion >= Build.VERSION_CODES.KITKAT) {

            getWindow().getDecorView().setSystemUiVisibility(flags);

            final View decorView = getWindow().getDecorView();
            decorView
                    .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {

                        @Override
                        public void onSystemUiVisibilityChange(int visibility) {
                            if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                                decorView.setSystemUiVisibility(flags);
                            }
                        }
                    });
        }

        requestPermission();
    }

    public void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Android M Permission check
            int permission = RequestPermissionsHelper.requestPermission(SplashScreen.this);
            if (permission == RequestPermissionsHelper.requestPermissionCode) {
                ActivityCompat.requestPermissions(SplashScreen.this, RequestPermissionsHelper.persmissionsStringArray, SplashScreen.PERMISSION_REQUEST_CODE);
            } else if (permission == RequestPermissionsHelper.notShouldShowRequestPermissionRationaleCode) {
                showDialogPermissionDenied();
            }
            if (permission == RequestPermissionsHelper.notCheckSelfPermissionCode) {
                initializeApplication();
            }
        } else {
            initializeApplication();
        }
    }

    public void initializeApplication() {
        Globals.getInstance(this);
        SugarContext.init(this);

        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(getApplicationContext()));
        checkLoggedInStatus();
    }

    private void checkLoggedInStatus() {

        if (Globals.getUsage().sharedPreferencesEditor.IsLoggedIn()) {

            LoginBody loginBody = new LoginBody();
            loginBody.setEmail(Globals.getUsage().sharedPreferencesEditor.getUser().getEmail());
            loginBody.setPassword(Globals.getUsage().sharedPreferencesEditor.getUser().getPassword());

            String password = Globals.getUsage().sharedPreferencesEditor.getUser().getPassword();
            new ServerHelper(this).login(loginBody, new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {

                        new ServerHelper(SplashScreen.this).getUsers(new HandlerDataSync() {
                            @Override
                            public void onSyncSuccessful(boolean success, boolean isDataError) {
                                if (success) {
                                    if (!Globals.isMyServiceRunning(MessagesService.class, SplashScreen.this))
                                        startService(new Intent(SplashScreen.this, MessagesService.class));

                                    Data user = Globals.getUsage().sharedPreferencesEditor.getUser();
                                    user.setPassword(password);

                                    Globals.getUsage().sharedPreferencesEditor.setLoggedIn(true);
                                    Globals.getUsage().sharedPreferencesEditor.setUser(user);

                                    startActivity(new Intent(SplashScreen.this, AppFrontActivity.class));
                                    finish();
                                }
                            }
                        });

                    }
                }
            });
        } else {

            if (Globals.isMyServiceRunning(MessagesService.class, this))
                stopService(new Intent(this, MessagesService.class));

            setToWait();
        }

    }

    public void setToWait() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(2000);
                    startHomeActivity();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        if (requestCode == SplashScreen.PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                boolean granted = true;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        granted = false;
                        break;
                    }
                }
                if (granted) {
                    initializeApplication();
                } else {
                    showDialogPermissionDenied();
                }
            } else {
                showDialogPermissionDenied();
            }
        }
    }

    public void startHomeActivity() {
        Intent intent = new Intent(SplashScreen.this, StartActivity.class);
        startActivity(intent);
        finish();
    }

    public void showDialogPermissionDenied() {

        SweetAlertDialogs.getInstance().showDialogPermissionDenied(this, new SweetAlertDialogs.OnDialogClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                checkInResume = true;
                sweetAlertDialog.dismiss();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
//                finish();
            }
        });

    }

    public static native String baseURL();

    public static native String publishableKey();


}
