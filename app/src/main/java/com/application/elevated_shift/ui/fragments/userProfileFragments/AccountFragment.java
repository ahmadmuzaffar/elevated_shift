package com.application.elevated_shift.ui.fragments.userProfileFragments;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.FragmentAccountBinding;
import com.application.elevated_shift.ui.activities.UserProfileActivity;
import com.application.elevated_shift.viewModels.fragmentsViewModels.AccountViewModel;

public class AccountFragment extends Fragment {

    FragmentAccountBinding fragmentAccountBinding;
    AccountViewModel accountViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentAccountBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_account, container, false);
        accountViewModel = new AccountViewModel(fragmentAccountBinding, (UserProfileActivity) getActivity());
        fragmentAccountBinding.setViewModel(accountViewModel);

        return fragmentAccountBinding.getRoot();
    }
}
