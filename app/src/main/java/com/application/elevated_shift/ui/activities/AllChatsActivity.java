package com.application.elevated_shift.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityAllChatsBinding;
import com.application.elevated_shift.viewModels.activitesViewModels.AllChatsViewModel;

public class AllChatsActivity extends AppCompatActivity {

    ActivityAllChatsBinding activityAllChatsBinding;
    AllChatsViewModel allChatsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityAllChatsBinding = DataBindingUtil.setContentView(this, R.layout.activity_all_chats);
        allChatsViewModel = new AllChatsViewModel(this, activityAllChatsBinding);
        activityAllChatsBinding.setViewModel(allChatsViewModel);

    }
}
