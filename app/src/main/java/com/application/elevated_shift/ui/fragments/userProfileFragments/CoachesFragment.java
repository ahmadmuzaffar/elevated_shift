package com.application.elevated_shift.ui.fragments.userProfileFragments;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.FragmentCoachesBinding;
import com.application.elevated_shift.viewModels.fragmentsViewModels.CoachesViewModel;

public class CoachesFragment extends Fragment {

    FragmentCoachesBinding fragmentCoachesBinding;
    CoachesViewModel coachesViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentCoachesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_coaches, container, false);
        coachesViewModel = new CoachesViewModel(fragmentCoachesBinding, getActivity());
        fragmentCoachesBinding.setViewModel(coachesViewModel);

        return fragmentCoachesBinding.getRoot();
    }
}
