package com.application.elevated_shift.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityMyProgressBinding;
import com.application.elevated_shift.viewModels.activitesViewModels.MyProgressViewModel;

public class MyProgressActivity extends AppCompatActivity {

    ActivityMyProgressBinding activityMyProgressBinding;
    MyProgressViewModel myProgressViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMyProgressBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_progress);
        myProgressViewModel = new MyProgressViewModel(this, activityMyProgressBinding);
        activityMyProgressBinding.setViewModel(myProgressViewModel);

    }
}
