package com.application.elevated_shift.ui.activities;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityMyGoalsBinding;
import com.application.elevated_shift.databinding.ActivityMyPerformanceBinding;
import com.application.elevated_shift.viewModels.activitesViewModels.MyPerformanceViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

public class MyPerformanceActivity extends AppCompatActivity {

    ActivityMyPerformanceBinding activityMyPerformanceBinding;
    MyPerformanceViewModel myPerformanceViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMyPerformanceBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_performance);
        myPerformanceViewModel = new MyPerformanceViewModel(this, activityMyPerformanceBinding);
        activityMyPerformanceBinding.setViewModel(myPerformanceViewModel);

    }

    @Override
    protected void onResume() {

        myPerformanceViewModel.onResume();

        super.onResume();
    }
}
