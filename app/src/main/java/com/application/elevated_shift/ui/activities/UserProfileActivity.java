package com.application.elevated_shift.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityUserProfileBinding;
import com.application.elevated_shift.ui.fragments.userProfileFragments.AccountFragment;
import com.application.elevated_shift.ui.fragments.userProfileFragments.UpdateAccountFragment;
import com.application.elevated_shift.viewModels.activitesViewModels.UserProfileViewModel;

public class UserProfileActivity extends AppCompatActivity {

    ActivityUserProfileBinding activityUserProfileBinding;
    UserProfileViewModel userProfileViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityUserProfileBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_profile);
        userProfileViewModel = new UserProfileViewModel(this, activityUserProfileBinding);
        activityUserProfileBinding.setViewModel(userProfileViewModel);

    }

    @Override
    public void onBackPressed() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.fragmentBox);
        Fragment cFragment = fragmentManager.findFragmentById(R.id.fragment);

        if (cFragment instanceof UpdateAccountFragment) {
            fragmentManager.popBackStackImmediate();
        } else {
            if (currentFragment instanceof AccountFragment)
                finish();
            else {
                activityUserProfileBinding.tabAccount.setBackgroundResource(R.drawable.tab_bottom);
                activityUserProfileBinding.tabCoaches.setBackground(null);
                activityUserProfileBinding.tabConnections.setBackground(null);
                userProfileViewModel.openAccountFragment();
            }
        }

    }

    @Override
    protected void onResume() {

        userProfileViewModel.onResume();

        super.onResume();
    }
}
