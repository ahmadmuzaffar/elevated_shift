package com.application.elevated_shift.ui.fragments.startFragments;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.FragmentForgotPasswordBinding;
import com.application.elevated_shift.viewModels.fragmentsViewModels.ForgotPasswordViewModel;

public class ForgotPasswordFragment extends Fragment {

    FragmentForgotPasswordBinding fragmentForgotPasswordBinding;
    ForgotPasswordViewModel forgotPasswordViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentForgotPasswordBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgot_password, container, false);
        forgotPasswordViewModel = new ForgotPasswordViewModel(fragmentForgotPasswordBinding, getActivity());
        fragmentForgotPasswordBinding.setViewModel(forgotPasswordViewModel);

        return fragmentForgotPasswordBinding.getRoot();
    }
}
