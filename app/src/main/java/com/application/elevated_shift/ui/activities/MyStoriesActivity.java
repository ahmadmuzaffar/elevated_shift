package com.application.elevated_shift.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityMyStoriesBinding;
import com.application.elevated_shift.viewModels.activitesViewModels.MyStoriesViewModel;

public class MyStoriesActivity extends AppCompatActivity {

    ActivityMyStoriesBinding activityMyStoriesBinding;
    MyStoriesViewModel myStoriesViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMyStoriesBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_stories);
        myStoriesViewModel = new MyStoriesViewModel(this, activityMyStoriesBinding);
        activityMyStoriesBinding.setViewModel(myStoriesViewModel);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
