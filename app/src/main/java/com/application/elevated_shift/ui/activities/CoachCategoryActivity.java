package com.application.elevated_shift.ui.activities;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityCoachCategoryBinding;
import com.application.elevated_shift.viewModels.activitesViewModels.CoachCategoryViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

public class CoachCategoryActivity extends AppCompatActivity {

    CoachCategoryViewModel coachCategoryViewModel;
    ActivityCoachCategoryBinding activityCoachCategoryBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityCoachCategoryBinding = DataBindingUtil.setContentView(this, R.layout.activity_coach_category);
        coachCategoryViewModel = new CoachCategoryViewModel(this, activityCoachCategoryBinding);
        activityCoachCategoryBinding.setViewModel(coachCategoryViewModel);

    }
}
