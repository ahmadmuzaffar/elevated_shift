package com.application.elevated_shift.ui.fragments.userProfileFragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;

import android.text.Html;
import android.text.Spannable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.FragmentUpdateAccountBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.sharedPreference.SharedPreferencesEditor;
import com.application.elevated_shift.utils.Utilities;
import com.application.elevated_shift.viewModels.fragmentsViewModels.AccountViewModel;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class UpdateAccountFragment extends Fragment {

    FragmentUpdateAccountBinding mBinding;
    public Data data;

    public UpdateAccountFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_update_account, container, false);

        TextView editFirstName, editLastName, editDate, editGender, email, okay, editWeight, editHeight, editCategory, editLocation, editServiceCharges;
        EditText etFirstName, etLastName, etDate, etGender, etAge, etWeight, etHeight, etCategory, etLocation, etServiceCharges;
        LinearLayout layoutCategory, layoutServiceCharges;
        View catDiv, chargesDiv;
        ImageView imageView;

        layoutCategory = mBinding.layoutCategory;
        layoutServiceCharges = mBinding.layoutServiceCharges;

        catDiv = mBinding.catDiv;
        chargesDiv = mBinding.chargesDiv;

        imageView = mBinding.temp;

        editFirstName = mBinding.editFirstName;
        editLastName = mBinding.editLastName;
        editDate = mBinding.dateOfBirth;
        editGender = mBinding.editGender;
        editWeight = mBinding.editWeight;
        editHeight = mBinding.editHeight;
        editCategory = mBinding.editCategory;
        editLocation = mBinding.editLocation;
        editServiceCharges = mBinding.editServiceCharges;
        email = mBinding.email;
        okay = mBinding.okay;

        etFirstName = mBinding.firstName;
        etLastName = mBinding.lastName;
        etDate = mBinding.dateOfBirth;
        etGender = mBinding.gender;
        etAge = mBinding.age;
        etWeight = mBinding.weight;
        etHeight = mBinding.height;
        etCategory = mBinding.category;
        etLocation = mBinding.location;
        etServiceCharges = mBinding.serviceCharges;

        new ServerHelper(getActivity()).getUser(Globals.getUsage().sharedPreferencesEditor.getUser().getId(), new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success) {

                    data = AccountViewModel.instance.data;

                    etFirstName.setText(data.getFirstName());
                    etLastName.setText(data.getLastName());
                    email.setText(data.getEmail());
                    Calendar calendar = Calendar.getInstance();
                    try {
                        calendar.setTime(new SimpleDateFormat(Constants.FORMAT_DATE_APP).parse(data.getDob()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    etDate.setText(new SimpleDateFormat(Constants.FORMAT_DATE_API).format(calendar.getTime()));
                    etGender.setText(data.getGender());

                    Calendar birthDay = Calendar.getInstance();
                    try {
                        birthDay.setTimeInMillis(new SimpleDateFormat(Constants.FORMAT_DATE_TIME).parse(data.getDob()).getTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    long currentTime = System.currentTimeMillis();
                    Calendar now = Calendar.getInstance();
                    now.setTimeInMillis(currentTime);

                    int years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);

                    etAge.setText(years + "");
                    etWeight.setText(data.getWeight() != null ? data.getWeight() : "");
                    etHeight.setText(data.getHeight() != null ? data.getHeight() : "");
                    etLocation.setText(data.getLocation() != null ? data.getLocation() : "");

                    if (data.getCoach()) {
                        layoutCategory.setVisibility(View.VISIBLE);
                        layoutServiceCharges.setVisibility(View.VISIBLE);
                        etCategory.setText(data.getCategory() != null ? data.getCategory() : "");
                        etServiceCharges.setText(data.getServicesCharges() != null ? data.getServicesCharges() + "" : "");

                    } else {
                        layoutCategory.setVisibility(View.GONE);
                        layoutServiceCharges.setVisibility(View.GONE);
                        catDiv.setVisibility(View.GONE);
                        chargesDiv.setVisibility(View.GONE);
                    }
                }
            }
        });

        editFirstName.setOnClickListener(v -> {
            if (editFirstName.getText().toString().equals("Edit")) {
                editFirstName.setText("Done");
                etFirstName.setEnabled(true);
                etFirstName.requestFocus();
            } else {

                JsonObject userObj = new JsonObject();
                userObj.addProperty("firstName", etFirstName.getText().toString());

                new ServerHelper(getActivity()).updateUser(userObj, Globals.getUsage().sharedPreferencesEditor.getUser().getId(), new HandlerDataSync() {
                    @Override
                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                        if (success) {
                            editFirstName.setText("Edit");
                            etFirstName.setEnabled(false);
                        }
                    }
                });
            }
        });

        editLastName.setOnClickListener(v -> {
            if (editLastName.getText().toString().equals("Edit")) {
                editLastName.setText("Done");
                etLastName.setEnabled(true);
                etLastName.requestFocus();
            } else {

                JsonObject userObj = new JsonObject();
                userObj.addProperty("lastName", etLastName.getText().toString());

                new ServerHelper(getActivity()).updateUser(userObj, Globals.getUsage().sharedPreferencesEditor.getUser().getId(), new HandlerDataSync() {
                    @Override
                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                        if (success) {
                            editLastName.setText("Edit");
                            etLastName.setEnabled(false);
                        }
                    }
                });
            }
        });

        editDate.setOnClickListener(v -> {
            if (editDate.getText().toString().equals("Edit")) {
                editLastName.setText("Done");
                Utilities.openCalender(getActivity(), etDate, null, "", Constants.FORMAT_DATE_API);
            } else {
                JsonObject userObj = new JsonObject();
                userObj.addProperty("dob", etDate.getText().toString());

                new ServerHelper(getActivity()).updateUser(userObj, Globals.getUsage().sharedPreferencesEditor.getUser().getId(), new HandlerDataSync() {
                    @Override
                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                        if (success) {
                            editDate.setText("Edit");
                            etDate.setEnabled(false);
                        }
                    }
                });
            }
        });

        editGender.setOnClickListener(v -> {
            if (editGender.getText().toString().equals("Edit")) {
                editGender.setText("Done");
                showGenderDialog(etGender);
            } else {
                JsonObject userObj = new JsonObject();
                userObj.addProperty("gender", etGender.getText().toString());

                new ServerHelper(getActivity()).updateUser(userObj, Globals.getUsage().sharedPreferencesEditor.getUser().getId(), new HandlerDataSync() {
                    @Override
                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                        if (success) {
                            editGender.setText("Edit");
                            etGender.setEnabled(false);
                        }
                    }
                });
            }
        });

        editHeight.setOnClickListener(v -> {
            if (editHeight.getText().toString().equals("Edit")) {
                editHeight.setText("Done");
                etHeight.setEnabled(true);
                etHeight.requestFocus();
            } else {

                JsonObject userObj = new JsonObject();
                userObj.addProperty("height", etHeight.getText().toString());

                new ServerHelper(getActivity()).updateUser(userObj, Globals.getUsage().sharedPreferencesEditor.getUser().getId(), new HandlerDataSync() {
                    @Override
                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                        if (success) {
                            editHeight.setText("Edit");
                            etHeight.setEnabled(false);
                        }
                    }
                });
            }
        });

        editWeight.setOnClickListener(v -> {
            if (editWeight.getText().toString().equals("Edit")) {
                editWeight.setText("Done");
                etWeight.setEnabled(true);
                etWeight.requestFocus();
            } else {
                JsonObject userObj = new JsonObject();
                userObj.addProperty("weight", etWeight.getText().toString());

                new ServerHelper(getActivity()).updateUser(userObj, Globals.getUsage().sharedPreferencesEditor.getUser().getId(), new HandlerDataSync() {
                    @Override
                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                        if (success) {
                            editWeight.setText("Edit");
                            etWeight.setEnabled(false);
                        }
                    }
                });
            }
        });

        editCategory.setOnClickListener(v -> {
            if (editCategory.getText().toString().equals("Edit")) {
                editCategory.setText("Done");
                showCategoryDialog(etCategory);
            } else {

                JsonObject userObj = new JsonObject();
                userObj.addProperty("category", etCategory.getText().toString());

                new ServerHelper(getActivity()).updateUser(userObj, Globals.getUsage().sharedPreferencesEditor.getUser().getId(), new HandlerDataSync() {
                    @Override
                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                        if (success) {
                            editCategory.setText("Edit");
                            etCategory.setEnabled(false);
                        }
                    }
                });
            }
        });

        editLocation.setOnClickListener(v -> {
            if (editLocation.getText().toString().equals("Edit")) {
                editLocation.setText("Done");
                etLocation.setEnabled(true);
                etLocation.requestFocus();
            } else {

                JsonObject userObj = new JsonObject();
                userObj.addProperty("location", etLocation.getText().toString());

                new ServerHelper(getActivity()).updateUser(userObj, Globals.getUsage().sharedPreferencesEditor.getUser().getId(), new HandlerDataSync() {
                    @Override
                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                        if (success) {
                            editLocation.setText("Edit");
                            etLocation.setEnabled(false);
                        }
                    }
                });
            }
        });

        editServiceCharges.setOnClickListener(v -> {
            if (editServiceCharges.getText().toString().equals("Edit")) {
                editServiceCharges.setText("Done");
                etServiceCharges.setEnabled(true);
                etServiceCharges.requestFocus();
            } else {

                JsonObject userObj = new JsonObject();
                userObj.addProperty("servicesCharges", Double.parseDouble(etServiceCharges.getText().toString()));

                new ServerHelper(getActivity()).updateUser(userObj, Globals.getUsage().sharedPreferencesEditor.getUser().getId(), new HandlerDataSync() {
                    @Override
                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                        if (success) {
                            editServiceCharges.setText("Edit");
                            etServiceCharges.setEnabled(false);
                        }
                    }
                });
            }
        });

        okay.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });

        if (!new SharedPreferencesEditor(getActivity()).isTutorialViewed(Constants.ACCOUNT_UPDATE)) {
            new GuideView.Builder(getActivity())
                    .setTitle("Edit")
                    .setContentSpan((Spannable) Html.fromHtml("<font color='#0B304F'>Tap on Edit to update first name</p>"))
                    .setGravity(Gravity.center)
                    .setTargetView(editFirstName)
                    .setDismissType(DismissType.outside)
                    .setGuideListener(new GuideListener() {
                        @Override
                        public void onDismiss(View view) {
                            editFirstName.setText("Done");
                            etFirstName.setEnabled(true);
                            etFirstName.requestFocus();
                            new GuideView.Builder(getActivity())
                                    .setTitle("Type")
                                    .setContentSpan((Spannable) Html.fromHtml("<font color='#0B304F'>Type new text in inout field</p>"))
                                    .setGravity(Gravity.center)
                                    .setTargetView(etFirstName)
                                    .setDismissType(DismissType.outside)
                                    .setGuideListener(new GuideListener() {
                                        @Override
                                        public void onDismiss(View view) {
                                            etFirstName.setEnabled(false);
                                            new GuideView.Builder(getActivity())
                                                    .setTitle("Done")
                                                    .setContentSpan((Spannable) Html.fromHtml("<font color='#0B304F'>Tap on Done to complete the upate</p>"))
                                                    .setGravity(Gravity.center)
                                                    .setTargetView(editFirstName)
                                                    .setDismissType(DismissType.outside)
                                                    .setGuideListener(new GuideListener() {
                                                        @Override
                                                        public void onDismiss(View view) {
                                                            new SharedPreferencesEditor(getActivity()).setTutorialViewed(Constants.ACCOUNT_UPDATE, true);
                                                            editFirstName.setText("Edit");
                                                        }
                                                    })
                                                    .build()
                                                    .show();
                                        }
                                    })
                                    .build()
                                    .show();
                        }
                    })
                    .build()
                    .show();

            Picasso.with(getActivity()).load(R.drawable.placeholder).into(imageView);
        }

        return mBinding.getRoot();
    }

    private void showCategoryDialog(EditText etCategory) {
        final Dialog d = new Dialog(getActivity());
        d.setContentView(R.layout.category_dialog);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setCancelable(false);
        d.setCanceledOnTouchOutside(false);

        WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();
        lWindowParams.copyFrom(d.getWindow().getAttributes());
        lWindowParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        d.show();
        d.getWindow().setAttributes(lWindowParams);

        RadioGroup rg = d.findViewById(R.id.rg_category);

        if (!etCategory.getText().toString().equals(""))
            if (etCategory.getText().toString().equals("Trendy"))
                ((RadioButton) rg.getChildAt(0)).setChecked(true);
            else if (etCategory.getText().toString().equals("Classic"))
                ((RadioButton) rg.getChildAt(1)).setChecked(true);
            else if (etCategory.getText().toString().equals("Hit"))
                ((RadioButton) rg.getChildAt(2)).setChecked(true);
            else if (etCategory.getText().toString().equals("Strict"))
                ((RadioButton) rg.getChildAt(3)).setChecked(true);
            else if (etCategory.getText().toString().equals("Pre-Planned"))
                ((RadioButton) rg.getChildAt(4)).setChecked(true);
            else if (etCategory.getText().toString().equals("Masters"))
                ((RadioButton) rg.getChildAt(5)).setChecked(true);

        rg.setOnCheckedChangeListener((group, checkedId) -> {
            d.dismiss();
            if (checkedId == group.getChildAt(0).getId()) {
                etCategory.setText("Trendy");
            } else if (checkedId == group.getChildAt(1).getId())
                etCategory.setText("Classic");
            else if (checkedId == group.getChildAt(2).getId())
                etCategory.setText("Hit");
            else if (checkedId == group.getChildAt(3).getId())
                etCategory.setText("Strict");
            else if (checkedId == group.getChildAt(4).getId())
                etCategory.setText("Pre-Planned");
            else if (checkedId == group.getChildAt(5).getId())
                etCategory.setText("Masters");
        });
    }

    public void showGenderDialog(EditText et) {

        final Dialog d = new Dialog(getActivity());
        d.setContentView(R.layout.gender_dialog);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setCancelable(false);
        d.setCanceledOnTouchOutside(false);

        WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();
        lWindowParams.copyFrom(d.getWindow().getAttributes());
        lWindowParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        d.show();
        d.getWindow().setAttributes(lWindowParams);

        RadioGroup rg = d.findViewById(R.id.rg_gender);

        if (et.getText().toString().equals("Male"))
            ((RadioButton) rg.getChildAt(0)).setChecked(true);
        else
            ((RadioButton) rg.getChildAt(1)).setChecked(true);

        rg.setOnCheckedChangeListener((group, checkedId) -> {
            d.dismiss();
            if (checkedId == group.getChildAt(0).getId()) {
                et.setText("Male");
            } else if (checkedId == group.getChildAt(1).getId())
                et.setText("Female");
        });

    }

}