package com.application.elevated_shift.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityMyAchievementBinding;
import com.application.elevated_shift.viewModels.activitesViewModels.MyAchievementViewModel;

public class MyAchievementActivity extends AppCompatActivity {

    ActivityMyAchievementBinding activityMyAchievementBinding;
    MyAchievementViewModel myAchievementViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMyAchievementBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_achievement);
        myAchievementViewModel = new MyAchievementViewModel(this, activityMyAchievementBinding);
        activityMyAchievementBinding.setViewModel(myAchievementViewModel);

    }

    @Override
    protected void onResume() {

        myAchievementViewModel.onResume();

        super.onResume();
    }
}
