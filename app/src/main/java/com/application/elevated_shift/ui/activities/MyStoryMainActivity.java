package com.application.elevated_shift.ui.activities;

import android.os.Bundle;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityMyStoriesBinding;
import com.application.elevated_shift.databinding.ActivityMyStoryMainBinding;
import com.application.elevated_shift.viewModels.activitesViewModels.MyStoriesMainViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

public class MyStoryMainActivity extends AppCompatActivity {

    ActivityMyStoryMainBinding activityMyStoryMainBinding;
    MyStoriesMainViewModel myStoriesMainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMyStoryMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_story_main);
        myStoriesMainViewModel = new MyStoriesMainViewModel(this, activityMyStoryMainBinding);
        activityMyStoryMainBinding.setViewModel(myStoriesMainViewModel);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
