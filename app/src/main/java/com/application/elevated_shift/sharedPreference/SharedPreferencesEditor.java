package com.application.elevated_shift.sharedPreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.models.user.Data;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class SharedPreferencesEditor {

    private final String PREF_FILE_APP = "ApplicationDetails";

    private final String PREF_LOGGED_IN = "loggedIn";

    private Context mContext = null;

    public static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public SharedPreferencesEditor(Context context) {
        this.mContext = context;
    }

    public void clearPreferences() {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE).edit();
        editor.clear().commit();

    }

    public boolean setAllUser(List<Data> value) {
        try {
            SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE).edit();
            editor.putString("all_user", new Gson().toJson(value));
            return editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<Data> getUsers() {
        try {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE);
            String value = sharedPreferences.getString("all_user", "");
            List<Data> user = new Gson().fromJson(value, new TypeToken<List<Data>>() {
            }.getType());
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean setUser(Data value) {
        try {
            SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE).edit();
            editor.putString("user", new Gson().toJson(value));
            return editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Data getUser() {
        try {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE);
            String value = sharedPreferences.getString("user", "");
            Data user = new Gson().fromJson(value, Data.class);
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean IsLoggedIn() {
        try {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE);
            boolean value = sharedPreferences.getBoolean(PREF_LOGGED_IN, false);
            return value;
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }

    public boolean setLoggedIn(boolean syncRequired) {
        try {
            SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE).edit();
            editor.putBoolean(PREF_LOGGED_IN, syncRequired);
            return editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void setTutorialViewed(String key, boolean searchViewed) {
        try {
            SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE).edit();
            editor.putBoolean(key, searchViewed);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isTutorialViewed(String key) {
        try {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE);
            return sharedPreferences.getBoolean(key, false);
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }

}