package com.application.elevated_shift.viewModels.activitesViewModels;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityAllChatsBinding;
import com.application.elevated_shift.databinding.ActivityReportBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.models.goal.GoalResponse;
import com.application.elevated_shift.models.sentiment.SentimentResponse;
import com.application.elevated_shift.models.stress.Datum;
import com.application.elevated_shift.models.stress.StressResponse;
import com.application.elevated_shift.models.weight.WeightResponse;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.ui.activities.ReportActivity;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import androidx.core.content.FileProvider;

public class ReportViewModel {

    public static ReportViewModel instance;
    public GoalResponse goalResponse;
    public StressResponse stressResponse = new StressResponse();
    public SentimentResponse sentimentResponse = new SentimentResponse();
    public WeightResponse weightResponse = new WeightResponse();
    ReportActivity mActivity;
    ActivityReportBinding activityReportBinding;
    Calendar cal = Calendar.getInstance();
    String weekStartDate, weekEndDate;
    private String date;
    private int day;
    private String dirpath;
    private Bitmap bitmap;
    private String pathName;
    private int completedGoals = 0;
    private List<com.application.elevated_shift.models.goal.Datum> goalsList = new ArrayList<>();
    private long totalMillis = 0;

    public ReportViewModel(ReportActivity mActivity, ActivityReportBinding activityReportBinding) {

        instance = this;
        this.mActivity = mActivity;
        this.activityReportBinding = activityReportBinding;

        initViews();
    }

    @SuppressLint("SimpleDateFormat")
    private void initViews() {

        activityReportBinding.btnBack.setOnClickListener(v -> mActivity.onBackPressed());

        cal.add(Calendar.DAY_OF_YEAR, -1);
        Calendar first = (Calendar) cal.clone();
        first.add(Calendar.DAY_OF_WEEK, first.getFirstDayOfWeek() - first.get(Calendar.DAY_OF_WEEK));
        weekStartDate = new SimpleDateFormat("yyyy-MM-dd").format(first.getTime());

        Calendar last = (Calendar) first.clone();
        last.add(Calendar.DAY_OF_YEAR, 7);
        weekEndDate = new SimpleDateFormat("yyyy-MM-dd").format(last.getTime());

        activityReportBinding.print.setOnClickListener(v -> {

            bitmap = loadBitmapFromView(activityReportBinding.pdfLayout, activityReportBinding.pdfLayout.getWidth(), activityReportBinding.pdfLayout.getHeight());
            createPdf();
        });

        new ServerHelper(mActivity).getGoals(new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success) {
                    calculateValues();
                }
            }
        });

        getChartData();

    }

    private void calculateValues() {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(Constants.FORMAT_DATE_API);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        date = weekStartDate;
        for (int j = 0; j < 7; j++) {
            for (int i = 0; i < goalResponse.getData().size(); i++) {
                try {
                    if (goalResponse.getData().get(i).getEndDateTime().contains(df.format(Objects.requireNonNull(sdf.parse(date))))) {
                        goalsList.add(goalResponse.getData().get(i));
                        completedGoals++;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            try {
                calendar.setTime(Objects.requireNonNull(sdf.parse(date)));
                calendar.add(Calendar.DATE, 1);
                date = sdf.format(calendar.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        activityReportBinding.totalGoals.setText(goalResponse.getData().size() + "");
        activityReportBinding.goals.setText(completedGoals + "");
        calculateTime(goalsList);

    }

    private void calculateTime(List<com.application.elevated_shift.models.goal.Datum> list) {

        SimpleDateFormat df = new SimpleDateFormat(Constants.FORMAT_DATE_TIME, Locale.ENGLISH);
        Date startDate = null, endDate = null;
        if (list.size() > 0) {
            for (com.application.elevated_shift.models.goal.Datum datum : list) {
                df.setTimeZone(TimeZone.getTimeZone("UTC"));
                try {
                    startDate = df.parse(datum.getCreatedAt());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                df.setTimeZone(TimeZone.getDefault());
                long startTimeInMillis = startDate.getTime();

                String endDateTime = datum.getEndDateTime() + " " + datum.getEndTime();
                try {
                    endDate = new SimpleDateFormat("MM/dd/yyyy HH:mm").parse(endDateTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long endTimeInMillis = endDate.getTime();
                totalMillis += (endTimeInMillis - startTimeInMillis);
            }
        }

        activityReportBinding.hours.setText(TimeUnit.MILLISECONDS.toHours(totalMillis) + "");
        activityReportBinding.mins.setText((TimeUnit.MILLISECONDS.toMinutes(totalMillis) % 60) + "");

    }

    private void getChartData() {

        new ServerHelper(mActivity).getStress(weekStartDate, weekEndDate, new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success) {

                    new ServerHelper(mActivity).getSentiments(weekStartDate, weekEndDate, new HandlerDataSync() {
                        @Override
                        public void onSyncSuccessful(boolean success, boolean isDataError) {
                            if (success) {
                                setBarChart();

                                new ServerHelper(mActivity).getWeights(weekStartDate, weekEndDate, new HandlerDataSync() {
                                    @Override
                                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                                        if (success) {
                                            setLineChart();
                                        }
                                    }
                                });

                            }
                        }
                    });

                }
            }
        });

    }

    private void setLineChart() {

        activityReportBinding.chartWeight.setBackgroundColor(Color.WHITE);
        activityReportBinding.chartWeight.setGridBackgroundColor(Color.WHITE);
        activityReportBinding.chartWeight.setDrawGridBackground(true);
        activityReportBinding.chartWeight.getDescription().setEnabled(false);

        activityReportBinding.chartWeight.getAxisRight().setEnabled(false);
        activityReportBinding.chartWeight.getAxisLeft().setDrawGridLines(true);
        activityReportBinding.chartWeight.getAxisRight().setDrawGridLines(true);

        Legend legend = activityReportBinding.chartWeight.getLegend();
        legend.setEnabled(false);

        LineDataSet lineDataSet = new LineDataSet(weightEntries(), "");
        lineDataSet.enableDashedLine(10f, 10f, 0f);
        lineDataSet.setValueTextColor(Color.BLACK);
        lineDataSet.setDrawValues(true);
        lineDataSet.setValues(weightEntries());
        lineDataSet.setValueTextSize(10f);
        lineDataSet.setCircleColor(Color.BLACK);
        lineDataSet.setCircleRadius(3f);
        lineDataSet.setColor(Color.BLACK);
        lineDataSet.setDrawCircles(true);
        lineDataSet.setLineWidth(2f);
        lineDataSet.setFillAlpha(255);
        lineDataSet.setDrawFilled(true);
        lineDataSet.setFillDrawable(mActivity.getResources().getDrawable(R.drawable.chart_gradient));

//        String[] days = new String[]{"Mon, Tue, Wed, Thur, Fri, Sat, Sun"};
        XAxis xAxis = activityReportBinding.chartWeight.getXAxis();
//        xAxis.setValueFormatter(new IndexAxisValueFormatter(days));
        xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1);

        LineData lineData = new LineData(lineDataSet);
        lineData.setDrawValues(true);

        activityReportBinding.chartWeight.getXAxis().setAxisMinimum(lineData.getXMin());
        activityReportBinding.chartWeight.getAxisLeft().setAxisMinimum(0);
        activityReportBinding.chartWeight.setData(lineData);
        activityReportBinding.chartWeight.animateXY(1000, 1000);

    }

    @SuppressLint("SimpleDateFormat")
    private ArrayList<BarEntry> stressBarEntries() {
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(Constants.FORMAT_DATE_TIME, Locale.ENGLISH);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        boolean isDataAvailable = false;
        Datum data = null;

        date = weekStartDate;
        for (int j = 0; j < 7; j++) {
            for (int i = 0; i < stressResponse.getData().size(); i++) {
                try {
                    df.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Date date1 = df.parse(stressResponse.getData().get(i).getCreatedAt());
                    df.setTimeZone(TimeZone.getDefault());
                    String formattedDate = df.format(Objects.requireNonNull(date1));
                    if (formattedDate.contains(sdf.format(Objects.requireNonNull(sdf.parse(date))))) {
                        isDataAvailable = true;
                        data = stressResponse.getData().get(i);
                        break;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            try {
                day = Integer.parseInt(new SimpleDateFormat("dd").format(Objects.requireNonNull(sdf.parse(date))));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (isDataAvailable) {
                barEntries.add(new BarEntry(day, Objects.requireNonNull(data).getValue()));
            } else
                barEntries.add(new BarEntry(day, 0));
            data = null;
            isDataAvailable = false;
            try {
                calendar.setTime(Objects.requireNonNull(sdf.parse(date)));
                calendar.add(Calendar.DATE, 1);
                date = sdf.format(calendar.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return barEntries;
    }

    @SuppressLint("SimpleDateFormat")
    private ArrayList<BarEntry> sentimentBarEntries() {
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(Constants.FORMAT_DATE_TIME, Locale.ENGLISH);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        boolean isDataAvailable = false;
        com.application.elevated_shift.models.sentiment.Datum data = null;

        date = weekStartDate;
        for (int j = 0; j < 7; j++) {
            for (int i = 0; i < sentimentResponse.getData().size(); i++) {
                try {
                    df.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Date date1 = df.parse(sentimentResponse.getData().get(i).getCreatedAt());
                    df.setTimeZone(TimeZone.getDefault());
                    String formattedDate = df.format(Objects.requireNonNull(date1));
                    if (formattedDate.contains(sdf.format(Objects.requireNonNull(sdf.parse(date))))) {
                        isDataAvailable = true;
                        data = sentimentResponse.getData().get(i);
                        break;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            try {
                day = Integer.parseInt(new SimpleDateFormat("dd").format(Objects.requireNonNull(sdf.parse(date))));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (isDataAvailable) {

                double percent = (double) (Objects.requireNonNull(data).getPeace() + data.getInsightful() + data.getSatisfaction() + data.getAnxiety() + data.getWorryness() + data.getDepression() + data.getFear() + data.getDisgust()) / 8;
                barEntries.add(new BarEntry(day, (int) percent));
            } else
                barEntries.add(new BarEntry(day, 0));
            data = null;
            isDataAvailable = false;
            try {
                calendar.setTime(Objects.requireNonNull(sdf.parse(date)));
                calendar.add(Calendar.DATE, 1);
                date = sdf.format(calendar.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return barEntries;
    }

    @SuppressLint("SimpleDateFormat")
    private ArrayList<Entry> weightEntries() {
        ArrayList<Entry> entries = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(Constants.FORMAT_DATE_TIME, Locale.ENGLISH);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        boolean isDataAvailable = false;
        com.application.elevated_shift.models.weight.Datum data = null;

        date = weekStartDate;
        for (int j = 0; j < 7; j++) {
            for (int i = 0; i < weightResponse.getData().size(); i++) {
                try {
                    df.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Date date1 = df.parse(weightResponse.getData().get(i).getCreatedAt());
                    df.setTimeZone(TimeZone.getDefault());
                    String formattedDate = df.format(Objects.requireNonNull(date1));
                    if (formattedDate.contains(sdf.format(Objects.requireNonNull(sdf.parse(date))))) {
                        isDataAvailable = true;
                        data = weightResponse.getData().get(i);
                        break;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            try {
                day = Integer.parseInt(new SimpleDateFormat("dd").format(Objects.requireNonNull(sdf.parse(date))));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (isDataAvailable) {
                entries.add(new Entry(day, Objects.requireNonNull(data).getWeight()));
            } else
                entries.add(new Entry(day, 0));
            data = null;
            isDataAvailable = false;
            try {
                calendar.setTime(Objects.requireNonNull(sdf.parse(date)));
                calendar.add(Calendar.DATE, 1);
                date = sdf.format(calendar.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return entries;
    }

    private void setBarChart() {

        BarDataSet barDataSetStress = new BarDataSet(stressBarEntries(), "Stress Free");
        barDataSetStress.setColor(mActivity.getResources().getColor(R.color.insightful));

        BarDataSet barDataSetSentiment = new BarDataSet(sentimentBarEntries(), "Mental Health");
        barDataSetStress.setColor(mActivity.getResources().getColor(R.color.peace));

        BarData barData = new BarData(barDataSetStress, barDataSetSentiment);

//        String[] days = new String[]{"Mon, Tue, Wed, Thur, Fri, Sat, Sun"};
        XAxis xAxis = activityReportBinding.barChart.getXAxis();
//        xAxis.setValueFormatter(new IndexAxisValueFormatter(days));
        xAxis.setCenterAxisLabels(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1);
        xAxis.setGranularityEnabled(true);

        float groupSpace = 0.06f;
        float barSpace = 0.02f;
        float barWidth = 0.45f;

        Legend l = activityReportBinding.barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(true);
        activityReportBinding.barChart.getDescription().setEnabled(false);
        activityReportBinding.barChart.getAxisRight().setEnabled(false);
        barData.setBarWidth(barWidth);
        activityReportBinding.barChart.setData(barData);
        activityReportBinding.barChart.setFitBars(true);
        activityReportBinding.barChart.groupBars(barDataSetStress.getEntryForIndex(0).getX(), groupSpace, barSpace);
        activityReportBinding.barChart.invalidate();

    }

    private static Bitmap loadBitmapFromView(View v, int width, int height) {
        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.draw(c);

        return b;
    }

    private void createPdf() {
        WindowManager wm = (WindowManager) mActivity.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        float hight = displaymetrics.heightPixels;
        float width = displaymetrics.widthPixels;

        int convertHighet = (int) hight, convertWidth = (int) width;

        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(convertWidth, convertHighet, 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();

        Paint paint = new Paint();
        canvas.drawPaint(paint);

        bitmap = Bitmap.createScaledBitmap(bitmap, convertWidth, convertHighet, true);

        paint.setColor(Color.BLUE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        document.finishPage(page);

        File filePath;
        pathName = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + File.separator + "WEEKLYREPORT" + System.currentTimeMillis() + ".pdf";
        filePath = new File(pathName);
        try {
            document.writeTo(new FileOutputStream(filePath));
            document.close();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(mActivity, "Something wrong: " + e.toString(), Toast.LENGTH_LONG).show();
        }

//        Toast.makeText(mActivity, "PDF is created!!!", Toast.LENGTH_SHORT).show();

        openGeneratedPDF();

    }

    private void openGeneratedPDF() {
        File file = new File(pathName);
        if (file.exists()) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = FileProvider.getUriForFile(mActivity, mActivity.getApplicationContext().getPackageName() + ".provider", file);
            intent.setDataAndType(uri, "application/pdf");
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            try {
                mActivity.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(mActivity, "No Application available to view pdf", Toast.LENGTH_LONG).show();
            }
        }
    }

}
