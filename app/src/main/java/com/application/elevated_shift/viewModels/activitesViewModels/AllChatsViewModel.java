package com.application.elevated_shift.viewModels.activitesViewModels;

import android.content.SharedPreferences;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.application.elevated_shift.R;
import com.application.elevated_shift.adapters.ChatsAdapter;
import com.application.elevated_shift.databinding.ActivityAllChatsBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.chat.Datum;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.sharedPreference.SharedPreferencesEditor;
import com.application.elevated_shift.ui.activities.AllChatsActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;

public class AllChatsViewModel {

    public static AllChatsViewModel instance;
    public List<Datum> chatsList = new ArrayList<>();
    AllChatsActivity mActivity;
    ActivityAllChatsBinding activityAllChatsBinding;
    ChatsAdapter chatsAdapter;

    public AllChatsViewModel(AllChatsActivity mActivity, ActivityAllChatsBinding activityAllChatsBinding) {

        instance = this;
        this.mActivity = mActivity;
        this.activityAllChatsBinding = activityAllChatsBinding;

        initViews();
    }

    private void initViews() {

        activityAllChatsBinding.btnBack.setOnClickListener(v -> mActivity.onBackPressed());

        activityAllChatsBinding.searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    setAdapter(chatsList);
                } else
                    performSearch();
            }
        });

//        activityAllChatsBinding.searchBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//
//                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                    Globals.hideKeyboard(mActivity);
//                    performSearch();
//                    return true;
//                }
//
//                return false;
//            }
//        });

        if (new SharedPreferencesEditor(mActivity).isTutorialViewed(Constants.CHAT_SEARCH))
            getChats();
        else
            showTutorials();

    }

    private void showTutorials() {

        new GuideView.Builder(mActivity)
                .setTitle("Search")
                .setContentSpan((Spannable) Html.fromHtml("<font color='#0B304F'>Type username here to find chats</p>"))
                .setGravity(Gravity.center)
                .setTargetView(activityAllChatsBinding.searchBar)
                .setDismissType(DismissType.outside)
                .setGuideListener(new GuideListener() {
                    @Override
                    public void onDismiss(View view) {
                        new SharedPreferencesEditor(mActivity).setTutorialViewed(Constants.CHAT_SEARCH, true);
                        getChats();
                    }
                })
                .build()
                .show();

    }

    public void getChats() {
        new ServerHelper(mActivity).getChats(new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success) {
                    setAdapter(chatsList);
                }
            }
        });
    }

    public void setAdapter(List<Datum> chatsList) {
        if (chatsList.size() == 0) {
            activityAllChatsBinding.noChat.setVisibility(View.VISIBLE);
            activityAllChatsBinding.chatsList.setVisibility(View.GONE);
        } else {
            activityAllChatsBinding.noChat.setVisibility(View.GONE);
            activityAllChatsBinding.chatsList.setVisibility(View.VISIBLE);
        }

        chatsAdapter = new ChatsAdapter(chatsList, mActivity);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        activityAllChatsBinding.chatsList.setLayoutManager(layoutManager);
        activityAllChatsBinding.chatsList.setAdapter(chatsAdapter);
    }

    private void performSearch() {
        List<Datum> tempList = new ArrayList<>();

        for (Datum datum : chatsList) {

            Data otherUser;
            if (!Globals.getUsage().sharedPreferencesEditor.getUser().getId().equals(datum.getFrom().getId()))
                otherUser = datum.getFrom();
            else
                otherUser = datum.getTo();

            if (otherUser.getName().toLowerCase().contains(activityAllChatsBinding.searchBar.getText().toString().toLowerCase())) {
                tempList.add(datum);
            }
        }

        setAdapter(tempList);

    }
}
