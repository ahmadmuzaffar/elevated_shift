package com.application.elevated_shift.viewModels.activitesViewModels;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.adapters.ChatAdapter;
import com.application.elevated_shift.databinding.ActivityChatBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.Typing;
import com.application.elevated_shift.models.chat.GetChatResponse;
import com.application.elevated_shift.models.chat.Message;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.ui.activities.ChatActivity;
import com.application.elevated_shift.ui.activities.CoachProfileActivity;
import com.application.elevated_shift.ui.activities.ProfileActivity;
import com.application.elevated_shift.utils.ImageUtilities;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Map;

import androidx.recyclerview.widget.LinearLayoutManager;

import static androidx.constraintlayout.widget.StateSet.TAG;

public class ChatViewModel {

    public static ChatViewModel instance;
    ChatActivity mActivity;
    ActivityChatBinding activityChatBinding;
    ChatAdapter chatAdapter;
    public GetChatResponse getChatResponse = new GetChatResponse();
    private String pictureURI;
    private String picturePath;
    private String chatId;
    private Data otherUser;
    private Socket mSocket;
    private boolean isSet = false;

    private Emitter.Listener isTyping = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String typingString = args[0].toString();
                    Typing typing = new Gson().fromJson(typingString, Typing.class);
                    if (!typing.getTyping().equals("") && !typing.getId().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId())) {
                        activityChatBinding.typing.setText(typing.getTyping());
                        activityChatBinding.typing.setVisibility(View.VISIBLE);
                    } else {
                        activityChatBinding.typing.setText("");
                        activityChatBinding.typing.setVisibility(View.GONE);
                    }
                }
            });
        }
    };

    {
        try {
            mSocket = IO.socket("https://gymbe.herokuapp.com/");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public ChatViewModel(ChatActivity mActivity, ActivityChatBinding activityChatBinding) {
        instance = this;
        this.mActivity = mActivity;
        this.activityChatBinding = activityChatBinding;

        connectSocket();

        initViews();
    }

    private void initViews() {

        chatId = mActivity.getIntent().getStringExtra("chatId");

        setAdapter();

        activityChatBinding.btnBack.setOnClickListener(v -> mActivity.onBackPressed());

        activityChatBinding.userPicture.setOnClickListener(v -> {
            if (!otherUser.getCoach()) {
                mActivity.startActivity(new Intent(mActivity, ProfileActivity.class)
                        .putExtra("user", otherUser));
            } else {
                mActivity.startActivity(new Intent(mActivity, CoachProfileActivity.class)
                        .putExtra("coach", otherUser));
            }
        });

        activityChatBinding.btnUpload.setOnClickListener(v -> {
            PickImageDialog.build(new PickSetup()
                    .setWidth(80)
                    .setHeight(80))
                    .setOnPickResult(r -> {

                        Uri uri = Uri.fromFile(new File(r.getPath()));
                        pictureURI = uri.toString();
                        picturePath = r.getPath();

                        Bitmap bitmapCompressed = ImageUtilities.getCompressedBitmap(r.getPath());
                        Uri imageUri = ImageUtilities.getImageUri(mActivity, bitmapCompressed);
                        picturePath = ImageUtilities.getRealPathFromURI(imageUri);

                        UploadCallback callback = new UploadCallback() {
                            @Override
                            public void onStart(String requestId) {
                            }

                            @Override
                            public void onProgress(String requestId, long bytes, long totalBytes) {
                            }

                            @Override
                            public void onSuccess(String requestId, Map resultData) {
                                Log.d(TAG, "onSuccess: " + resultData.toString());

                                Message message = new Message();
                                message.setType("link");
                                message.setChatId(chatId);
                                message.setText(resultData.get("url").toString());
                                message.setUser(Globals.getUsage().sharedPreferencesEditor.getUser());
                                saveMessage(message);
                            }

                            @Override
                            public void onError(String requestId, ErrorInfo error) {
                                Toast.makeText(mActivity, "Image Uploading Failed", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onReschedule(String requestId, ErrorInfo error) {

                            }
                        };
                        String requestId = MediaManager.get().upload(picturePath).unsigned("mdczte2b").callback(callback).dispatch();

                    }).show(mActivity);
        });

        activityChatBinding.btnSend.setOnClickListener(v -> {
            if (activityChatBinding.etMessageBox.length() > 0) {


                Message message = new Message();
                message.setType("text");
                message.setChatId(chatId);
                message.setText(activityChatBinding.etMessageBox.getText().toString());
                message.setUser(Globals.getUsage().sharedPreferencesEditor.getUser());
                saveMessage(message);

                activityChatBinding.etMessageBox.setText("");
            }
        });

        activityChatBinding.etMessageBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    Globals.hideKeyboard(mActivity);
                    if (activityChatBinding.etMessageBox.length() > 0) {

                        Message message = new Message();
                        message.setType("text");
                        message.setChatId(chatId);
                        message.setText(activityChatBinding.etMessageBox.getText().toString());
                        message.setUser(Globals.getUsage().sharedPreferencesEditor.getUser());
                        saveMessage(message);

                        activityChatBinding.etMessageBox.setText("");

                    }
                    return true;
                }

                return false;
            }
        });

        activityChatBinding.etMessageBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    String typing = Globals.getUsage().sharedPreferencesEditor.getUser().getFirstName() + " is typing";
                    Typing typing1 = new Typing();
                    typing1.setId(Globals.getUsage().sharedPreferencesEditor.getUser().getId());
                    typing1.setTyping(typing);
                    mSocket.emit("userTyping", new Gson().toJson(typing1));
                } else if (s.length() == 0) {
                    Typing typing1 = new Typing();
                    typing1.setId("");
                    typing1.setTyping("");
                    mSocket.emit("userTyping", new Gson().toJson(typing1));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    public void setAdapter() {

        new ServerHelper(mActivity).getChat(chatId, new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success) {

                    chatAdapter = new ChatAdapter(getChatResponse.getData().getMessages());
                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
                    mLayoutManager.setStackFromEnd(true);
                    activityChatBinding.messageList.setLayoutManager(mLayoutManager);
                    activityChatBinding.messageList.setAdapter(chatAdapter);

                    if (!isSet) {
                        isSet = true;

                        if (!Globals.getUsage().sharedPreferencesEditor.getUser().getId().equals(getChatResponse.getData().getFrom().getId()))
                            otherUser = getChatResponse.getData().getFrom();
                        else
                            otherUser = getChatResponse.getData().getTo();
                        activityChatBinding.userName.setText(otherUser.getFirstName() + " " + otherUser.getLastName());
                        Picasso.with(activityChatBinding.userPicture.getContext())
                                .load(otherUser.getDisplayImage())
                                .placeholder(R.drawable.placeholder)
                                .centerCrop()
                                .resize(200, 200)
                                .into(activityChatBinding.userPicture);
                    }
                }
            }
        });
    }


    private void connectSocket() {

        mSocket.on("isTyping", isTyping);
        mSocket.connect();

    }

    public void disconnectSocket() {
        mSocket.off("isTyping", isTyping);
    }

    private void saveMessage(Message message) {
        new ServerHelper(mActivity).saveMessage(message, new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success) {
                    mSocket.emit("sendMessage", new Gson().toJson(message));
//                    Toast.makeText(mActivity, "Message saved", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
