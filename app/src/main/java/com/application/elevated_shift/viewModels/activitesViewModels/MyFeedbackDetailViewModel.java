package com.application.elevated_shift.viewModels.activitesViewModels;

import android.content.Intent;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.application.elevated_shift.R;
import com.application.elevated_shift.adapters.CommentsAdapter;
import com.application.elevated_shift.databinding.ActivityMyFeedbackDetailBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.feedback.Comment;
import com.application.elevated_shift.models.feedback.Datum;
import com.application.elevated_shift.models.feedback.SingleFeedback;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.ui.activities.CoachProfileActivity;
import com.application.elevated_shift.ui.activities.MyFeedbackDetailActivity;
import com.application.elevated_shift.ui.activities.ProfileActivity;
import com.application.elevated_shift.ui.activities.UserProfileActivity;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import androidx.recyclerview.widget.LinearLayoutManager;

public class MyFeedbackDetailViewModel {

    public static MyFeedbackDetailViewModel instance;
    public SingleFeedback singleFeedback;
    MyFeedbackDetailActivity mActivity;
    ActivityMyFeedbackDetailBinding activityMyFeedbackDetailBinding;
    private Datum feedback;
    private Socket mSocket;
    private CommentsAdapter commentAdapter;

    {
        try {
            mSocket = IO.socket("https://gymbe.herokuapp.com/");
        } catch (
                URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public MyFeedbackDetailViewModel(MyFeedbackDetailActivity mActivity, ActivityMyFeedbackDetailBinding activityMyFeedbackDetailBinding) {

        instance = this;
        this.mActivity = mActivity;
        this.activityMyFeedbackDetailBinding = activityMyFeedbackDetailBinding;
        mSocket.connect();

        initViews();
    }

    private void initViews() {

        feedback = (Datum) mActivity.getIntent().getSerializableExtra("feedback");
        populateValues();

        activityMyFeedbackDetailBinding.btnBack.setOnClickListener(v -> {
            mActivity.onBackPressed();
        });

        activityMyFeedbackDetailBinding.commentBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    Globals.hideKeyboard(mActivity);
                    sendComment();
                    return true;
                }

                return false;
            }
        });

        activityMyFeedbackDetailBinding.btnSendComment.setOnClickListener(view -> {
            sendComment();
        });

        setAdapter(feedback.getComments());

    }

    private void sendComment() {
        if (activityMyFeedbackDetailBinding.commentBox.length() != 0) {

            Comment comment = new Comment();
            comment.setCommentText(activityMyFeedbackDetailBinding.commentBox.getText().toString());
            activityMyFeedbackDetailBinding.commentBox.setText("");

            new ServerHelper(mActivity).addComment(comment, feedback.getId(), new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        new ServerHelper(mActivity).getFeedback(feedback.getId(), new HandlerDataSync() {
                            @Override
                            public void onSyncSuccessful(boolean success, boolean isDataError) {
                                if (success) {
                                    feedback.getComments().add(singleFeedback.getData().getComments().get(singleFeedback.getData().getComments().size() - 1));
//                                    setAdapter(feedback.getComments());
                                    mSocket.emit("comment", new Gson().toJson(feedback));
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    public void setAdapter(List<Comment> comments) {

        if (comments.size() == 0)
            return;

        commentAdapter = new CommentsAdapter(comments, mActivity);
        activityMyFeedbackDetailBinding.commentList.setLayoutManager(new LinearLayoutManager(mActivity));
        activityMyFeedbackDetailBinding.commentList.setAdapter(commentAdapter);

    }

    private void populateValues() {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(Constants.FORMAT_DATE_TIME, Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(feedback.getCreatedAt());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(TimeZone.getDefault());
        String formattedDate = df.format(Objects.requireNonNull(date));
        try {
            calendar.setTime(Objects.requireNonNull(new SimpleDateFormat(Constants.FORMAT_DATE_TIME).parse(formattedDate)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        activityMyFeedbackDetailBinding.date.setText(new SimpleDateFormat("hh:mm - dd/MM").format(calendar.getTime()));

        if (feedback.getFeedbackImageUrl() != null) {
            Picasso.with(mActivity)
                    .load(feedback.getFeedbackImageUrl())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .resize(500, 500)
                    .into(activityMyFeedbackDetailBinding.picture);
        } else {
            activityMyFeedbackDetailBinding.picture.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.placeholder));
        }

        Picasso.with(mActivity)
                .load(feedback.getFeedbackGivenBy().getDisplayImage())
                .placeholder(R.drawable.placeholder)
                .centerCrop()
                .resize(100, 100)
                .into(activityMyFeedbackDetailBinding.dp);
        activityMyFeedbackDetailBinding.username.setText(feedback.getFeedbackGivenBy().getName());
        activityMyFeedbackDetailBinding.feedbackMessage.setText(feedback.getFeedbackText());

        activityMyFeedbackDetailBinding.dp.setOnClickListener(v -> {
            if (!feedback.getFeedbackGivenBy().getId().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId())) {
                if (!feedback.getFeedbackGivenBy().getCoach()) {
                    mActivity.startActivity(new Intent(mActivity, ProfileActivity.class)
                            .putExtra("user", feedback.getFeedbackGivenBy()));
                } else {
                    mActivity.startActivity(new Intent(mActivity, CoachProfileActivity.class)
                            .putExtra("coach", feedback.getFeedbackGivenBy()));
                }
            } else {
                mActivity.startActivity(new Intent(mActivity, UserProfileActivity.class));
            }
        });

    }

}
