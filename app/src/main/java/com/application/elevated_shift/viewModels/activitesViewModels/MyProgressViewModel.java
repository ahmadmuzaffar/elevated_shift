package com.application.elevated_shift.viewModels.activitesViewModels;

import com.application.elevated_shift.adapters.ReviewAdapter;
import com.application.elevated_shift.databinding.ActivityMyProgressBinding;
import com.application.elevated_shift.databinding.ActivityMyReviewsBinding;
import com.application.elevated_shift.models.Review;
import com.application.elevated_shift.ui.activities.MyProgressActivity;

import java.util.ArrayList;
import java.util.List;

public class MyProgressViewModel {

    MyProgressActivity mActivity;
    ActivityMyProgressBinding activityMyProgressBinding;
    List<Review> reviewList = new ArrayList<>();
    ReviewAdapter reviewAdapter;

    public MyProgressViewModel(MyProgressActivity mActivity, ActivityMyProgressBinding activityMyProgressBinding) {
        this.mActivity = mActivity;
        this.activityMyProgressBinding = activityMyProgressBinding;

        initViews();
    }

    private void initViews() {

        activityMyProgressBinding.btnBack.setOnClickListener(v -> mActivity.onBackPressed());

    }

}
