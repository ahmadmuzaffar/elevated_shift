package com.application.elevated_shift.viewModels.activitesViewModels;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityMyAchievementBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.gallery.GalleryResponse;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.ui.activities.MyAchievementActivity;
import com.application.elevated_shift.ui.activities.MyReviewsActivity;
import com.application.elevated_shift.utils.ImageUtilities;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.etiennelawlor.imagegallery.library.ImageGalleryFragment;
import com.etiennelawlor.imagegallery.library.activities.FullScreenImageGalleryActivity;
import com.etiennelawlor.imagegallery.library.activities.ImageGalleryActivity;
import com.etiennelawlor.imagegallery.library.adapters.FullScreenImageGalleryAdapter;
import com.etiennelawlor.imagegallery.library.adapters.ImageGalleryAdapter;
import com.etiennelawlor.imagegallery.library.enums.PaletteColorType;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.palette.graphics.Palette;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class MyAchievementViewModel implements ImageGalleryAdapter.ImageThumbnailLoader, FullScreenImageGalleryAdapter.FullScreenImageLoader {

    public static MyAchievementViewModel instance;
    public GalleryResponse galleryResponse = new GalleryResponse();
    List<String> images = new ArrayList<>();
    MyAchievementActivity mActivity;
    ActivityMyAchievementBinding activityMyAchievementBinding;
    private PaletteColorType paletteColorType;
    private String pictureURI;
    private String picturePath;
    private String imageUrl;

    public MyAchievementViewModel(MyAchievementActivity mActivity, ActivityMyAchievementBinding activityMyAchievementBinding) {
        instance = this;
        this.mActivity = mActivity;
        this.activityMyAchievementBinding = activityMyAchievementBinding;

        initViews();
    }

    public void onResume() {
        Data user = Globals.getUsage().sharedPreferencesEditor.getUser();
        activityMyAchievementBinding.tvUsername.setText(user.getName());
        Picasso.with(activityMyAchievementBinding.dp.getContext())
                .load(user.getDisplayImage())
                .placeholder(R.drawable.placeholder)
                .centerCrop()
                .resize(200, 200)
                .into(activityMyAchievementBinding.dp);
    }

    private void initViews() {

        ImageGalleryActivity.setImageThumbnailLoader(this);
        ImageGalleryFragment.setImageThumbnailLoader(this);
        FullScreenImageGalleryActivity.setFullScreenImageLoader(this);

        // optionally set background color using Palette for full screen images
        paletteColorType = PaletteColorType.VIBRANT;

        activityMyAchievementBinding.btnBack.setOnClickListener(v -> mActivity.onBackPressed());

        activityMyAchievementBinding.btnReviews.setOnClickListener(v -> {
            mActivity.startActivity(new Intent(mActivity, MyReviewsActivity.class));
        });

        activityMyAchievementBinding.btnProgress.setOnClickListener(v -> {
            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(mActivity, SweetAlertDialog.WARNING_TYPE);
            sweetAlertDialog.setTitle("Information");
            sweetAlertDialog.setContentText("This feature is currently not available");
            sweetAlertDialog.setConfirmButton("OK", new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sweetAlertDialog.dismissWithAnimation();
                }
            });
            sweetAlertDialog.showCancelButton(false);
            sweetAlertDialog.show();
//            mActivity.startActivity(new Intent(mActivity, MyProgressActivity.class));
        });

        activityMyAchievementBinding.btnGallery.setOnClickListener(v -> {

            Intent intent = new Intent(mActivity, ImageGalleryActivity.class);

//                        String[] images = mActivity.getResources().getStringArray(R.array.unsplash_images);
            Bundle bundle = new Bundle();
            bundle.putStringArrayList(ImageGalleryActivity.KEY_IMAGES, new ArrayList<>(Globals.getUsage().sharedPreferencesEditor.getUser().getAchievements()));
            bundle.putString(ImageGalleryActivity.KEY_TITLE, "My Gallery");
            intent.putExtras(bundle);
            mActivity.startActivity(intent);

//            new ServerHelper(mActivity).getGallery(new HandlerDataSync() {
//                @Override
//                public void onSyncSuccessful(boolean success, boolean isDataError) {
//                    if (success) {
//                        Intent intent = new Intent(mActivity, ImageGalleryActivity.class);
//
////                        String[] images = mActivity.getResources().getStringArray(R.array.unsplash_images);
//                        Bundle bundle = new Bundle();
//                        bundle.putStringArrayList(ImageGalleryActivity.KEY_IMAGES, new ArrayList<>(galleryResponse.getData().get(0).getImages()));
//                        bundle.putString(ImageGalleryActivity.KEY_TITLE, "My Gallery");
//                        intent.putExtras(bundle);
//                        mActivity.startActivity(intent);
//                    }
//                }
//            });

        });

        activityMyAchievementBinding.btnAddAchievement.setOnClickListener(v -> {
            PickImageDialog.build(new PickSetup()
                    .setWidth(80)
                    .setHeight(80))
                    .setOnPickResult(r -> {

                        Uri uri = Uri.fromFile(new File(r.getPath()));
                        pictureURI = uri.toString();
                        picturePath = r.getPath();

                        Bitmap bitmapCompressed = ImageUtilities.getCompressedBitmap(r.getPath());
                        Uri imageUri = ImageUtilities.getImageUri(mActivity, bitmapCompressed);
                        picturePath = ImageUtilities.getRealPathFromURI(imageUri);

                        UploadCallback callback = new UploadCallback() {
                            @Override
                            public void onStart(String requestId) {

                            }

                            @Override
                            public void onProgress(String requestId, long bytes, long totalBytes) {

                            }

                            @Override
                            public void onSuccess(String requestId, Map resultData) {

                                imageUrl = resultData.get("secure_url").toString();
                                if (!TextUtils.isEmpty(imageUrl)) {

                                    List<String> list = Globals.getUsage().sharedPreferencesEditor.getUser().getAchievements();
                                    list.add(imageUrl);

                                    HashMap<String, List<String>> map = new HashMap<>();
                                    map.put("achievements", list);

                                    new ServerHelper(mActivity).updateUser(map, Globals.getUsage().sharedPreferencesEditor.getUser().getId(), new HandlerDataSync() {
                                        @Override
                                        public void onSyncSuccessful(boolean success, boolean isDataError) {
                                            if (success) {
                                                Toast.makeText(mActivity, "Achievement Uploaded", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });

//                                    JsonObject jsonObject = new JsonObject();
//                                    jsonObject.addProperty("image", imageUrl);
//
//                                    new ServerHelper(mActivity).addPicture(jsonObject, Globals.getUsage().sharedPreferencesEditor.getUser().getId(), new HandlerDataSync() {
//                                        @Override
//                                        public void onSyncSuccessful(boolean success, boolean isDataError) {
//                                            if (success) {
//                                                Toast.makeText(mActivity, "Achievement Uploaded", Toast.LENGTH_SHORT).show();
//                                            }
//                                        }
//                                    });

                                }

                            }

                            @Override
                            public void onError(String requestId, ErrorInfo error) {
                                Toast.makeText(mActivity, "Image Uploading Failed", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onReschedule(String requestId, ErrorInfo error) {

                            }
                        };
                        String requestId = MediaManager.get().upload(picturePath).unsigned("mdczte2b").callback(callback).dispatch();

                    }).show(mActivity);
        });

    }

    @Override
    public void loadImageThumbnail(final ImageView iv, String imageUrl, int dimension) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(iv.getContext())
                    .load(imageUrl)
                    .resize(dimension, dimension)
                    .centerCrop()
                    .into(iv);
        } else {
            iv.setImageDrawable(null);
        }
    }

    @Override
    public void loadFullScreenImage(final ImageView iv, String imageUrl, int width, final LinearLayout bgLinearLayout) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(iv.getContext())
                    .load(imageUrl)
                    .resize(width, 0)
                    .into(iv, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap bitmap = ((BitmapDrawable) iv.getDrawable()).getBitmap();
                            Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                                public void onGenerated(Palette palette) {
                                    applyPalette(palette, bgLinearLayout);
                                }
                            });
                        }

                        @Override
                        public void onError() {

                        }
                    });
        } else {
            iv.setImageDrawable(null);
        }
    }
    // endregion

    // region Helper Methods
    private void applyPalette(Palette palette, ViewGroup viewGroup) {
        int bgColor = getBackgroundColor(palette);
        if (bgColor != -1)
            viewGroup.setBackgroundColor(bgColor);
    }

    private void applyPalette(Palette palette, View view) {
        int bgColor = getBackgroundColor(palette);
        if (bgColor != -1)
            view.setBackgroundColor(bgColor);
    }

    private int getBackgroundColor(Palette palette) {
        int bgColor = -1;

        int vibrantColor = palette.getVibrantColor(0x000000);
        int lightVibrantColor = palette.getLightVibrantColor(0x000000);
        int darkVibrantColor = palette.getDarkVibrantColor(0x000000);

        int mutedColor = palette.getMutedColor(0x000000);
        int lightMutedColor = palette.getLightMutedColor(0x000000);
        int darkMutedColor = palette.getDarkMutedColor(0x000000);

        if (paletteColorType != null) {
            switch (paletteColorType) {
                case VIBRANT:
                    if (vibrantColor != 0) { // primary option
                        bgColor = vibrantColor;
                    } else if (lightVibrantColor != 0) { // fallback options
                        bgColor = lightVibrantColor;
                    } else if (darkVibrantColor != 0) {
                        bgColor = darkVibrantColor;
                    } else if (mutedColor != 0) {
                        bgColor = mutedColor;
                    } else if (lightMutedColor != 0) {
                        bgColor = lightMutedColor;
                    } else if (darkMutedColor != 0) {
                        bgColor = darkMutedColor;
                    }
                    break;
                case LIGHT_VIBRANT:
                    if (lightVibrantColor != 0) { // primary option
                        bgColor = lightVibrantColor;
                    } else if (vibrantColor != 0) { // fallback options
                        bgColor = vibrantColor;
                    } else if (darkVibrantColor != 0) {
                        bgColor = darkVibrantColor;
                    } else if (mutedColor != 0) {
                        bgColor = mutedColor;
                    } else if (lightMutedColor != 0) {
                        bgColor = lightMutedColor;
                    } else if (darkMutedColor != 0) {
                        bgColor = darkMutedColor;
                    }
                    break;
                case DARK_VIBRANT:
                    if (darkVibrantColor != 0) { // primary option
                        bgColor = darkVibrantColor;
                    } else if (vibrantColor != 0) { // fallback options
                        bgColor = vibrantColor;
                    } else if (lightVibrantColor != 0) {
                        bgColor = lightVibrantColor;
                    } else if (mutedColor != 0) {
                        bgColor = mutedColor;
                    } else if (lightMutedColor != 0) {
                        bgColor = lightMutedColor;
                    } else if (darkMutedColor != 0) {
                        bgColor = darkMutedColor;
                    }
                    break;
                case MUTED:
                    if (mutedColor != 0) { // primary option
                        bgColor = mutedColor;
                    } else if (lightMutedColor != 0) { // fallback options
                        bgColor = lightMutedColor;
                    } else if (darkMutedColor != 0) {
                        bgColor = darkMutedColor;
                    } else if (vibrantColor != 0) {
                        bgColor = vibrantColor;
                    } else if (lightVibrantColor != 0) {
                        bgColor = lightVibrantColor;
                    } else if (darkVibrantColor != 0) {
                        bgColor = darkVibrantColor;
                    }
                    break;
                case LIGHT_MUTED:
                    if (lightMutedColor != 0) { // primary option
                        bgColor = lightMutedColor;
                    } else if (mutedColor != 0) { // fallback options
                        bgColor = mutedColor;
                    } else if (darkMutedColor != 0) {
                        bgColor = darkMutedColor;
                    } else if (vibrantColor != 0) {
                        bgColor = vibrantColor;
                    } else if (lightVibrantColor != 0) {
                        bgColor = lightVibrantColor;
                    } else if (darkVibrantColor != 0) {
                        bgColor = darkVibrantColor;
                    }
                    break;
                case DARK_MUTED:
                    if (darkMutedColor != 0) { // primary option
                        bgColor = darkMutedColor;
                    } else if (mutedColor != 0) { // fallback options
                        bgColor = mutedColor;
                    } else if (lightMutedColor != 0) {
                        bgColor = lightMutedColor;
                    } else if (vibrantColor != 0) {
                        bgColor = vibrantColor;
                    } else if (lightVibrantColor != 0) {
                        bgColor = lightVibrantColor;
                    } else if (darkVibrantColor != 0) {
                        bgColor = darkVibrantColor;
                    }
                    break;
                default:
                    break;
            }
        }

        return bgColor;
    }

}
