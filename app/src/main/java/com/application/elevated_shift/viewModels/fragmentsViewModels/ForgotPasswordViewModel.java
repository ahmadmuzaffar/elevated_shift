package com.application.elevated_shift.viewModels.fragmentsViewModels;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.application.elevated_shift.databinding.FragmentForgotPasswordBinding;
import com.application.elevated_shift.databinding.FragmentLoginBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.user.ChangePasswordBody;
import com.application.elevated_shift.models.user.ForgotPasswordBody;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.ui.activities.StartActivity;

public class ForgotPasswordViewModel {

    FragmentForgotPasswordBinding fragmentForgotPasswordBinding;
    Activity mActivity;

    public ForgotPasswordViewModel(FragmentForgotPasswordBinding fragmentForgotPasswordBinding, Activity mActivity) {
        this.fragmentForgotPasswordBinding = fragmentForgotPasswordBinding;
        this.mActivity = mActivity;

        initViews();
    }

    private void initViews() {

        fragmentForgotPasswordBinding.btnSendPin.setOnClickListener(v -> {
            if (fragmentForgotPasswordBinding.llChangePassword.getVisibility() == View.GONE) {
                if (Globals.isEmailValid(fragmentForgotPasswordBinding.etUsername.getText().toString())) {

                    ForgotPasswordBody forgotPasswordBody = new ForgotPasswordBody();
                    forgotPasswordBody.setEmail(fragmentForgotPasswordBinding.etUsername.getText().toString());

                    new ServerHelper(mActivity).forgotPassword(forgotPasswordBody, new HandlerDataSync() {
                        @Override
                        public void onSyncSuccessful(boolean success, boolean isDataError) {
                            if (success) {
                                Toast.makeText(mActivity, "Kindly check your registered email for verification Code", Toast.LENGTH_LONG).show();
//                                fragmentForgotPasswordBinding.etUsername.setText("");
                                fragmentForgotPasswordBinding.llChangePassword.setVisibility(View.VISIBLE);
                                fragmentForgotPasswordBinding.btnSendPin.setText("Change Password");
                                Globals.hideKeyboard(mActivity);
                            }
                        }
                    });
                }
            } else {
                if (Globals.isEmailValid(fragmentForgotPasswordBinding.etUsername.getText().toString()) && fragmentForgotPasswordBinding.etPin.length() == 6 && fragmentForgotPasswordBinding.etNewPassword.length() > 5) {

                    ChangePasswordBody changePasswordBody = new ChangePasswordBody();
                    changePasswordBody.setEmail(fragmentForgotPasswordBinding.etUsername.getText().toString().trim());
                    changePasswordBody.setVerificationCode(fragmentForgotPasswordBinding.etPin.getText().toString());
                    changePasswordBody.setNewPassword(fragmentForgotPasswordBinding.etNewPassword.getText().toString().trim());

                    new ServerHelper(mActivity).changePassword(changePasswordBody, new HandlerDataSync() {
                        @Override
                        public void onSyncSuccessful(boolean success, boolean isDataError) {
                            if (success) {
                                Toast.makeText(mActivity, "Password Updated!", Toast.LENGTH_LONG).show();
                                mActivity.startActivity(new Intent(mActivity, StartActivity.class));
                                mActivity.finish();
                            }
                        }
                    });
                } else
                    Toast.makeText(mActivity, "Please enter valid values", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
