package com.application.elevated_shift.viewModels.fragmentsViewModels;

import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.FragmentLoginBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.interfaces.CallBackFragment;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.models.user.LoginBody;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.services.MessagesService;
import com.application.elevated_shift.ui.activities.AppFrontActivity;

public class LoginViewModel implements View.OnClickListener {

    FragmentLoginBinding fragmentLoginBinding;
    Activity mActivity;
    boolean isToggled = false;
    private CallBackFragment callBackFragment;

    public LoginViewModel(FragmentLoginBinding fragmentLoginBinding, Activity mActivity) {
        this.fragmentLoginBinding = fragmentLoginBinding;
        this.mActivity = mActivity;
        callBackFragment = (CallBackFragment) mActivity;

        initViews();
    }

    private void initViews() {

        fragmentLoginBinding.etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    isToggled = false;
                    fragmentLoginBinding.togglePassword.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_action_visible));
                    fragmentLoginBinding.togglePassword.setVisibility(View.GONE);
                } else {
                    if (fragmentLoginBinding.togglePassword.getVisibility() == View.GONE)
                        fragmentLoginBinding.togglePassword.setVisibility(View.VISIBLE);
                }
            }
        });

        fragmentLoginBinding.togglePassword.setOnClickListener(this);
        fragmentLoginBinding.tvForgotPassword.setOnClickListener(this);
        fragmentLoginBinding.tvSignUp.setOnClickListener(this);
        fragmentLoginBinding.btnLogin.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.togglePassword:
                isToggled = !isToggled;
                if (isToggled) {
                    fragmentLoginBinding.togglePassword.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_action_hide));
                    fragmentLoginBinding.etPassword.setTransformationMethod(new HideReturnsTransformationMethod());
                    fragmentLoginBinding.etPassword.setSelection(fragmentLoginBinding.etPassword.length());
                } else {
                    fragmentLoginBinding.togglePassword.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_action_visible));
                    fragmentLoginBinding.etPassword.setTransformationMethod(new PasswordTransformationMethod());
                    fragmentLoginBinding.etPassword.setSelection(fragmentLoginBinding.etPassword.length());
                }
                break;
            case R.id.btnLogin:
                if (validateFields()) {

                    LoginBody loginBody = new LoginBody();
                    loginBody.setEmail(fragmentLoginBinding.etUsername.getText().toString());
                    loginBody.setPassword(fragmentLoginBinding.etPassword.getText().toString());

                    new ServerHelper(mActivity).login(loginBody, new HandlerDataSync() {
                        @Override
                        public void onSyncSuccessful(boolean success, boolean isDataError) {
                            if (success) {

                                new ServerHelper(mActivity).getUsers(new HandlerDataSync() {
                                    @Override
                                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                                        if (success) {
                                            if (!Globals.isMyServiceRunning(MessagesService.class, mActivity))
                                                mActivity.startService(new Intent(mActivity, MessagesService.class));

                                            Data user = Globals.getUsage().sharedPreferencesEditor.getUser();
                                            user.setPassword(fragmentLoginBinding.etPassword.getText().toString());

                                            Globals.getUsage().sharedPreferencesEditor.setLoggedIn(true);
                                            Globals.getUsage().sharedPreferencesEditor.setUser(user);

                                            mActivity.startActivity(new Intent(mActivity, AppFrontActivity.class));
                                            mActivity.finish();
                                        }
                                    }
                                });

                            }
                        }
                    });

                }
                break;
            case R.id.tvForgotPassword:
                callBackFragment.changeFragment(1);
                break;
            case R.id.tvSignUp:
                callBackFragment.changeFragment(0);
                break;
        }
    }

    private boolean validateFields() {
        if (fragmentLoginBinding.etUsername.length() == 0) {
            Toast.makeText(mActivity, "Please enter username", Toast.LENGTH_SHORT).show();
            fragmentLoginBinding.etUsername.requestFocus();
            return false;
        }
        if (fragmentLoginBinding.etPassword.length() == 0) {
            Toast.makeText(mActivity, "Please enter password", Toast.LENGTH_SHORT).show();
            fragmentLoginBinding.etPassword.requestFocus();
            return false;
        }
        if (fragmentLoginBinding.etPassword.length() < 6) {
            Toast.makeText(mActivity, "Enter password atleast 6 characters long", Toast.LENGTH_SHORT).show();
            fragmentLoginBinding.etPassword.requestFocus();
            return false;
        }
        return true;
    }
}
