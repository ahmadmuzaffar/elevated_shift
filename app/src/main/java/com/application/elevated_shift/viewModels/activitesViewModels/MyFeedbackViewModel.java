package com.application.elevated_shift.viewModels.activitesViewModels;

import android.view.View;

import com.application.elevated_shift.adapters.FeedbackAdapter;
import com.application.elevated_shift.databinding.ActivityMyFeedbackBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.models.feedback.FeedbackResponse;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.ui.activities.MyFeedbackActivity;

import java.util.Collections;

import androidx.recyclerview.widget.LinearLayoutManager;

public class MyFeedbackViewModel {

    public static MyFeedbackViewModel instance;
    MyFeedbackActivity mActivity;
    ActivityMyFeedbackBinding activityMyFeedbackBinding;
    public FeedbackResponse feedbackResponse = new FeedbackResponse();
    FeedbackAdapter feedbackAdapter;

    public MyFeedbackViewModel(MyFeedbackActivity mActivity, ActivityMyFeedbackBinding activityMyFeedbackBinding) {
        instance = this;
        this.mActivity = mActivity;
        this.activityMyFeedbackBinding = activityMyFeedbackBinding;

        initViews();
    }

    private void initViews() {

        activityMyFeedbackBinding.btnBack.setOnClickListener(v -> mActivity.onBackPressed());
        setAdapter();

    }

    public void setAdapter() {

        new ServerHelper(mActivity).getFeedbacks(new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success) {
                    if (feedbackResponse.getData().size() == 0) {
                        activityMyFeedbackBinding.noFeedback.setVisibility(View.VISIBLE);
                        activityMyFeedbackBinding.feedbackList.setVisibility(View.GONE);
                        return;
                    } else {
                        activityMyFeedbackBinding.noFeedback.setVisibility(View.GONE);
                        activityMyFeedbackBinding.feedbackList.setVisibility(View.VISIBLE);
                    }

                    Collections.reverse(feedbackResponse.getData());

                    feedbackAdapter = new FeedbackAdapter(feedbackResponse.getData(), mActivity);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
                    activityMyFeedbackBinding.feedbackList.setLayoutManager(linearLayoutManager);
                    activityMyFeedbackBinding.feedbackList.setAdapter(feedbackAdapter);
                }
            }
        });
    }


}
