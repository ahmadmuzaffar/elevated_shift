package com.application.elevated_shift.viewModels.activitesViewModels;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.text.Html;
import android.text.Spannable;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.adapters.StoriesAdapter;
import com.application.elevated_shift.databinding.ActivityMyStoriesBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.models.Story;
import com.application.elevated_shift.models.collection.CollectionResponse;
import com.application.elevated_shift.models.collection.Datum;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.sharedPreference.SharedPreferencesEditor;
import com.application.elevated_shift.ui.activities.MyStoriesActivity;
import com.application.elevated_shift.utils.ImageUtilities;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.squareup.picasso.Picasso;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import androidx.recyclerview.widget.LinearLayoutManager;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;

public class MyStoriesViewModel {

    public static MyStoriesViewModel instance;
    MyStoriesActivity mActivity;
    ActivityMyStoriesBinding activityMyStoriesBinding;
    public CollectionResponse collectionResponse = new CollectionResponse();
    StoriesAdapter storiesAdapter;
    private String picturePath;
    private String pictureURI;
    private String imageUrl = "";
    private Datum collection;

    public MyStoriesViewModel(MyStoriesActivity mActivity, ActivityMyStoriesBinding activityMyStoriesBinding) {
        instance = this;
        this.mActivity = mActivity;
        this.activityMyStoriesBinding = activityMyStoriesBinding;

        initViews();
    }

    private void initViews() {

        collection = (Datum) mActivity.getIntent().getSerializableExtra("collection");
        activityMyStoriesBinding.storyTitle.setText(collection.getTitle());


        activityMyStoriesBinding.btnBack.setOnClickListener(v -> mActivity.onBackPressed());

        activityMyStoriesBinding.btnAdd.setOnClickListener(v -> {
            showDialog();
        });

        if (!new SharedPreferencesEditor(mActivity).isTutorialViewed(Constants.STORIES_ADD))
            showTutorials();
        else
            setAdapter();
    }

    private void showTutorials() {

        new GuideView.Builder(mActivity)
                .setTitle("Add")
                .setContentSpan((Spannable) Html.fromHtml("<font color='#0B304F'>Tap on plus icon to add new story</p>"))
                .setGravity(Gravity.center)
                .setTargetView(activityMyStoriesBinding.btnAdd)
                .setDismissType(DismissType.outside)
                .setGuideListener(new GuideListener() {
                    @Override
                    public void onDismiss(View view) {
                        new SharedPreferencesEditor(mActivity).setTutorialViewed(Constants.STORIES_ADD, true);
                        setAdapter();
                    }
                })
                .build()
                .show();

    }

    public void setAdapter() {

        new ServerHelper(mActivity).getStories(collection.getId(), new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (collectionResponse.getData().getStories() == null)
                    return;
                if (collectionResponse.getData().getStories().size() == 0) {
                    activityMyStoriesBinding.count.setText("0");
                    activityMyStoriesBinding.noStory.setVisibility(View.VISIBLE);
                    activityMyStoriesBinding.storiesList.setVisibility(View.GONE);
                } else {
                    activityMyStoriesBinding.count.setText(collectionResponse.getData().getStories().size() + "");
                    activityMyStoriesBinding.noStory.setVisibility(View.GONE);
                    activityMyStoriesBinding.storiesList.setVisibility(View.VISIBLE);
                }

                storiesAdapter = new StoriesAdapter(collectionResponse.getData().getStories(), mActivity);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
                activityMyStoriesBinding.storiesList.setLayoutManager(linearLayoutManager);
                activityMyStoriesBinding.storiesList.setAdapter(storiesAdapter);
            }
        });
    }

    private void showDialog() {
        final Dialog d = new Dialog(mActivity);
        d.setContentView(R.layout.dialog_add_story);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setCancelable(false);
        d.setCanceledOnTouchOutside(false);

        WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();
        lWindowParams.copyFrom(d.getWindow().getAttributes());
        lWindowParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        d.show();
        d.getWindow().setAttributes(lWindowParams);

        TextView post, cancel;
        EditText message, date;
        ImageView picture, takePicture;

        message = d.findViewById(R.id.message);
        date = d.findViewById(R.id.date);
        post = d.findViewById(R.id.okay);
        cancel = d.findViewById(R.id.cancel);
        takePicture = d.findViewById(R.id.takePicture);
        picture = d.findViewById(R.id.picture);

        date.setText(new SimpleDateFormat(Constants.FORMAT_DATE_API).format(new Date()));

        takePicture.setOnClickListener(v -> {
            PickImageDialog.build(new PickSetup()
                    .setWidth(80)
                    .setHeight(80))
                    .setOnPickResult(r -> {

                        Uri uri = Uri.fromFile(new File(r.getPath()));
                        pictureURI = uri.toString();
                        picturePath = r.getPath();

                        Bitmap bitmapCompressed = ImageUtilities.getCompressedBitmap(r.getPath());
                        Uri imageUri = ImageUtilities.getImageUri(mActivity, bitmapCompressed);
                        picturePath = ImageUtilities.getRealPathFromURI(imageUri);

                        UploadCallback callback = new UploadCallback() {
                            @Override
                            public void onStart(String requestId) {

                            }

                            @Override
                            public void onProgress(String requestId, long bytes, long totalBytes) {

                            }

                            @Override
                            public void onSuccess(String requestId, Map resultData) {
                                Toast.makeText(mActivity, "Image Uploaded", Toast.LENGTH_SHORT).show();

                                imageUrl = resultData.get("secure_url").toString();
                                if (!TextUtils.isEmpty(imageUrl)) {
                                    Picasso.with(picture.getContext())
                                            .load(imageUrl)
                                            .fit()
                                            .into(picture);
                                    picture.setVisibility(View.VISIBLE);
                                    d.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                } else {
                                    picture.setVisibility(View.GONE);
                                    picture.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_upload_image_foreground));
                                }

                            }

                            @Override
                            public void onError(String requestId, ErrorInfo error) {
                                Toast.makeText(mActivity, "Image Uploading Failed", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onReschedule(String requestId, ErrorInfo error) {

                            }
                        };
                        String requestId = MediaManager.get().upload(picturePath).unsigned("mdczte2b").callback(callback).dispatch();

                    }).show(mActivity);
        });

        post.setOnClickListener(v -> {

            if (message.length() == 0 || date.length() == 0) {
                Toast.makeText(mActivity, "Please enter message to post story", Toast.LENGTH_SHORT).show();
                return;
            }

            Story story = new Story();
            story.setCollectionId(collection.getId());
            story.setStoryText(message.getText().toString());
            story.setImageUrl(imageUrl);

            new ServerHelper(mActivity).saveStory(story, new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        setAdapter();
                    }
                }
            });

            imageUrl = "";
            d.dismiss();
        });

        cancel.setOnClickListener(v -> {
            d.dismiss();
        });
    }

}
