package com.application.elevated_shift.viewModels.activitesViewModels;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.adapters.PaymentsAdapter;
import com.application.elevated_shift.databinding.ActivityMyPaymentsBinding;
import com.application.elevated_shift.databinding.ActivityMyProgressBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.payment.InvoicesRespone;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.ui.activities.MyPaymentsActivity;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

public class MyPaymentsViewModel {

    public static MyPaymentsViewModel instance;
    MyPaymentsActivity mActivity;
    ActivityMyPaymentsBinding activityMyPaymentsBinding;
    public InvoicesRespone invoicesRespone = new InvoicesRespone();
    private PaymentsAdapter paymentsAdapter;
    private String dirpath;
    private Bitmap bitmap;
    private String pathName;

    public MyPaymentsViewModel(MyPaymentsActivity mActivity, ActivityMyPaymentsBinding activityMyPaymentsBinding) {
        instance = this;
        this.mActivity = mActivity;
        this.activityMyPaymentsBinding = activityMyPaymentsBinding;

        initViews();
    }

    public void onResume() {
        Data user = Globals.getUsage().sharedPreferencesEditor.getUser();
        activityMyPaymentsBinding.tvUsername.setText(user.getName());
        Picasso.with(activityMyPaymentsBinding.dp.getContext())
                .load(user.getDisplayImage())
                .placeholder(R.drawable.placeholder)
                .centerCrop()
                .resize(200, 200)
                .into(activityMyPaymentsBinding.dp);
    }

    private void initViews() {

        activityMyPaymentsBinding.btnBack.setOnClickListener(v -> mActivity.onBackPressed());

        activityMyPaymentsBinding.generateStatement.setOnClickListener(v -> {
            bitmap = loadBitmapFromView(activityMyPaymentsBinding.layoutPrint, activityMyPaymentsBinding.layoutPrint.getWidth(), activityMyPaymentsBinding.layoutPrint.getHeight());
            createPdf();
        });

        setAdapter();

    }

    public void setAdapter() {

        new ServerHelper(mActivity).getInvoices(new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success) {
                    if (invoicesRespone.getData().size() == 0) {
                        activityMyPaymentsBinding.noPayment.setVisibility(View.VISIBLE);
                        activityMyPaymentsBinding.paymentsList.setVisibility(View.GONE);
                        activityMyPaymentsBinding.layoutHeader.setVisibility(View.GONE);
                        activityMyPaymentsBinding.div.setVisibility(View.GONE);
                    } else {
                        activityMyPaymentsBinding.noPayment.setVisibility(View.GONE);
                        activityMyPaymentsBinding.paymentsList.setVisibility(View.VISIBLE);

                        activityMyPaymentsBinding.layoutHeader.setVisibility(View.VISIBLE);
                        activityMyPaymentsBinding.div.setVisibility(View.VISIBLE);
                    }

                    paymentsAdapter = new PaymentsAdapter(invoicesRespone.getData(), mActivity);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
                    activityMyPaymentsBinding.paymentsList.setLayoutManager(linearLayoutManager);
                    activityMyPaymentsBinding.paymentsList.setAdapter(paymentsAdapter);
                }
            }
        });
    }

    private static Bitmap loadBitmapFromView(View v, int width, int height) {
        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.draw(c);

        return b;
    }

    private void createPdf() {
        WindowManager wm = (WindowManager) mActivity.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        float hight = displaymetrics.heightPixels;
        float width = displaymetrics.widthPixels;

        int convertHighet = (int) hight, convertWidth = (int) width;

        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(convertWidth, convertHighet, 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();

        Paint paint = new Paint();
        canvas.drawPaint(paint);

        bitmap = Bitmap.createScaledBitmap(bitmap, convertWidth, convertHighet, true);

        paint.setColor(Color.BLUE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        document.finishPage(page);

        File filePath;
        pathName = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + File.separator + "E-Statement" + System.currentTimeMillis() + ".pdf";
        filePath = new File(pathName);
        try {
            document.writeTo(new FileOutputStream(filePath));
            document.close();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(mActivity, "Something wrong: " + e.toString(), Toast.LENGTH_LONG).show();
        }

//        Toast.makeText(mActivity, "PDF is created!!!", Toast.LENGTH_SHORT).show();

        openGeneratedPDF();

    }

    private void openGeneratedPDF() {
        File file = new File(pathName);
        if (file.exists()) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = FileProvider.getUriForFile(mActivity, mActivity.getApplicationContext().getPackageName() + ".provider", file);
            intent.setDataAndType(uri, "application/pdf");
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            try {
                mActivity.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(mActivity, "No Application available to view pdf", Toast.LENGTH_LONG).show();
            }
        }
    }

}
