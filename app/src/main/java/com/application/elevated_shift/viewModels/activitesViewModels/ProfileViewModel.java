package com.application.elevated_shift.viewModels.activitesViewModels;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.adapters.ConnectionAdapter;
import com.application.elevated_shift.adapters.GalleryAdapter;
import com.application.elevated_shift.databinding.ActivityAllChatsBinding;
import com.application.elevated_shift.databinding.ActivityProfileBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.Review;
import com.application.elevated_shift.models.chat.ChatBody;
import com.application.elevated_shift.models.chat.ChatResponse;
import com.application.elevated_shift.models.chat.Datum;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.ui.activities.ChatActivity;
import com.application.elevated_shift.ui.activities.ProfileActivity;
import com.application.elevated_shift.utils.ImageUtilities;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.squareup.picasso.Picasso;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;

import java.io.File;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.application.elevated_shift.ui.activities.CheckoutActivity.coach;

public class ProfileViewModel {

    public static ProfileViewModel instance;
    public List<Datum> chatsList = new ArrayList<>();
    public ChatResponse chatResponse = new ChatResponse();
    ProfileActivity mActivity;
    ActivityProfileBinding activityProfileBinding;
    private Data user = new Data();
    private GalleryAdapter galleryAdapter;
    private String chatId = "";
    private ConnectionAdapter connectionAdapter;
    private String pictureURI;
    private String picturePath;
    private String imageUrl;
    private Socket mSocket;

    {
        try {
            mSocket = IO.socket("https://gymbe.herokuapp.com/");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public ProfileViewModel(ProfileActivity mActivity, ActivityProfileBinding activityProfileBinding) {

        instance = this;
        this.mActivity = mActivity;
        this.activityProfileBinding = activityProfileBinding;
        mSocket.connect();

        initViews();
    }

    private void initViews() {

        user = (Data) mActivity.getIntent().getSerializableExtra("user");

        activityProfileBinding.btnBack.setOnClickListener(v -> mActivity.onBackPressed());
        populateValues();
        setConnectionAdapter();
        setAchievementAdapter();

        new ServerHelper(mActivity).getChats(new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success) {
                    for (Datum datum : chatsList) {
                        if ((datum.getFrom().getId().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId())
                                || datum.getFrom().getId().equals(user.getId()))
                                && (datum.getTo().getId().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId())
                                || datum.getTo().getId().equals(user.getId()))) {

                            chatId = datum.getId();
                            break;
                        }
                    }
                }
            }
        });

        activityProfileBinding.btnReview.setOnClickListener(v -> {
            showDialog();
        });

        activityProfileBinding.btnFeedback.setOnClickListener(v -> {
            showFeedbackDialog();
        });

        activityProfileBinding.btnMessage.setOnClickListener(v -> {

            if (!chatId.equals("")) {
                mActivity.startActivity(new Intent(mActivity, ChatActivity.class)
                                .putExtra("chatId", chatId)
//                        .putExtra("from", user)
                );
            } else {

                ChatBody chatBody = new ChatBody();
                chatBody.setFrom(Globals.getUsage().sharedPreferencesEditor.getUser().getId());
                chatBody.setTo(user.getId());

                new ServerHelper(mActivity).createChat(chatBody, new HandlerDataSync() {
                    @Override
                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                        if (success) {
                            mActivity.startActivity(new Intent(mActivity, ChatActivity.class)
                                            .putExtra("chatId", chatResponse.getData().getChatId())
//                                    .putExtra("from", user)
                            );
                        }
                    }
                });
            }

        });

    }

    private void setConnectionAdapter() {

        if (user.getSocialLinks().size() == 0) {
            activityProfileBinding.noConnections.setVisibility(View.VISIBLE);
            activityProfileBinding.connectionList.setVisibility(View.GONE);
        } else {
            activityProfileBinding.noConnections.setVisibility(View.GONE);
            activityProfileBinding.connectionList.setVisibility(View.VISIBLE);

            connectionAdapter = new ConnectionAdapter(user.getSocialLinks());
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
            activityProfileBinding.connectionList.setLayoutManager(linearLayoutManager);
            activityProfileBinding.connectionList.setItemAnimator(new DefaultItemAnimator());
            activityProfileBinding.connectionList.setAdapter(connectionAdapter);

        }

    }

    private void setAchievementAdapter() {

        if (user.getAchievements().size() == 0) {
            activityProfileBinding.noAchievement.setVisibility(View.VISIBLE);
            activityProfileBinding.achievementList.setVisibility(View.GONE);
        } else {
            activityProfileBinding.noAchievement.setVisibility(View.GONE);
            activityProfileBinding.achievementList.setVisibility(View.VISIBLE);

            galleryAdapter = new GalleryAdapter(user.getAchievements());
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mActivity, 2, activityProfileBinding.achievementList.VERTICAL, false);
            activityProfileBinding.achievementList.setLayoutManager(mLayoutManager);
            activityProfileBinding.achievementList.setItemAnimator(new DefaultItemAnimator());
            activityProfileBinding.achievementList.setAdapter(galleryAdapter);

        }

    }

    private void populateValues() {

        Picasso.with(activityProfileBinding.dp.getContext())
                .load(user.getDisplayImage())
                .placeholder(R.drawable.placeholder)
                .centerCrop()
                .resize(100, 100)
                .into(activityProfileBinding.dp);

        activityProfileBinding.name.setText(user.getName());
        try {
            activityProfileBinding.dob.setText(new SimpleDateFormat(Constants.FORMAT_DATE_API).format(new SimpleDateFormat(Constants.FORMAT_DATE_TIME).parse(user.getDob())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        activityProfileBinding.email.setText(user.getEmail());
        activityProfileBinding.gender.setText(user.getGender());

    }

    private void showDialog() {
        final Dialog d = new Dialog(mActivity);
        d.setContentView(R.layout.dialog_give_review);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setCancelable(false);
        d.setCanceledOnTouchOutside(false);

        WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();
        lWindowParams.copyFrom(d.getWindow().getAttributes());
        lWindowParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        d.show();
        d.getWindow().setAttributes(lWindowParams);

        TextView btnGiveReview, btnCancel;
        EditText reviewValue;
        RatingBar ratingBar;

        btnGiveReview = d.findViewById(R.id.btnGiveReview);
        btnCancel = d.findViewById(R.id.btnCancel);
        reviewValue = d.findViewById(R.id.reviewValue);
        ratingBar = d.findViewById(R.id.ratingBar);

        ratingBar.setOnTouchListener(new View.OnTouchListener() {
            private float downXValue;

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    downXValue = event.getX();
                    return false;
                }

                if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    // When true is returned, view will not handle this event.
                    return true;
                }

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    float currentX = event.getX();
                    float difference = 0;
                    // Swipe on left side
                    if (currentX < downXValue)
                        difference = downXValue - currentX;
                        // Swipe on right side
                    else if (currentX > downXValue)
                        difference = currentX - downXValue;

                    if (difference < 10)
                        return false;

                    return true;
                }
                return false;
            }
        });

        reviewValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    ratingBar.setRating(Float.parseFloat(s.toString()));
                }
            }
        });

        btnGiveReview.setOnClickListener(v -> {
//            if (reviewValue.length() == 0) {
//                Toast.makeText(mActivity, "Please enter review ratings to give review", Toast.LENGTH_SHORT).show();
//                return;
//            }

            Review review = new Review();
            review.setUserId(user.getId());
//            review.setReviewRating(reviewValue.getText().toString());
            review.setReviewRating(String.valueOf(ratingBar.getRating()));

            new ServerHelper(mActivity).saveReview(review, new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        d.dismiss();
                    }
                }
            });

        });

        btnCancel.setOnClickListener(v -> {
            d.dismiss();
        });
    }

    private void showFeedbackDialog() {
        final Dialog d = new Dialog(mActivity);
        d.setContentView(R.layout.dialog_add_story);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setCancelable(false);
        d.setCanceledOnTouchOutside(false);

        WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();
        lWindowParams.copyFrom(d.getWindow().getAttributes());
        lWindowParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        d.show();
        d.getWindow().setAttributes(lWindowParams);

        TextView post, cancel;
        EditText message, date;
        ImageView picture, takePicture;

        message = d.findViewById(R.id.message);
        date = d.findViewById(R.id.date);
        post = d.findViewById(R.id.okay);
        cancel = d.findViewById(R.id.cancel);
        takePicture = d.findViewById(R.id.takePicture);
        picture = d.findViewById(R.id.picture);

        date.setText(new SimpleDateFormat(Constants.FORMAT_DATE_API).format(new Date()));

        takePicture.setOnClickListener(v -> {
            PickImageDialog.build(new PickSetup()
                    .setWidth(80)
                    .setHeight(80))
                    .setOnPickResult(r -> {

                        Uri uri = Uri.fromFile(new File(r.getPath()));
                        pictureURI = uri.toString();
                        picturePath = r.getPath();

                        Bitmap bitmapCompressed = ImageUtilities.getCompressedBitmap(r.getPath());
                        Uri imageUri = ImageUtilities.getImageUri(mActivity, bitmapCompressed);
                        picturePath = ImageUtilities.getRealPathFromURI(imageUri);

                        UploadCallback callback = new UploadCallback() {
                            @Override
                            public void onStart(String requestId) {

                            }

                            @Override
                            public void onProgress(String requestId, long bytes, long totalBytes) {

                            }

                            @Override
                            public void onSuccess(String requestId, Map resultData) {
                                Toast.makeText(mActivity, "Image Uploaded", Toast.LENGTH_SHORT).show();

                                imageUrl = resultData.get("secure_url").toString();
                                if (!TextUtils.isEmpty(imageUrl)) {
                                    Picasso.with(picture.getContext())
                                            .load(imageUrl)
                                            .fit()
                                            .into(picture);
                                    picture.setVisibility(View.VISIBLE);
                                    d.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                } else {
                                    picture.setVisibility(View.GONE);
                                    picture.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_upload_image_foreground));
                                }

                            }

                            @Override
                            public void onError(String requestId, ErrorInfo error) {
                                Toast.makeText(mActivity, "Image Uploading Failed", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onReschedule(String requestId, ErrorInfo error) {

                            }
                        };
                        String requestId = MediaManager.get().upload(picturePath).unsigned("mdczte2b").callback(callback).dispatch();

                    }).show(mActivity);
        });

        post.setOnClickListener(v -> {

            if (message.length() == 0 || date.length() == 0) {
                Toast.makeText(mActivity, "Please enter message to post feedback", Toast.LENGTH_SHORT).show();
                return;
            }

            com.application.elevated_shift.models.feedback.Datum feedback = new com.application.elevated_shift.models.feedback.Datum();
            feedback.setFeedbackGivenBy(Globals.getUsage().sharedPreferencesEditor.getUser());
            feedback.setFeedbackGivenTo(coach);
            feedback.setFeedbackImageUrl(imageUrl);
            feedback.setFeedbackText(message.getText().toString());

            new ServerHelper(mActivity).saveFeedback(feedback, new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        mSocket.emit("feedback", feedback);
                    }
                }
            });

            imageUrl = "";
            d.dismiss();
        });

        cancel.setOnClickListener(v -> {
            d.dismiss();
        });
    }

}
