package com.application.elevated_shift.viewModels.fragmentsViewModels;

import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.FragmentLoginBinding;
import com.application.elevated_shift.databinding.FragmentSignUpBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.interfaces.CallBackFragment;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.models.user.User;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.services.MessagesService;
import com.application.elevated_shift.ui.activities.AppFrontActivity;
import com.application.elevated_shift.utils.Utilities;

public class SignUpViewModel implements View.OnClickListener {

    FragmentSignUpBinding fragmentSignUpBinding;
    Activity mActivity;
    boolean isToggled = false;
    private boolean isCToggled = false;
    private CallBackFragment callBackFragment;

    public SignUpViewModel(FragmentSignUpBinding fragmentSignUpBinding, Activity mActivity) {
        this.fragmentSignUpBinding = fragmentSignUpBinding;
        this.mActivity = mActivity;
        callBackFragment = (CallBackFragment) mActivity;

        initViews();
    }

    private void initViews() {

        fragmentSignUpBinding.etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    isToggled = false;
                    fragmentSignUpBinding.togglePassword.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_action_visible));
                    fragmentSignUpBinding.togglePassword.setVisibility(View.GONE);
                } else {
                    if (fragmentSignUpBinding.togglePassword.getVisibility() == View.GONE)
                        fragmentSignUpBinding.togglePassword.setVisibility(View.VISIBLE);
                }
            }
        });
        fragmentSignUpBinding.etCPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    isToggled = false;
                    fragmentSignUpBinding.toggleCPassword.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_action_visible));
                    fragmentSignUpBinding.toggleCPassword.setVisibility(View.GONE);
                } else {
                    if (fragmentSignUpBinding.toggleCPassword.getVisibility() == View.GONE)
                        fragmentSignUpBinding.toggleCPassword.setVisibility(View.VISIBLE);
                }
            }
        });

        fragmentSignUpBinding.togglePassword.setOnClickListener(this);
        fragmentSignUpBinding.toggleCPassword.setOnClickListener(this);
        fragmentSignUpBinding.tvLogin.setOnClickListener(this);
        fragmentSignUpBinding.btnSignup.setOnClickListener(this);
        fragmentSignUpBinding.etDateOfBirth.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.togglePassword:
                isToggled = !isToggled;
                if (isToggled) {
                    fragmentSignUpBinding.togglePassword.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_action_hide));
                    fragmentSignUpBinding.etPassword.setTransformationMethod(new HideReturnsTransformationMethod());
                    fragmentSignUpBinding.etPassword.setSelection(fragmentSignUpBinding.etPassword.length());
                } else {
                    fragmentSignUpBinding.togglePassword.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_action_visible));
                    fragmentSignUpBinding.etPassword.setTransformationMethod(new PasswordTransformationMethod());
                    fragmentSignUpBinding.etPassword.setSelection(fragmentSignUpBinding.etPassword.length());
                }
                break;
            case R.id.toggleCPassword:
                isCToggled = !isCToggled;
                if (isCToggled) {
                    fragmentSignUpBinding.toggleCPassword.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_action_hide));
                    fragmentSignUpBinding.etCPassword.setTransformationMethod(new HideReturnsTransformationMethod());
                    fragmentSignUpBinding.etCPassword.setSelection(fragmentSignUpBinding.etCPassword.length());
                } else {
                    fragmentSignUpBinding.toggleCPassword.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_action_visible));
                    fragmentSignUpBinding.etCPassword.setTransformationMethod(new PasswordTransformationMethod());
                    fragmentSignUpBinding.etCPassword.setSelection(fragmentSignUpBinding.etCPassword.length());
                }
                break;
            case R.id.btnSignup:
                if (validateFields()) {
                    User user = new User();
                    user.setFirstName(fragmentSignUpBinding.etFirstName.getText().toString());
                    user.setLastName(fragmentSignUpBinding.etLastName.getText().toString());
                    user.setEmail(fragmentSignUpBinding.etEmail.getText().toString());
                    user.setPassword(fragmentSignUpBinding.etPassword.getText().toString());
                    user.setGender(fragmentSignUpBinding.rbMale.isChecked() ? "male" : "female");
                    user.setDob(fragmentSignUpBinding.etDateOfBirth.getText().toString());

                    new ServerHelper(mActivity).signup(user, new HandlerDataSync() {
                        @Override
                        public void onSyncSuccessful(boolean success, boolean isDataError) {
                            if (success) {

                                if (!Globals.isMyServiceRunning(MessagesService.class, mActivity))
                                    mActivity.startService(new Intent(mActivity, MessagesService.class));

                                Data user = Globals.getUsage().sharedPreferencesEditor.getUser();
                                user.setPassword(fragmentSignUpBinding.etPassword.getText().toString());

                                Globals.getUsage().sharedPreferencesEditor.setLoggedIn(true);
                                Globals.getUsage().sharedPreferencesEditor.setUser(user);

                                mActivity.startActivity(new Intent(mActivity, AppFrontActivity.class));
                                mActivity.finish();

//                                new ServerHelper(mActivity).createGallery(new HandlerDataSync() {
//                                    @Override
//                                    public void onSyncSuccessful(boolean success, boolean isDataError) {
//                                        if (success) {
//                                            mActivity.startActivity(new Intent(mActivity, AppFrontActivity.class));
//                                            mActivity.finish();
//                                        }
//                                    }
//                                });
                            }
                        }
                    });

                }
                break;
            case R.id.tvLogin:
                mActivity.onBackPressed();
                break;
            case R.id.etDateOfBirth:
                Utilities.openCalender(mActivity, fragmentSignUpBinding.etDateOfBirth, null, "", Constants.FORMAT_DATE_API);
                break;
        }
    }

    private boolean validateFields() {
        if (fragmentSignUpBinding.etFirstName.length() == 0) {
            Toast.makeText(mActivity, "Please enter fullname", Toast.LENGTH_SHORT).show();
            fragmentSignUpBinding.etFirstName.requestFocus();
            return false;
        }
        if (fragmentSignUpBinding.etLastName.length() == 0) {
            Toast.makeText(mActivity, "Please enter fullname", Toast.LENGTH_SHORT).show();
            fragmentSignUpBinding.etLastName.requestFocus();
            return false;
        }
        if (fragmentSignUpBinding.etEmail.length() == 0) {
            Toast.makeText(mActivity, "Please enter email", Toast.LENGTH_SHORT).show();
            fragmentSignUpBinding.etPassword.requestFocus();
            return false;
        }
        if (!Globals.isEmailValid(fragmentSignUpBinding.etEmail.getText().toString())) {
            Toast.makeText(mActivity, "Please enter valid email", Toast.LENGTH_SHORT).show();
            fragmentSignUpBinding.etPassword.requestFocus();
            return false;
        }
        if (fragmentSignUpBinding.etDateOfBirth.length() == 0) {
            Toast.makeText(mActivity, "Please select date of birth", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (fragmentSignUpBinding.etPassword.length() == 0) {
            Toast.makeText(mActivity, "Please enter password", Toast.LENGTH_SHORT).show();
            fragmentSignUpBinding.etPassword.requestFocus();
            return false;
        }
        if (fragmentSignUpBinding.etPassword.length() < 6) {
            Toast.makeText(mActivity, "Please enter password at least 6 character long", Toast.LENGTH_SHORT).show();
            fragmentSignUpBinding.etPassword.requestFocus();
            return false;
        }
        if (fragmentSignUpBinding.etCPassword.length() == 0) {
            Toast.makeText(mActivity, "Please enter confirm password", Toast.LENGTH_SHORT).show();
            fragmentSignUpBinding.etCPassword.requestFocus();
            return false;
        }
        if (fragmentSignUpBinding.etCPassword.length() < 6) {
            Toast.makeText(mActivity, "Please enter confirm password at least 6 character long", Toast.LENGTH_SHORT).show();
            fragmentSignUpBinding.etCPassword.requestFocus();
            return false;
        }
        if (!fragmentSignUpBinding.etPassword.getText().toString().equals(fragmentSignUpBinding.etCPassword.getText().toString())) {
            Toast.makeText(mActivity, "Passwords did not match", Toast.LENGTH_SHORT).show();
            fragmentSignUpBinding.etCPassword.requestFocus();
            return false;
        }
        return true;
    }
}
