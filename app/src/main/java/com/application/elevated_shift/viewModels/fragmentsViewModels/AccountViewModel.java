package com.application.elevated_shift.viewModels.fragmentsViewModels;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.text.Spannable;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.FragmentAccountBinding;
import com.application.elevated_shift.databinding.FragmentLoginBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.sharedPreference.SharedPreferencesEditor;
import com.application.elevated_shift.ui.activities.MyAchievementActivity;
import com.application.elevated_shift.ui.activities.MyGoalsActivity;
import com.application.elevated_shift.ui.activities.MyPaymentsActivity;
import com.application.elevated_shift.ui.activities.UserProfileActivity;
import com.application.elevated_shift.ui.fragments.userProfileFragments.UpdateAccountFragment;
import com.application.elevated_shift.utils.Utilities;
import com.daimajia.swipe.SwipeLayout;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;

public class AccountViewModel {

    public static AccountViewModel instance;
    public Data data;
    FragmentAccountBinding fragmentAccountBinding;
    UserProfileActivity mActivity;

    public AccountViewModel(FragmentAccountBinding fragmentAccountBinding, UserProfileActivity mActivity) {
        this.fragmentAccountBinding = fragmentAccountBinding;
        this.mActivity = mActivity;
        instance = this;

        initViews();
    }

    private void initViews() {

        fragmentAccountBinding.llAccountInfo.setOnClickListener(v -> {
            Fragment fragment = new UpdateAccountFragment();
            FragmentManager manager = mActivity.getSupportFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(R.id.fragment, fragment, "");
            ft.addToBackStack(null);
            ft.commit();
        });

        fragmentAccountBinding.llReviews.setOnClickListener(v -> {
            mActivity.startActivity(new Intent(mActivity, MyAchievementActivity.class));
        });

        fragmentAccountBinding.llGoals.setOnClickListener(v -> {
            mActivity.startActivity(new Intent(mActivity, MyGoalsActivity.class));
        });

        fragmentAccountBinding.llPayments.setOnClickListener(v -> {
            mActivity.startActivity(new Intent(mActivity, MyPaymentsActivity.class));
        });

    }

}
