package com.application.elevated_shift.viewModels.fragmentsViewModels;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.FragmentConnectionsBinding;
import com.application.elevated_shift.databinding.FragmentLoginBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.user.SocialLinks;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.gsd.dynamicMenu.data.ActionItem;
import com.gsd.dynamicMenu.menus.HorizontalPopupMenu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class ConnectionsViewModel implements View.OnClickListener {

    FragmentConnectionsBinding fragmentConnectionsBinding;
    Activity mActivity;
    ActionItem edit, remove;
    HorizontalPopupMenu mQuickAction;
    private String name;
    private String type;
//    final PixelPopupMenu mQuickAction = new PixelPopupMenu(context);
//    final VerticalListPopupMenu mQuickAction = new VerticalListPopupMenu(context);
//    final VerticalPopupMenu mQuickAction = new VerticalPopupMenu(context);

    public ConnectionsViewModel(FragmentConnectionsBinding fragmentConnectionsBinding, Activity mActivity) {
        this.fragmentConnectionsBinding = fragmentConnectionsBinding;
        this.mActivity = mActivity;
        edit = new ActionItem(1, "Edit", mActivity.getResources().getDrawable(R.drawable.ic_edit), false);
        remove = new ActionItem(2, "Remove", mActivity.getResources().getDrawable(R.drawable.ic_delete), false);
        mQuickAction = new HorizontalPopupMenu(mActivity);

        initViews();
    }

    private void initViews() {

        mQuickAction.setGravity(0);
//        mQuickAction.setBackgroundRadias(42);
        mQuickAction.setHasActionTitles(true);
        mQuickAction.setBackgroundColor(Color.WHITE);
        mQuickAction.addActionItem(edit);
        mQuickAction.addActionItem(remove);

        mQuickAction.setOnActionItemClickListener(new HorizontalPopupMenu.OnActionItemClickListener() {
            @Override
            public void onItemClick(HorizontalPopupMenu source, int pos, int actionId) {
                switch (actionId) {
                    case 1:
                        showEditDialog();
                        break;
                    case 2:
                        List<SocialLinks> connectionsList = Globals.getUsage().sharedPreferencesEditor.getUser().getSocialLinks();
                        int index = -1;
                        for (int i = 0; i < connectionsList.size(); i++) {
                            if (connectionsList.get(i).getName().equals(name))
                                index = i;
                        }
                        connectionsList.remove(index);

                        HashMap<String, List<SocialLinks>> map = new HashMap<>();
                        map.put("socialLinks", connectionsList);

                        new ServerHelper(mActivity).updateUserLinks(map, Globals.getUsage().sharedPreferencesEditor.getUser().getId(), new HandlerDataSync() {
                            @Override
                            public void onSyncSuccessful(boolean success, boolean isDataError) {
                                if (success) {
                                    Toast.makeText(mActivity, "Connection removed successfully", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        break;
                }
            }
        });

        fragmentConnectionsBinding.phone.setOnClickListener(this);
        fragmentConnectionsBinding.instagram.setOnClickListener(this);
        fragmentConnectionsBinding.twitter.setOnClickListener(this);
        fragmentConnectionsBinding.tumblr.setOnClickListener(this);
        fragmentConnectionsBinding.youtube.setOnClickListener(this);
        fragmentConnectionsBinding.facebook.setOnClickListener(this);
        fragmentConnectionsBinding.email.setOnClickListener(this);
        fragmentConnectionsBinding.whatsapp.setOnClickListener(this);
        fragmentConnectionsBinding.linkedin.setOnClickListener(this);
        fragmentConnectionsBinding.qq.setOnClickListener(this);
        fragmentConnectionsBinding.wechat.setOnClickListener(this);
        fragmentConnectionsBinding.googleplus.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.phone:
                for (SocialLinks socialLinks : Globals.getUsage().sharedPreferencesEditor.getUser().getSocialLinks()) {
                    if (socialLinks.getName().equals("Phone")) {
//                        Toast.makeText(mActivity, "Connection already added", Toast.LENGTH_SHORT).show();
                        name = "Phone";
                        type = "phone";
                        mQuickAction.show(v);
                        return;
                    }
                }

                showDialog("phone", "Phone");

                break;
            case R.id.instagram:
                for (SocialLinks socialLinks : Globals.getUsage().sharedPreferencesEditor.getUser().getSocialLinks()) {
                    if (socialLinks.getName().equals("Instagram")) {
//                        Toast.makeText(mActivity, "Connection already added", Toast.LENGTH_SHORT).show();
                        name = "Instagram";
                        type = "link";
                        mQuickAction.show(v);
                        return;
                    }
                }

                showDialog("link", "Instagram");
                break;
            case R.id.twitter:
                for (SocialLinks socialLinks : Globals.getUsage().sharedPreferencesEditor.getUser().getSocialLinks()) {
                    if (socialLinks.getName().equals("Twitter")) {
//                        Toast.makeText(mActivity, "Connection already added", Toast.LENGTH_SHORT).show();
                        name = "Twitter";
                        type = "link";
                        mQuickAction.show(v);
                        return;
                    }
                }

                showDialog("link", "Twitter");
                break;
            case R.id.tumblr:
                for (SocialLinks socialLinks : Globals.getUsage().sharedPreferencesEditor.getUser().getSocialLinks()) {
                    if (socialLinks.getName().equals("Tumblr")) {
//                        Toast.makeText(mActivity, "Connection already added", Toast.LENGTH_SHORT).show();
                        name = "Tumblr";
                        type = "link";
                        mQuickAction.show(v);
                        return;
                    }
                }

                showDialog("link", "Tumblr");
                break;
            case R.id.youtube:
                for (SocialLinks socialLinks : Globals.getUsage().sharedPreferencesEditor.getUser().getSocialLinks()) {
                    if (socialLinks.getName().equals("Youtube")) {
//                        Toast.makeText(mActivity, "Connection already added", Toast.LENGTH_SHORT).show();
                        name = "Youtube";
                        type = "link";
                        mQuickAction.show(v);
                        return;
                    }
                }

                showDialog("link", "Youtube");
                break;
            case R.id.facebook:
                for (SocialLinks socialLinks : Globals.getUsage().sharedPreferencesEditor.getUser().getSocialLinks()) {
                    if (socialLinks.getName().equals("Facebook")) {
//                        Toast.makeText(mActivity, "Connection already added", Toast.LENGTH_SHORT).show();
                        name = "Facebook";
                        type = "link";
                        mQuickAction.show(v);
                        return;
                    }
                }

                showDialog("link", "Facebook");
                break;
            case R.id.email:
                for (SocialLinks socialLinks : Globals.getUsage().sharedPreferencesEditor.getUser().getSocialLinks()) {
                    if (socialLinks.getName().equals("Email")) {
//                        Toast.makeText(mActivity, "Connection already added", Toast.LENGTH_SHORT).show();
                        name = "Email";
                        type = "email";
                        mQuickAction.show(v);
                        return;
                    }
                }

                showDialog("email", "Email");
                break;
            case R.id.whatsapp:
                for (SocialLinks socialLinks : Globals.getUsage().sharedPreferencesEditor.getUser().getSocialLinks()) {
                    if (socialLinks.getName().equals("Whatsapp")) {
//                        Toast.makeText(mActivity, "Connection already added", Toast.LENGTH_SHORT).show();
                        name = "Whatsapp";
                        type = "phone";
                        mQuickAction.show(v);
                        return;
                    }
                }

                showDialog("phone", "Whatsapp");
                break;
            case R.id.linkedin:
                for (SocialLinks socialLinks : Globals.getUsage().sharedPreferencesEditor.getUser().getSocialLinks()) {
                    if (socialLinks.getName().equals("LinkedIn")) {
//                        Toast.makeText(mActivity, "Connection already added", Toast.LENGTH_SHORT).show();
                        name = "LinkedIn";
                        type = "link";
                        mQuickAction.show(v);
                        return;
                    }
                }

                showDialog("link", "LinkedIn");
                break;
            case R.id.qq:
                for (SocialLinks socialLinks : Globals.getUsage().sharedPreferencesEditor.getUser().getSocialLinks()) {
                    if (socialLinks.getName().equals("QQ")) {
//                        Toast.makeText(mActivity, "Connection already added", Toast.LENGTH_SHORT).show();
                        name = "QQ";
                        type = "link";
                        mQuickAction.show(v);
                        return;
                    }
                }

                showDialog("link", "QQ");
                break;
            case R.id.wechat:
                for (SocialLinks socialLinks : Globals.getUsage().sharedPreferencesEditor.getUser().getSocialLinks()) {
                    if (socialLinks.getName().equals("WeChat")) {
//                        Toast.makeText(mActivity, "Connection already added", Toast.LENGTH_SHORT).show();
                        name = "WeChat";
                        type = "link";
                        mQuickAction.show(v);
                        return;
                    }
                }

                showDialog("link", "WeChat");
                break;
            case R.id.googleplus:
                for (SocialLinks socialLinks : Globals.getUsage().sharedPreferencesEditor.getUser().getSocialLinks()) {
                    if (socialLinks.getName().equals("GooglePlus")) {
//                        Toast.makeText(mActivity, "Connection already added", Toast.LENGTH_SHORT).show();
                        name = "GooglePlus";
                        type = "link";
                        mQuickAction.show(v);
                        return;
                    }
                }

                showDialog("link", "GooglePlus");
                break;
        }
    }

    private void showEditDialog() {
        final Dialog d = new Dialog(mActivity);
        d.setContentView(R.layout.dialog_add_connection);
        Objects.requireNonNull(d.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView post, cancel;
        EditText link;
        TextView name;

        name = d.findViewById(R.id.name);
        link = d.findViewById(R.id.link);
        post = d.findViewById(R.id.okay);
        cancel = d.findViewById(R.id.cancel);

        name.setText(this.name);
        post.setText("Save");
        if (type.equals("phone")) {
            link.setHint("Enter phone number");
        } else if (type.equals("email")) {
            link.setHint("Enter email address");
        } else {
            link.setHint("Paste profile link here");
        }

        post.setOnClickListener(v -> {

            if (type.equals("phone")) {
                if (link.length() == 0) {
                    Toast.makeText(mActivity, "Please enter phone number to add connection", Toast.LENGTH_SHORT).show();
                    return;
                }
            } else if (type.equals("email")) {
                if (link.length() == 0) {
                    Toast.makeText(mActivity, "Please enter email address to add connection", Toast.LENGTH_SHORT).show();
                    return;
                }
            } else {
                if (link.length() == 0) {
                    Toast.makeText(mActivity, "Please enter link to add connection", Toast.LENGTH_SHORT).show();
                    return;
                }
            }

            List<SocialLinks> connectionsList = Globals.getUsage().sharedPreferencesEditor.getUser().getSocialLinks();
            int index = -1;
            for (int i = 0; i < connectionsList.size(); i++) {
                if (connectionsList.get(i).getName().equals(this.name))
                    index = i;
            }
            connectionsList.get(index).setLink(link.getText().toString());

            HashMap<String, List<SocialLinks>> map = new HashMap<>();
            map.put("socialLinks", connectionsList);

            new ServerHelper(mActivity).updateUserLinks(map, Globals.getUsage().sharedPreferencesEditor.getUser().getId(), new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        Toast.makeText(mActivity, "Connection updated successfully", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            d.dismiss();
        });

        cancel.setOnClickListener(v -> {
            d.dismiss();
        });

        d.show();
    }

    private void showDialog(String type, String connectionName) {
        final Dialog d = new Dialog(mActivity);
        d.setContentView(R.layout.dialog_add_connection);
        Objects.requireNonNull(d.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setCancelable(false);
        d.setCanceledOnTouchOutside(false);

        WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();
        lWindowParams.copyFrom(d.getWindow().getAttributes());
        lWindowParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        d.show();
        d.getWindow().setAttributes(lWindowParams);

        TextView post, cancel;
        EditText link;
        TextView name;

        name = d.findViewById(R.id.name);
        link = d.findViewById(R.id.link);
        post = d.findViewById(R.id.okay);
        cancel = d.findViewById(R.id.cancel);

        name.setText(connectionName);

        if (type.equals("phone")) {
            link.setHint("Enter phone number");
        } else if (type.equals("email")) {
            link.setHint("Enter email address");
        } else {
            link.setHint("Paste profile link here");
        }

        post.setOnClickListener(v -> {

            if (type.equals("phone")) {
                if (link.length() == 0) {
                    Toast.makeText(mActivity, "Please enter phone number to add connection", Toast.LENGTH_SHORT).show();
                    return;
                }
            } else if (type.equals("email")) {
                if (link.length() == 0) {
                    Toast.makeText(mActivity, "Please enter email address to add connection", Toast.LENGTH_SHORT).show();
                    return;
                }
            } else {
                if (link.length() == 0) {
                    Toast.makeText(mActivity, "Please enter link to add connection", Toast.LENGTH_SHORT).show();
                    return;
                }
            }

            SocialLinks socialLinks = new SocialLinks(name.getText().toString(), link.getText().toString());
            List<SocialLinks> connectionsList = new ArrayList<>();
            if (Globals.getUsage().sharedPreferencesEditor.getUser().getSocialLinks() != null)
                connectionsList = Globals.getUsage().sharedPreferencesEditor.getUser().getSocialLinks();
            connectionsList.add(socialLinks);

            HashMap<String, List<SocialLinks>> map = new HashMap<>();
            map.put("socialLinks", connectionsList);

            new ServerHelper(mActivity).updateUserLinks(map, Globals.getUsage().sharedPreferencesEditor.getUser().getId(), new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        Toast.makeText(mActivity, "Connection added successfully", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            d.dismiss();
        });

        cancel.setOnClickListener(v -> {
            d.dismiss();
        });
    }

}
