package com.application.elevated_shift.viewModels.activitesViewModels;

import android.annotation.SuppressLint;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityMyGoalsBinding;
import com.application.elevated_shift.databinding.ActivityMyPerformanceBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.goal.Datum;
import com.application.elevated_shift.models.goal.GoalResponse;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.ui.activities.MyPerformanceActivity;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class MyPerformanceViewModel {

    public static MyPerformanceViewModel instance;
    MyPerformanceActivity mActivity;
    ActivityMyPerformanceBinding activityMyPerformanceBinding;
    public GoalResponse goalResponse = new GoalResponse();
    private int completedGoals = 0, snoozedGoals = 0, overdueGoals = 0, totalGoals = 0;

    public MyPerformanceViewModel(MyPerformanceActivity mActivity, ActivityMyPerformanceBinding activityMyPerformanceBinding) {
        instance = this;
        this.mActivity = mActivity;
        this.activityMyPerformanceBinding = activityMyPerformanceBinding;

        initViews();
    }

    public void onResume() {
        Data user = Globals.getUsage().sharedPreferencesEditor.getUser();
        activityMyPerformanceBinding.tvUsername.setText(user.getName());
        Picasso.with(activityMyPerformanceBinding.dp.getContext())
                .load(user.getDisplayImage())
                .placeholder(R.drawable.placeholder)
                .centerCrop()
                .resize(200, 200)
                .into(activityMyPerformanceBinding.dp);
    }

    @SuppressLint("SimpleDateFormat")
    private void initViews() {

        activityMyPerformanceBinding.btnBack.setOnClickListener(v -> mActivity.onBackPressed());

        activityMyPerformanceBinding.monthName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                calculateGoals();
            }
        });

        activityMyPerformanceBinding.monthName.setText(new SimpleDateFormat("MMMM").format(new Date()));

        activityMyPerformanceBinding.previousMonth.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            try {
                calendar.setTime(Objects.requireNonNull(new SimpleDateFormat("MMMM").parse(activityMyPerformanceBinding.monthName.getText().toString())));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            calendar.add(Calendar.MONTH, -1);
            activityMyPerformanceBinding.monthName.setText(new SimpleDateFormat("MMMM").format(calendar.getTime()));
        });

        activityMyPerformanceBinding.nextMonth.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            try {
                calendar.setTime(Objects.requireNonNull(new SimpleDateFormat("MMMM").parse(activityMyPerformanceBinding.monthName.getText().toString())));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            calendar.add(Calendar.MONTH, 1);
            activityMyPerformanceBinding.monthName.setText(new SimpleDateFormat("MMMM").format(calendar.getTime()));
        });

    }

    private void calculateGoals() {
        new ServerHelper(mActivity).getGoals(new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success) {
                    if (goalResponse.getData() != null) {

                        for (Datum datum : goalResponse.getData()) {
                            Calendar calendar = Calendar.getInstance();
                            try {
                                calendar.setTime(Objects.requireNonNull(new SimpleDateFormat(Constants.FORMAT_DATE_API).parse(datum.getEndDateTime())));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            String month = new SimpleDateFormat("MMMM").format(calendar.getTime());

                            if (activityMyPerformanceBinding.monthName.getText().toString().equals(month)) {
                                totalGoals++;
                                if (datum.getStatus().equals("Completed"))
                                    completedGoals++;
                                else if (datum.getStatus().equals("Snoozed"))
                                    snoozedGoals++;
                                else if (datum.getStatus().equals("Overdue"))
                                    overdueGoals++;
                            }
                        }

                        populateFields();

                    }
                }
            }
        });
    }

    private void populateFields() {

        activityMyPerformanceBinding.completedGoals.setText(completedGoals + "");
        activityMyPerformanceBinding.snoozedGoals.setText(snoozedGoals + "");
        activityMyPerformanceBinding.overdueGoals.setText(overdueGoals + "");
        activityMyPerformanceBinding.totalGoals.setText(totalGoals + "");

        completedGoals = snoozedGoals = overdueGoals = totalGoals = 0;

    }
}
