package com.application.elevated_shift.viewModels.activitesViewModels;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.adapters.NewsFeedAdapter;
import com.application.elevated_shift.databinding.ActivityMyNewsFeedBinding;
import com.application.elevated_shift.databinding.ActivityMyPerformanceBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.Story;
import com.application.elevated_shift.models.collection.CollectionsResponse;
import com.application.elevated_shift.models.story.Datum;
import com.application.elevated_shift.models.story.StoryResponse;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.ui.activities.MyNewsFeedActivity;
import com.application.elevated_shift.ui.activities.SearchViewActivity;
import com.application.elevated_shift.utils.ImageUtilities;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.squareup.picasso.Picasso;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;

import java.io.File;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class MyNewsFeedViewModel {

    public static MyNewsFeedViewModel instance;
    MyNewsFeedActivity mActivity;
    ActivityMyNewsFeedBinding activityMyNewsFeedBinding;
    public StoryResponse storyResponse = new StoryResponse();
    public CollectionsResponse collectionsResponse = new CollectionsResponse();
    private NewsFeedAdapter newsFeedAdapter;
    private String pictureURI;
    private String picturePath;
    private String imageUrl;
    private ArrayList<Object> collectionList;
    private String collectionId;
    private Socket mSocket;

    {
        try {
            mSocket = IO.socket("https://gymbe.herokuapp.com/");
            mSocket.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public MyNewsFeedViewModel(MyNewsFeedActivity mActivity, ActivityMyNewsFeedBinding activityMyNewsFeedBinding) {
        instance = this;
        this.mActivity = mActivity;
        this.activityMyNewsFeedBinding = activityMyNewsFeedBinding;

        initViews();
    }

    private void initViews() {

        activityMyNewsFeedBinding.btnBack.setOnClickListener(v -> mActivity.onBackPressed());

        activityMyNewsFeedBinding.btnAddPost.setOnClickListener(v -> {
            showDialog();
        });

        activityMyNewsFeedBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                activityMyNewsFeedBinding.swipeRefreshLayout.setRefreshing(true);
                new ServerHelper(mActivity).getStories(new HandlerDataSync() {
                    @Override
                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                        if (success) {
                            setAdapter(storyResponse.getData());
                        }
                    }
                });
                activityMyNewsFeedBinding.swipeRefreshLayout.setRefreshing(false);
            }
        });

        activityMyNewsFeedBinding.searchBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.startActivity(new Intent(mActivity, SearchViewActivity.class));
//                mActivity.overridePendingTransition(R.anim.enter,
//                        R.anim.leave);
            }
        });

        new ServerHelper(mActivity).getStories(new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success) {
                    setAdapter(storyResponse.getData());
                }
            }
        });

    }

    public void setAdapter(List<Datum> list) {

        if (list == null)
            return;
        if (list.size() == 0) {
            activityMyNewsFeedBinding.noPost.setVisibility(View.VISIBLE);
            activityMyNewsFeedBinding.postsList.setVisibility(View.GONE);
        } else {
            activityMyNewsFeedBinding.noPost.setVisibility(View.GONE);
            activityMyNewsFeedBinding.postsList.setVisibility(View.VISIBLE);

            Collections.reverse(list);
            newsFeedAdapter = new NewsFeedAdapter(list, mActivity);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
            activityMyNewsFeedBinding.postsList.setLayoutManager(linearLayoutManager);
            activityMyNewsFeedBinding.postsList.setAdapter(newsFeedAdapter);

        }
    }

    private void showDialog() {
        final Dialog d = new Dialog(mActivity);
        d.setContentView(R.layout.dialog_add_post);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setCancelable(false);
        d.setCanceledOnTouchOutside(false);

        WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();
        lWindowParams.copyFrom(d.getWindow().getAttributes());
        lWindowParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        d.show();
        d.getWindow().setAttributes(lWindowParams);

        TextView post, cancel;
        EditText message, date;
        ImageView picture, takePicture;
        MaterialSpinner collections;

        collections = d.findViewById(R.id.collection);
        message = d.findViewById(R.id.message);
        date = d.findViewById(R.id.date);
        post = d.findViewById(R.id.okay);
        cancel = d.findViewById(R.id.cancel);
        takePicture = d.findViewById(R.id.takePicture);
        picture = d.findViewById(R.id.picture);

        date.setText(new SimpleDateFormat(Constants.FORMAT_DATE_API).format(new Date()));

        new ServerHelper(mActivity).getCollections(new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                collectionList = new ArrayList<>();
                collectionList.add("Select Collection");
                for (com.application.elevated_shift.models.collection.Datum datum : collectionsResponse.getData()) {
                    if (datum.getUser().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId()))
                        collectionList.add(datum.getTitle());
                }
                collections.setItems(collectionList);
            }
        });

        collections.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                for (com.application.elevated_shift.models.collection.Datum datum : collectionsResponse.getData()) {
                    if (datum.getUser().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId()))
                        if (datum.getTitle().equals(collections.getText().toString())) {
                            collectionId = datum.getId();
                            break;
                        }
                }
            }
        });

        takePicture.setOnClickListener(v -> {
            PickImageDialog.build(new PickSetup()
                    .setWidth(80)
                    .setHeight(80))
                    .setOnPickResult(r -> {

                        Uri uri = Uri.fromFile(new File(r.getPath()));
                        pictureURI = uri.toString();
                        picturePath = r.getPath();

                        Bitmap bitmapCompressed = ImageUtilities.getCompressedBitmap(r.getPath());
                        Uri imageUri = ImageUtilities.getImageUri(mActivity, bitmapCompressed);
                        picturePath = ImageUtilities.getRealPathFromURI(imageUri);

                        UploadCallback callback = new UploadCallback() {
                            @Override
                            public void onStart(String requestId) {

                            }

                            @Override
                            public void onProgress(String requestId, long bytes, long totalBytes) {

                            }

                            @Override
                            public void onSuccess(String requestId, Map resultData) {
                                Toast.makeText(mActivity, "Image Uploaded", Toast.LENGTH_SHORT).show();

                                imageUrl = resultData.get("secure_url").toString();
                                if (!TextUtils.isEmpty(imageUrl)) {
                                    Picasso.with(picture.getContext())
                                            .load(imageUrl)
                                            .fit()
                                            .into(picture);
                                    picture.setVisibility(View.VISIBLE);
                                    d.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                } else {
                                    picture.setVisibility(View.GONE);
                                    picture.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_upload_image_foreground));
                                }

                            }

                            @Override
                            public void onError(String requestId, ErrorInfo error) {
                                Toast.makeText(mActivity, "Image Uploading Failed", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onReschedule(String requestId, ErrorInfo error) {

                            }
                        };
                        String requestId = MediaManager.get().upload(picturePath).unsigned("mdczte2b").callback(callback).dispatch();

                    }).show(mActivity);
        });

        post.setOnClickListener(v -> {

            if (collections.getText().toString().equals("Select Collection") || collectionId.equals("")) {
                Toast.makeText(mActivity, "Please select collection from drop down or add it in collections page", Toast.LENGTH_SHORT).show();
                return;
            }
            if (message.length() == 0 || date.length() == 0) {
                Toast.makeText(mActivity, "Please enter message to post story", Toast.LENGTH_SHORT).show();
                return;
            }

            Story story = new Story();
            story.setCollectionId(collectionId);
            story.setStoryText(message.getText().toString());
            story.setImageUrl(imageUrl);

            new ServerHelper(mActivity).saveStory(story, new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        new ServerHelper(mActivity).getStories(new HandlerDataSync() {
                            @Override
                            public void onSyncSuccessful(boolean success, boolean isDataError) {
                                if (success) {
                                    setAdapter(storyResponse.getData());
                                }
                            }
                        });
                    }
                }
            });

            imageUrl = "";
            d.dismiss();
        });

        cancel.setOnClickListener(v -> {
            d.dismiss();
        });
    }

}
