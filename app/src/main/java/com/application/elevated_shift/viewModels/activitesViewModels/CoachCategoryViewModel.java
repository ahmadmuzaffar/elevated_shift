package com.application.elevated_shift.viewModels.activitesViewModels;

import android.view.View;

import com.application.elevated_shift.adapters.CategoryAdapter;
import com.application.elevated_shift.databinding.ActivityAllChatsBinding;
import com.application.elevated_shift.databinding.ActivityCoachCategoryBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.models.user.GetCoachesRespone;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.ui.activities.CoachCategoryActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;

public class CoachCategoryViewModel {

    public static CoachCategoryViewModel instance;
    public GetCoachesRespone getCoachesRespone;
    CoachCategoryActivity mActivity;
    ActivityCoachCategoryBinding activityCoachCategoryBinding;
    CategoryAdapter categoryAdapter;
    String category;
    List<Data> coachesList = new ArrayList<>();

    public CoachCategoryViewModel(CoachCategoryActivity mActivity, ActivityCoachCategoryBinding activityCoachCategoryBinding) {

        instance = this;
        this.mActivity = mActivity;
        this.activityCoachCategoryBinding = activityCoachCategoryBinding;

        initViews();
    }

    private void initViews() {

        category = mActivity.getIntent().getStringExtra("category");
        activityCoachCategoryBinding.headerTitle.setText(category);
        activityCoachCategoryBinding.btnBack.setOnClickListener(v -> mActivity.onBackPressed());
        new ServerHelper(mActivity).getCoaches(new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success) {
                    for (Data data : getCoachesRespone.getData()) {
                        if (data.getCategory() != null) {
                            if (data.getCategory().toLowerCase().equals(category.toLowerCase()))
                                coachesList.add(data);
                        }
                    }
                    setAdapter();
                }
            }
        });

    }

    public void setAdapter() {

        if (coachesList.size() == 0) {
            activityCoachCategoryBinding.noCoach.setVisibility(View.VISIBLE);
            activityCoachCategoryBinding.coachList.setVisibility(View.GONE);
        } else {
            activityCoachCategoryBinding.noCoach.setVisibility(View.GONE);
            activityCoachCategoryBinding.coachList.setVisibility(View.VISIBLE);
        }

        categoryAdapter = new CategoryAdapter(coachesList, mActivity);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        activityCoachCategoryBinding.coachList.setLayoutManager(layoutManager);
        activityCoachCategoryBinding.coachList.setAdapter(categoryAdapter);
    }
}
