package com.application.elevated_shift.viewModels.activitesViewModels;

import android.view.View;

import com.application.elevated_shift.adapters.ReviewAdapter;
import com.application.elevated_shift.databinding.ActivityMyReviewsBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.review.Datum;
import com.application.elevated_shift.models.review.ReviewResponse;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.ui.activities.MyReviewsActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;

public class MyReviewsViewModel {

    public static MyReviewsViewModel instance;
    MyReviewsActivity mActivity;
    ActivityMyReviewsBinding activityMyReviewsBinding;
    public ReviewResponse reviewResponse = new ReviewResponse();
    ReviewAdapter reviewAdapter;

    public MyReviewsViewModel(MyReviewsActivity mActivity, ActivityMyReviewsBinding activityMyReviewsBinding) {
        instance = this;
        this.mActivity = mActivity;
        this.activityMyReviewsBinding = activityMyReviewsBinding;

        initViews();
    }

    private void initViews() {

        activityMyReviewsBinding.btnBack.setOnClickListener(v -> mActivity.onBackPressed());

        new ServerHelper(mActivity).getReviews(new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success)
                    setAdapter();
            }
        });

    }

    public void setAdapter() {

        List<Datum> list = new ArrayList<>();
        for (Datum datum : reviewResponse.getData()) {
            if (Globals.getUsage().sharedPreferencesEditor.getUser().getId().equals(datum.getUser().getId())) {
                list.add(datum);
            }
        }

        if (list.size() == 0) {
            activityMyReviewsBinding.noReview.setVisibility(View.VISIBLE);
            activityMyReviewsBinding.reviewList.setVisibility(View.GONE);
        } else {
            activityMyReviewsBinding.noReview.setVisibility(View.GONE);
            activityMyReviewsBinding.reviewList.setVisibility(View.VISIBLE);
        }

        reviewAdapter = new ReviewAdapter(list, mActivity);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
        activityMyReviewsBinding.reviewList.setLayoutManager(linearLayoutManager);
        activityMyReviewsBinding.reviewList.setAdapter(reviewAdapter);
    }

}
