package com.application.elevated_shift.viewModels.activitesViewModels;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.text.Spannable;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.adapters.MyStoriesAdapter;
import com.application.elevated_shift.databinding.ActivityMyStoriesBinding;
import com.application.elevated_shift.databinding.ActivityMyStoryMainBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.collection.CollectionsResponse;
import com.application.elevated_shift.models.collection.Datum;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.sharedPreference.SharedPreferencesEditor;
import com.application.elevated_shift.ui.activities.MyStoryMainActivity;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;

public class MyStoriesMainViewModel {

    public static MyStoriesMainViewModel instance;
    MyStoryMainActivity mActivity;
    ActivityMyStoryMainBinding activityMyStoriesBinding;
    public CollectionsResponse collectionsResponse = new CollectionsResponse();
    MyStoriesAdapter storiesAdapter;

    public MyStoriesMainViewModel(MyStoryMainActivity mActivity, ActivityMyStoryMainBinding activityMyStoriesBinding) {
        instance = this;
        this.mActivity = mActivity;
        this.activityMyStoriesBinding = activityMyStoriesBinding;

        initViews();
    }

    private void initViews() {


        activityMyStoriesBinding.btnBack.setOnClickListener(v -> mActivity.onBackPressed());

        activityMyStoriesBinding.btnAdd.setOnClickListener(v -> {
            showDialog();
        });

        if (!new SharedPreferencesEditor(mActivity).isTutorialViewed(Constants.COLLECTION_ADD)) {
            showTutorials();
        } else
            setAdapter();
    }

    private void showTutorials() {

        new GuideView.Builder(mActivity)
                .setTitle("Add")
                .setContentSpan((Spannable) Html.fromHtml("<font color='#0B304F'>Tap on plus icon to add new collection</p>"))
                .setGravity(Gravity.center)
                .setTargetView(activityMyStoriesBinding.btnAdd)
                .setDismissType(DismissType.outside)
                .setGuideListener(new GuideListener() {
                    @Override
                    public void onDismiss(View view) {
                        new SharedPreferencesEditor(mActivity).setTutorialViewed(Constants.COLLECTION_ADD, true);
                        setAdapter();
                    }
                })
                .build()
                .show();

    }

    public void setAdapter() {

        new ServerHelper(mActivity).getCollections(new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (collectionsResponse.getData() == null)
                    return;
                List<Datum> list = new ArrayList<>();
                for (Datum datum : collectionsResponse.getData()) {
                    if (datum.getUser().equals(Globals.getUsage().sharedPreferencesEditor.getUser().getId()))
                        list.add(datum);
                }
                if (list.size() == 0) {
                    activityMyStoriesBinding.count.setText("0");
                    activityMyStoriesBinding.noStory.setVisibility(View.VISIBLE);
                    activityMyStoriesBinding.storiesList.setVisibility(View.GONE);
                } else {

                    activityMyStoriesBinding.count.setText(list.size() + "");
                    activityMyStoriesBinding.noStory.setVisibility(View.GONE);
                    activityMyStoriesBinding.storiesList.setVisibility(View.VISIBLE);
                }

                storiesAdapter = new MyStoriesAdapter(list, mActivity);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
                activityMyStoriesBinding.storiesList.setLayoutManager(linearLayoutManager);
                activityMyStoriesBinding.storiesList.setAdapter(storiesAdapter);
                activityMyStoriesBinding.noStory.requestFocus();
            }
        });
    }

    private void showDialog() {
        final Dialog d = new Dialog(mActivity);
        d.setContentView(R.layout.dialog_add_story_collection);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setCancelable(false);
        d.setCanceledOnTouchOutside(false);

        WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();
        lWindowParams.copyFrom(d.getWindow().getAttributes());
        lWindowParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        d.show();
        d.getWindow().setAttributes(lWindowParams);

        TextView post, cancel;
        EditText title;

        title = d.findViewById(R.id.title);
        post = d.findViewById(R.id.okay);
        cancel = d.findViewById(R.id.cancel);

        post.setOnClickListener(v -> {

            if (title.length() == 0) {
                Toast.makeText(mActivity, "Please enter title to add collection", Toast.LENGTH_SHORT).show();
                return;
            }

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("title", title.getText().toString());

            new ServerHelper(mActivity).saveCollection(jsonObject, new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        setAdapter();
                    }
                }
            });

            d.dismiss();
        });

        cancel.setOnClickListener(v -> {
            d.dismiss();
        });
    }

}
