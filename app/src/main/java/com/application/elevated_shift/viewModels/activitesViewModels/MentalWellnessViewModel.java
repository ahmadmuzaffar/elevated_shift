package com.application.elevated_shift.viewModels.activitesViewModels;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityMentalWellnessBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.models.sentiment.SentimentResponse;
import com.application.elevated_shift.models.stress.Datum;
import com.application.elevated_shift.models.stress.StressResponse;
import com.application.elevated_shift.models.weight.WeightResponse;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.ui.activities.MentalWellnessActivity;
import com.application.elevated_shift.ui.activities.ReportActivity;
import com.application.elevated_shift.utils.Utilities;
import com.google.gson.JsonObject;
import com.rtugeek.android.colorseekbar.ColorSeekBar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

public class MentalWellnessViewModel {

    public static MentalWellnessViewModel instance;
    public WeightResponse weightResponse = new WeightResponse();
    public SentimentResponse sentimentResponse = new SentimentResponse();
    public StressResponse stressResponse = new StressResponse();
    SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_DATE_APP);
    MentalWellnessActivity mActivity;
    ActivityMentalWellnessBinding activityMentalWellnessBinding;
    Calendar cal = Calendar.getInstance();
    String weekStartDate, weekEndDate;
    private int peace = 0;
    private int insightful = 0;
    private int satisfaction = 0;
    private int anxiety = 0;
    private int worryness = 0;
    private int depression = 0;
    private int fear = 0;
    private int disgust = 0;
    private boolean todayWeightAvailable = false;
    private boolean todaymentalAvailable = false;
    private boolean todayStressAvailable = false;

    public MentalWellnessViewModel(MentalWellnessActivity mActivity, ActivityMentalWellnessBinding activityMentalWellnessBinding) {
        instance = this;
        this.mActivity = mActivity;
        this.activityMentalWellnessBinding = activityMentalWellnessBinding;

        initViews();
    }

    @SuppressLint("SimpleDateFormat")
    private void initViews() {

        cal.add(Calendar.DAY_OF_YEAR, -1);
        Calendar first = (Calendar) cal.clone();
        first.add(Calendar.DAY_OF_WEEK, first.getFirstDayOfWeek() - first.get(Calendar.DAY_OF_WEEK));
        weekStartDate = sdf.format(first.getTime());

        Calendar last = (Calendar) first.clone();
        last.add(Calendar.DAY_OF_YEAR, 6);
        weekEndDate = sdf.format(last.getTime());

        sdf.setTimeZone(TimeZone.getDefault());
        new ServerHelper(mActivity).getWeights(weekStartDate, weekEndDate, new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success) {

                    for (com.application.elevated_shift.models.weight.Datum datum : weightResponse.getData()) {
                        try {
                            Calendar calendar = Calendar.getInstance();
                            SimpleDateFormat df = new SimpleDateFormat(Constants.FORMAT_DATE_TIME, Locale.ENGLISH);
                            df.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date = null;

                            try {
                                date = df.parse(datum.getCreatedAt());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            df.setTimeZone(TimeZone.getDefault());
                            String formattedDate = df.format(Objects.requireNonNull(date));
                            calendar.setTime(Objects.requireNonNull(sdf.parse(formattedDate)));
                            if (Utilities.compareDates(sdf.parse(sdf.format(calendar.getTime())), sdf.parse(sdf.format(new Date()))) != 0) {
                                todayWeightAvailable = true;
                                break;
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

                    new ServerHelper(mActivity).getSentiments(weekStartDate, weekEndDate, new HandlerDataSync() {
                        @Override
                        public void onSyncSuccessful(boolean success, boolean isDataError) {
                            if (success) {

                                for (com.application.elevated_shift.models.sentiment.Datum datum : sentimentResponse.getData()) {
                                    try {
                                        Calendar calendar = Calendar.getInstance();
                                        SimpleDateFormat df = new SimpleDateFormat(Constants.FORMAT_DATE_TIME, Locale.ENGLISH);
                                        df.setTimeZone(TimeZone.getTimeZone("UTC"));
                                        Date date = null;

                                        try {
                                            date = df.parse(datum.getCreatedAt());
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        df.setTimeZone(TimeZone.getDefault());
                                        String formattedDate = df.format(Objects.requireNonNull(date));
                                        calendar.setTime(Objects.requireNonNull(sdf.parse(formattedDate)));
                                        if (Utilities.compareDates(sdf.parse(sdf.format(calendar.getTime())), sdf.parse(sdf.format(new Date()))) != 0) {

                                            todaymentalAvailable = true;

                                            activityMentalWellnessBinding.seekBarPeace.setProgress(datum.getPeace());
                                            activityMentalWellnessBinding.seekBarInsightful.setProgress(datum.getInsightful());
                                            activityMentalWellnessBinding.seekBarSatisfaction.setProgress(datum.getSatisfaction());
                                            activityMentalWellnessBinding.seekBarAnxiety.setProgress(datum.getAnxiety());
                                            activityMentalWellnessBinding.seekBarWorryness.setProgress(datum.getWorryness());
                                            activityMentalWellnessBinding.seekBarDepression.setProgress(datum.getDepression());
                                            activityMentalWellnessBinding.seekBarFear.setProgress(datum.getFear());
                                            activityMentalWellnessBinding.seekBarDisgust.setProgress(datum.getDisgust());

//                                            activityMentalWellnessBinding.seekBarPeace.setEnabled(false);
//                                            activityMentalWellnessBinding.seekBarInsightful.setEnabled(false);
//                                            activityMentalWellnessBinding.seekBarSatisfaction.setEnabled(false);
//                                            activityMentalWellnessBinding.seekBarAnxiety.setEnabled(false);
//                                            activityMentalWellnessBinding.seekBarWorryness.setEnabled(false);
//                                            activityMentalWellnessBinding.seekBarDepression.setEnabled(false);
//                                            activityMentalWellnessBinding.seekBarFear.setEnabled(false);
//                                            activityMentalWellnessBinding.seekBarDisgust.setEnabled(false);

                                            activityMentalWellnessBinding.btnAddMentalHealth.setVisibility(View.GONE);

                                            break;
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }

                                new ServerHelper(mActivity).getStress(weekStartDate, weekEndDate, new HandlerDataSync() {
                                    @Override
                                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                                        if (success) {
                                            for (Datum datum : stressResponse.getData()) {
                                                try {
                                                    Calendar calendar = Calendar.getInstance();
                                                    SimpleDateFormat df = new SimpleDateFormat(Constants.FORMAT_DATE_TIME, Locale.ENGLISH);
                                                    df.setTimeZone(TimeZone.getTimeZone("UTC"));
                                                    Date date = null;

                                                    try {
                                                        date = df.parse(datum.getCreatedAt());
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }

                                                    df.setTimeZone(TimeZone.getDefault());
                                                    String formattedDate = df.format(Objects.requireNonNull(date));
                                                    calendar.setTime(Objects.requireNonNull(sdf.parse(formattedDate)));
                                                    if (Utilities.compareDates(sdf.parse(sdf.format(calendar.getTime())), sdf.parse(sdf.format(new Date()))) != 0) {
                                                        activityMentalWellnessBinding.colorSlider.setColorBarPosition(datum.getValue());
//                                                        activityMentalWellnessBinding.colorSlider.setEnabled(false);
                                                        activityMentalWellnessBinding.btnAddStress.setVisibility(View.GONE);
                                                        todayStressAvailable = true;
                                                        break;
                                                    }
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });

        activityMentalWellnessBinding.colorSlider.setMaxPosition(100);

        activityMentalWellnessBinding.colorSlider.setShowAlphaBar(false);
        activityMentalWellnessBinding.colorSlider.setBarHeight(5); //5dpi
        activityMentalWellnessBinding.colorSlider.setThumbHeight(30); //30dpi
        activityMentalWellnessBinding.colorSlider.setBarMargin(10);

        activityMentalWellnessBinding.btnBack.setOnClickListener(v -> mActivity.onBackPressed());

        activityMentalWellnessBinding.colorSlider.setOnColorChangeListener(new ColorSeekBar.OnColorChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onColorChangeListener(int colorBarPosition, int alphaBarPosition, int color) {
                if (activityMentalWellnessBinding.stress.getVisibility() == View.GONE)
                    activityMentalWellnessBinding.stress.setVisibility(View.VISIBLE);
                if (!todayStressAvailable)
                    activityMentalWellnessBinding.stress.setText("" + colorBarPosition);
            }
        });

        activityMentalWellnessBinding.seekBarPeace.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!todaymentalAvailable) {
                    peace = progress;
                    double percent = (double) (peace + insightful + satisfaction + anxiety + worryness + depression + fear + disgust) / 8;
                    activityMentalWellnessBinding.mentalHealth.setText(percent + "");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        activityMentalWellnessBinding.seekBarInsightful.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!todaymentalAvailable) {
                    insightful = progress;
                    double percent = (double) (peace + insightful + satisfaction + anxiety + worryness + depression + fear + disgust) / 8;
                    activityMentalWellnessBinding.mentalHealth.setText(percent + "");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        activityMentalWellnessBinding.seekBarSatisfaction.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!todaymentalAvailable) {
                    satisfaction = progress;
                    double percent = (double) (peace + insightful + satisfaction + anxiety + worryness + depression + fear + disgust) / 8;
                    activityMentalWellnessBinding.mentalHealth.setText(percent + "");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        activityMentalWellnessBinding.seekBarAnxiety.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!todaymentalAvailable) {
                    anxiety = progress;
                    double percent = (double) (peace + insightful + satisfaction + anxiety + worryness + depression + fear + disgust) / 8;
                    activityMentalWellnessBinding.mentalHealth.setText(percent + "");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        activityMentalWellnessBinding.seekBarWorryness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!todaymentalAvailable) {
                    worryness = progress;
                    double percent = (double) (peace + insightful + satisfaction + anxiety + worryness + depression + fear + disgust) / 8;
                    activityMentalWellnessBinding.mentalHealth.setText(percent + "");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        activityMentalWellnessBinding.seekBarDepression.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!todaymentalAvailable) {
                    depression = progress;
                    double percent = (double) (peace + insightful + satisfaction + anxiety + worryness + depression + fear + disgust) / 8;
                    activityMentalWellnessBinding.mentalHealth.setText(percent + "");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        activityMentalWellnessBinding.seekBarFear.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!todaymentalAvailable) {
                    fear = progress;
                    double percent = (double) (peace + insightful + satisfaction + anxiety + worryness + depression + fear + disgust) / 8;
                    activityMentalWellnessBinding.mentalHealth.setText(percent + "");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        activityMentalWellnessBinding.seekBarDisgust.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!todaymentalAvailable) {
                    disgust = progress;
                    double percent = (double) (peace + insightful + satisfaction + anxiety + worryness + depression + fear + disgust) / 8;
                    activityMentalWellnessBinding.mentalHealth.setText(percent + "");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        activityMentalWellnessBinding.btnAddWeight.setOnClickListener(v -> {
            if (!todayWeightAvailable)
                showWeightDialog();
            else
                Toast.makeText(mActivity, "Weight already added for today", Toast.LENGTH_SHORT).show();
        });

        activityMentalWellnessBinding.btnAddStress.setOnClickListener(v -> {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("value", Integer.parseInt(activityMentalWellnessBinding.stress.getText().toString()));
            new ServerHelper(mActivity).addStress(jsonObject, new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        activityMentalWellnessBinding.btnAddStress.setVisibility(View.GONE);
//            activityMentalWellnessBinding.stress.setVisibility(View.GONE);
                        activityMentalWellnessBinding.colorSlider.setEnabled(false);
                    }
                }
            });

        });

        activityMentalWellnessBinding.btnAddMentalHealth.setOnClickListener(v -> {

            JsonObject jsonObjectv = new JsonObject();
            jsonObjectv.addProperty("peace", peace);
            jsonObjectv.addProperty("insightful", insightful);
            jsonObjectv.addProperty("satisfaction", satisfaction);
            jsonObjectv.addProperty("anxiety", anxiety);
            jsonObjectv.addProperty("worryness", worryness);
            jsonObjectv.addProperty("depression", depression);
            jsonObjectv.addProperty("fear", fear);
            jsonObjectv.addProperty("disgust", disgust);

            new ServerHelper(mActivity).addSentiments(jsonObjectv, new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        activityMentalWellnessBinding.btnAddMentalHealth.setVisibility(View.GONE);
                    }
                }
            });

        });

        activityMentalWellnessBinding.breatheSpinner.setVisibility(View.GONE);

        activityMentalWellnessBinding.btnPower.setOnClickListener(v -> {
            if (activityMentalWellnessBinding.breatheSpinner.getVisibility() == View.GONE) {
                activityMentalWellnessBinding.breatheSpinner.setVisibility(View.VISIBLE);
                activityMentalWellnessBinding.loader.setVisibility(View.GONE);
            } else {
                activityMentalWellnessBinding.breatheSpinner.setVisibility(View.GONE);
                activityMentalWellnessBinding.loader.setVisibility(View.VISIBLE);
            }
        });

        activityMentalWellnessBinding.btnReport.setOnClickListener(v -> {
            mActivity.startActivity(new Intent(mActivity, ReportActivity.class));
        });

    }

    public void showWeightDialog() {

        final Dialog d = new Dialog(mActivity);
        d.setContentView(R.layout.single_number_picker);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button b1 = d.findViewById(R.id.btn_set);
        Button b2 = d.findViewById(R.id.btn_cancel);
        final com.shawnlin.numberpicker.NumberPicker np = d.findViewById(R.id.np);
        np.setMaxValue(120);
        np.setMinValue(40);
        np.setValue(60);
        np.setWrapSelectorWheel(false);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("weight", np.getValue());
                new ServerHelper(mActivity).addWeight(jsonObject, new HandlerDataSync() {
                    @Override
                    public void onSyncSuccessful(boolean success, boolean isDataError) {
                        if (success) {
                            d.dismiss();
                        }
                    }
                });
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

}
