package com.application.elevated_shift.viewModels.fragmentsViewModels;

import android.app.Activity;
import android.content.Intent;

import com.application.elevated_shift.adapters.CoachAdapter;
import com.application.elevated_shift.databinding.FragmentCoachesBinding;
import com.application.elevated_shift.databinding.FragmentLoginBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.models.user.GetCoachesRespone;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.ui.activities.CoachCategoryActivity;

import androidx.recyclerview.widget.LinearLayoutManager;

public class CoachesViewModel {

    public static CoachesViewModel instance;
    FragmentCoachesBinding fragmentCoachesBinding;
    Activity mActivity;
    private CoachAdapter coachAdapter;
    public GetCoachesRespone getCoachesRespone = new GetCoachesRespone();

    public CoachesViewModel(FragmentCoachesBinding fragmentCoachesBinding, Activity mActivity) {
        instance = this;
        this.fragmentCoachesBinding = fragmentCoachesBinding;
        this.mActivity = mActivity;

        initViews();
    }

    private void initViews() {

        fragmentCoachesBinding.trendy.setOnClickListener(v -> {
            mActivity.startActivity(new Intent(mActivity, CoachCategoryActivity.class).putExtra("category", "Trendy"));
        });

        fragmentCoachesBinding.classic.setOnClickListener(v -> {
            mActivity.startActivity(new Intent(mActivity, CoachCategoryActivity.class).putExtra("category", "Classic"));
        });

        fragmentCoachesBinding.hit.setOnClickListener(v -> {
            mActivity.startActivity(new Intent(mActivity, CoachCategoryActivity.class).putExtra("category", "Hit"));
        });

        fragmentCoachesBinding.strict.setOnClickListener(v -> {
            mActivity.startActivity(new Intent(mActivity, CoachCategoryActivity.class).putExtra("category", "Strict"));
        });

        fragmentCoachesBinding.prePlanned.setOnClickListener(v -> {
            mActivity.startActivity(new Intent(mActivity, CoachCategoryActivity.class).putExtra("category", "Pre-Planned"));
        });

        fragmentCoachesBinding.masters.setOnClickListener(v -> {
            mActivity.startActivity(new Intent(mActivity, CoachCategoryActivity.class).putExtra("category", "Masters"));
        });

        new ServerHelper(mActivity).getCoaches(new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success) {
                    setAdapter();
                }
            }
        });

    }

    private void setAdapter() {
        coachAdapter = new CoachAdapter(getCoachesRespone.getData(), mActivity);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        fragmentCoachesBinding.topCoachesList.setLayoutManager(linearLayoutManager);
        fragmentCoachesBinding.topCoachesList.setAdapter(coachAdapter);
    }
}
