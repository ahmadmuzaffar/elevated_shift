package com.application.elevated_shift.viewModels.activitesViewModels;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.application.elevated_shift.adapters.SearchedCoachAdapter;
import com.application.elevated_shift.adapters.SearchedUserAdapter;
import com.application.elevated_shift.databinding.ActivitySearchViewBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.models.user.GetCoachesRespone;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.ui.activities.SearchViewActivity;

import java.util.Collections;
import java.util.Comparator;

import androidx.recyclerview.widget.LinearLayoutManager;

public class SearchViewViewModel {

    public static SearchViewViewModel instance;
    public GetCoachesRespone getCoachesRespone;
    SearchViewActivity mActivity;
    ActivitySearchViewBinding activitySearchViewBinding;
    SearchedUserAdapter searchedUserAdapter;
    private SearchedCoachAdapter searchedCoachAdapter;
    private boolean[] check;

    public SearchViewViewModel(SearchViewActivity mActivity, ActivitySearchViewBinding activitySearchViewBinding) {
        instance = this;
        this.mActivity = mActivity;
        this.activitySearchViewBinding = activitySearchViewBinding;

        initViews();
    }

    private void initViews() {

        activitySearchViewBinding.searchBar.requestFocus();
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        activitySearchViewBinding.btnBack.setOnClickListener(v -> {
            mActivity.onBackPressed();
        });

        activitySearchViewBinding.searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    activitySearchViewBinding.listArea.setVisibility(View.GONE);
                } else
                    performSearch();
            }
        });

//        activitySearchViewBinding.searchBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//
//                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                    Globals.hideKeyboard(mActivity);
//                    performSearch();
//                    return true;
//                }
//
//                return false;
//            }
//        });

    }

    private void performSearch() {
        new ServerHelper(mActivity).searchUsers(activitySearchViewBinding.searchBar.getText().toString(), new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success) {
                    setAdapters();
                }
            }
        });
    }

    private void setAdapters() {

        if (getCoachesRespone != null && getCoachesRespone.getData().size() > 0) {

            activitySearchViewBinding.listArea.setVisibility(View.VISIBLE);
            activitySearchViewBinding.noResult.setVisibility(View.GONE);
            Collections.sort(getCoachesRespone.getData(), new Comparator<Data>() {
                @Override
                public int compare(Data o1, Data o2) {
                    return Boolean.compare(o1.getCoach(), o2.getCoach());
                }
            });

            searchedUserAdapter = new SearchedUserAdapter(getCoachesRespone.getData());
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
            activitySearchViewBinding.userList.setLayoutManager(linearLayoutManager);
            activitySearchViewBinding.userList.setAdapter(searchedUserAdapter);

        } else {
            activitySearchViewBinding.noResult.setVisibility(View.VISIBLE);
            activitySearchViewBinding.listArea.setVisibility(View.GONE);
        }

    }
}
