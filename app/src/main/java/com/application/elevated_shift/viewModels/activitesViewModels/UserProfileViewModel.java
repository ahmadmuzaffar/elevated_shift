package com.application.elevated_shift.viewModels.activitesViewModels;

import android.text.TextUtils;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityUserProfileBinding;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.ui.activities.UserProfileActivity;
import com.application.elevated_shift.ui.fragments.userProfileFragments.AccountFragment;
import com.application.elevated_shift.ui.fragments.userProfileFragments.CoachesFragment;
import com.application.elevated_shift.ui.fragments.userProfileFragments.ConnectionsFragment;
import com.squareup.picasso.Picasso;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class UserProfileViewModel {

    UserProfileActivity mActivity;
    ActivityUserProfileBinding activityStartBinding;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private AccountFragment accountFragment;
    private CoachesFragment coachesFragment;
    private ConnectionsFragment connectionsFragment;
    private String activeTab = "account";

    public UserProfileViewModel(UserProfileActivity mActivity, ActivityUserProfileBinding activityStartBinding) {
        this.mActivity = mActivity;
        this.activityStartBinding = activityStartBinding;

        initViews();
    }

    public void onResume() {
        Data user = Globals.getUsage().sharedPreferencesEditor.getUser();
        activityStartBinding.tvUsername.setText(user.getName());
        if (!TextUtils.isEmpty(user.getDisplayImage())) {
            Picasso.with(activityStartBinding.dp.getContext())
                    .load(user.getDisplayImage())
                    .placeholder(R.drawable.placeholder)
                    .centerCrop()
                    .resize(200, 200)
                    .into(activityStartBinding.dp);
        } else {
            activityStartBinding.dp.setImageDrawable(activityStartBinding.dp.getContext().getResources().getDrawable(R.drawable.placeholder));
        }
    }

    private void initViews() {

        activityStartBinding.btnBack.setOnClickListener(v -> mActivity.onBackPressed());

        openAccountFragment();
        activityStartBinding.tabAccount.setOnClickListener(v -> {
            if (!activeTab.equals("account")) {
                activityStartBinding.tabAccount.setBackgroundResource(R.drawable.tab_bottom);
                activityStartBinding.tabCoaches.setBackground(null);
                activityStartBinding.tabConnections.setBackground(null);
                mActivity.onBackPressed();
            }
        });
        activityStartBinding.tabCoaches.setOnClickListener(v -> {
            if (!activeTab.equals("coach")) {
                activityStartBinding.tabAccount.setBackground(null);
                activityStartBinding.tabCoaches.setBackgroundResource(R.drawable.tab_bottom);
                activityStartBinding.tabConnections.setBackground(null);
                openCoachesFragment();
            }
        });
        activityStartBinding.tabConnections.setOnClickListener(v -> {
            if (!activeTab.equals("connection")) {
                activityStartBinding.tabAccount.setBackground(null);
                activityStartBinding.tabCoaches.setBackground(null);
                activityStartBinding.tabConnections.setBackgroundResource(R.drawable.tab_bottom);
                openConnectionFragment();
            }
        });

    }

    public void openAccountFragment() {
        if (accountFragment == null) {
            accountFragment = new AccountFragment();
        }
        fragmentManager = mActivity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentBox, accountFragment, "");
        fragmentTransaction.commit();
        activeTab = "account";
    }

    private void openCoachesFragment() {
        if (coachesFragment == null) {
            coachesFragment = new CoachesFragment();
        }
        fragmentManager = mActivity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentBox, coachesFragment, "");
        fragmentTransaction.commit();
        activeTab = "coach";

    }

    private void openConnectionFragment() {
        if (connectionsFragment == null) {
            connectionsFragment = new ConnectionsFragment();
        }
        fragmentManager = mActivity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentBox, connectionsFragment, "");
        fragmentTransaction.commit();
        activeTab = "connection";

    }

}
