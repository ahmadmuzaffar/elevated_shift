package com.application.elevated_shift.viewModels.activitesViewModels;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.text.Spannable;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.adapters.GoalAdapter;
import com.application.elevated_shift.databinding.ActivityMyGoalsBinding;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Constants;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.Goal;
import com.application.elevated_shift.models.goal.Datum;
import com.application.elevated_shift.models.goal.GoalResponse;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.serverRetrofit.ServerHelper;
import com.application.elevated_shift.sharedPreference.SharedPreferencesEditor;
import com.application.elevated_shift.ui.activities.MyGoalsActivity;
import com.application.elevated_shift.ui.activities.MyPerformanceActivity;
import com.application.elevated_shift.utils.Utilities;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.recyclerview.widget.LinearLayoutManager;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;

public class MyGoalsViewModel {

    public static MyGoalsViewModel instance;
    MyGoalsActivity mActivity;
    ActivityMyGoalsBinding activityMyGoalsBinding;
    public GoalResponse goalResponse = new GoalResponse();
    GoalAdapter goalAdapter;

    public MyGoalsViewModel(MyGoalsActivity mActivity, ActivityMyGoalsBinding activityMyGoalsBinding) {
        instance = this;
        this.mActivity = mActivity;
        this.activityMyGoalsBinding = activityMyGoalsBinding;

        initViews();
    }

    public void onResume() {
        Data user = Globals.getUsage().sharedPreferencesEditor.getUser();
        activityMyGoalsBinding.tvUsername.setText(user.getName());
        Picasso.with(activityMyGoalsBinding.dp.getContext())
                .load(user.getDisplayImage())
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .resize(200, 200)
                .into(activityMyGoalsBinding.dp);
    }

    private void initViews() {

        activityMyGoalsBinding.btnBack.setOnClickListener(v -> mActivity.onBackPressed());

        activityMyGoalsBinding.btnAddGoal.setOnClickListener(v -> {
            showDialog();
        });

        activityMyGoalsBinding.dp.setOnClickListener(v -> {
            mActivity.startActivity(new Intent(mActivity, MyPerformanceActivity.class));
        });

        if (!new SharedPreferencesEditor(mActivity).isTutorialViewed(Constants.GOALS_ADD))
            showTutorials();
        else
            setAdapter();

    }

    private void showTutorials() {

        new GuideView.Builder(mActivity)
                .setTitle("Add")
                .setContentSpan((Spannable) Html.fromHtml("<font color='#0B304F'>Tap on plus icon to add new story</p>"))
                .setGravity(Gravity.center)
                .setTargetView(activityMyGoalsBinding.btnAddGoal)
                .setDismissType(DismissType.outside)
                .setGuideListener(new GuideListener() {
                    @Override
                    public void onDismiss(View view) {
                        new SharedPreferencesEditor(mActivity).setTutorialViewed(Constants.GOALS_ADD, true);
                        new GuideView.Builder(mActivity)
                                .setTitle("Goal Details")
                                .setContentSpan((Spannable) Html.fromHtml("<font color='#0B304F'>Tap on Image to view goals details</p>"))
                                .setGravity(Gravity.center)
                                .setTargetView(activityMyGoalsBinding.dp)
                                .setDismissType(DismissType.outside)
                                .setGuideListener(new GuideListener() {
                                    @Override
                                    public void onDismiss(View view) {
                                        new SharedPreferencesEditor(mActivity).setTutorialViewed(Constants.GOALS_DETAILS, true);
                                        setAdapter();
                                    }
                                })
                                .build()
                                .show();
                    }
                })
                .build()
                .show();

    }

    private void showDialog() {
        final Dialog d = new Dialog(mActivity);
        d.setContentView(R.layout.dialog_add_goal);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setCancelable(false);
        d.setCanceledOnTouchOutside(false);

        WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();
        lWindowParams.copyFrom(d.getWindow().getAttributes());
        lWindowParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        d.show();
        d.getWindow().setAttributes(lWindowParams);

        TextView post, cancel;
        EditText message, date, time, endTime;

        message = d.findViewById(R.id.message);
        date = d.findViewById(R.id.startDate);
        time = d.findViewById(R.id.endDate);
        post = d.findViewById(R.id.okay);
        endTime = d.findViewById(R.id.endTime);
        cancel = d.findViewById(R.id.cancel);

        date.setText(new SimpleDateFormat(Constants.FORMAT_DATE_API).format(new Date()));
        date.setOnClickListener(v -> {
            Utilities.openCalender(mActivity, date, null, "", Constants.FORMAT_DATE_API);
        });

        time.setOnClickListener(v -> {
            Utilities.openCalender(mActivity, time, null, "", Constants.FORMAT_DATE_API);
        });

        endTime.setOnClickListener(v -> {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(mActivity, new TimePickerDialog.OnTimeSetListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    endTime.setText("" + selectedHour + ":" + selectedMinute);
                }
            }, hour, minute, true);//Yes 24 hour time
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();
        });

        post.setOnClickListener(v -> {

            if (message.length() == 0) {
                Toast.makeText(mActivity, "Please enter goal name to post goal", Toast.LENGTH_SHORT).show();
                return;
            }
            if (date.length() == 0) {
                Toast.makeText(mActivity, "Please select start date to post goal", Toast.LENGTH_SHORT).show();
                return;
            }
            if (time.length() == 0) {
                Toast.makeText(mActivity, "Please select end date to post goal", Toast.LENGTH_SHORT).show();
                return;
            }
            if (endTime.length() == 0) {
                Toast.makeText(mActivity, "Please select end time to post goal", Toast.LENGTH_SHORT).show();
                return;
            }

            Goal goal = new Goal();
            goal.setGoalName(message.getText().toString());
            goal.setStartDateTime(date.getText().toString());
            goal.setEndDateTime(time.getText().toString());
            goal.setEndTime(endTime.getText().toString());
            goal.setStatus("In Progress");

            new ServerHelper(mActivity).saveGoal(goal, new HandlerDataSync() {
                @Override
                public void onSyncSuccessful(boolean success, boolean isDataError) {
                    if (success) {
                        setAdapter();
                    }
                }
            });

            d.dismiss();
        });

        cancel.setOnClickListener(v -> {
            d.dismiss();
        });
    }

    public void setAdapter() {

        new ServerHelper(mActivity).getGoals(new HandlerDataSync() {
            @Override
            public void onSyncSuccessful(boolean success, boolean isDataError) {
                if (success) {

                    if (checkForOverDue()) {
                        new ServerHelper(mActivity).getGoals(new HandlerDataSync() {
                            @Override
                            public void onSyncSuccessful(boolean success, boolean isDataError) {
                                if (goalResponse.getData().size() == 0) {
                                    activityMyGoalsBinding.noGoal.setVisibility(View.VISIBLE);
                                    activityMyGoalsBinding.goalsList.setVisibility(View.GONE);
                                } else {
                                    activityMyGoalsBinding.noGoal.setVisibility(View.GONE);
                                    activityMyGoalsBinding.goalsList.setVisibility(View.VISIBLE);
                                }

                                goalAdapter = new GoalAdapter(goalResponse.getData(), mActivity);
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
                                activityMyGoalsBinding.goalsList.setLayoutManager(linearLayoutManager);
                                activityMyGoalsBinding.goalsList.setAdapter(goalAdapter);
                            }
                        });
                        return;
                    }

                    if (goalResponse.getData().size() == 0) {
                        activityMyGoalsBinding.noGoal.setVisibility(View.VISIBLE);
                        activityMyGoalsBinding.goalsList.setVisibility(View.GONE);
                    } else {
                        activityMyGoalsBinding.noGoal.setVisibility(View.GONE);
                        activityMyGoalsBinding.goalsList.setVisibility(View.VISIBLE);
                    }

                    goalAdapter = new GoalAdapter(goalResponse.getData(), mActivity);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
                    activityMyGoalsBinding.goalsList.setLayoutManager(linearLayoutManager);
                    activityMyGoalsBinding.goalsList.setAdapter(goalAdapter);
                    onResume();
                }
            }
        });
    }

    @SuppressLint("SimpleDateFormat")
    private boolean checkForOverDue() {
        boolean isOverDue = false;
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_DATE_API);
        for (Datum datum : goalResponse.getData()) {
            try {
                if (Utilities.compareDates(sdf.parse(datum.getEndDateTime()), sdf.parse(sdf.format(new Date()))) == 1 && datum.getStatus().equals("In Progress")) {
                    isOverDue = true;
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("status", "Overdue");
                    new ServerHelper(mActivity).updateGoals(jsonObject, datum.getId(), new HandlerDataSync() {
                        @Override
                        public void onSyncSuccessful(boolean success, boolean isDataError) {
                            if (success) {
//                                setAdapter();
                            }
                        }
                    });
                } else if (Utilities.compareDates(sdf.parse(datum.getEndDateTime()), sdf.parse(sdf.format(new Date()))) == 0 && datum.getStatus().equals("In Progress")) {
                    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm");
                    String dateToCheck = datum.getEndDateTime() + " " + datum.getEndTime();
                    Date checking = format.parse(dateToCheck);
                    if (Utilities.compareDates(format.parse(format.format(new Date())), checking) == 1) {
                        isOverDue = true;
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("status", "Overdue");
                        new ServerHelper(mActivity).updateGoals(jsonObject, datum.getId(), new HandlerDataSync() {
                            @Override
                            public void onSyncSuccessful(boolean success, boolean isDataError) {
                                if (success) {
//                                setAdapter();
                                }
                            }
                        });
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return isOverDue;
    }
}
