package com.application.elevated_shift.viewModels.activitesViewModels;

import com.application.elevated_shift.R;
import com.application.elevated_shift.databinding.ActivityStartBinding;
import com.application.elevated_shift.ui.activities.StartActivity;
import com.application.elevated_shift.ui.fragments.startFragments.ForgotPasswordFragment;
import com.application.elevated_shift.ui.fragments.startFragments.LoginFragment;
import com.application.elevated_shift.ui.fragments.startFragments.SignUpFragment;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class StartViewModel {

    StartActivity mActivity;
    ActivityStartBinding activityStartBinding;
    private LoginFragment loginFragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private SignUpFragment signupFragment;
    private ForgotPasswordFragment forgotPasswordFragment;

    public StartViewModel(StartActivity mActivity, ActivityStartBinding activityStartBinding) {
        this.mActivity = mActivity;
        this.activityStartBinding = activityStartBinding;

        initViews();
    }

    private void initViews() {

        openLoginFragment();

    }

    public void openLoginFragment() {
        if (loginFragment == null) {
            loginFragment = new LoginFragment();
        }
        fragmentManager = mActivity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, loginFragment, "");
        fragmentTransaction.commit();
    }

    private void openSignUpFragment() {

        if (signupFragment == null) {
            signupFragment = new SignUpFragment();
        }
        fragmentManager = mActivity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, signupFragment, "");
        fragmentTransaction.commit();

    }

    private void openForgotPasswordFragment() {

        if (forgotPasswordFragment == null) {
            forgotPasswordFragment = new ForgotPasswordFragment();
        }
        fragmentManager = mActivity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, forgotPasswordFragment, "");
        fragmentTransaction.commit();

    }

    public void replaceFragment(int id) {
        if (id == 0) {
            openSignUpFragment();
        } else if (id == 1) {
            openForgotPasswordFragment();
        }
    }

}
