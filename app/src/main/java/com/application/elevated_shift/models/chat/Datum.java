package com.application.elevated_shift.models.chat;
import java.util.List;

import com.application.elevated_shift.models.user.Data;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("messages")
    @Expose
    private List<Message> messages = null;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("to")
    @Expose
    private Data to;
    @SerializedName("from")
    @Expose
    private Data from;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private Integer v;

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Data getTo() {
        return to;
    }

    public void setTo(Data to) {
        this.to = to;
    }

    public Data getFrom() {
        return from;
    }

    public void setFrom(Data from) {
        this.from = from;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

}
