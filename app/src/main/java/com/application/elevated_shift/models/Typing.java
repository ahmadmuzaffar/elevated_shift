package com.application.elevated_shift.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Typing {

    @SerializedName("typing")
    @Expose
    String typing;
    @SerializedName("id")
    @Expose
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTyping() {
        return typing;
    }

    public void setTyping(String typing) {
        this.typing = typing;
    }
}
