package com.application.elevated_shift.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClientIdResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("clientSecret")
    @Expose
    private String clientSecret;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

}
