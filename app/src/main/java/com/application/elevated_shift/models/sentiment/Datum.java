package com.application.elevated_shift.models.sentiment;

import com.application.elevated_shift.models.user.Data;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("peace")
    @Expose
    private Integer peace;
    @SerializedName("insightful")
    @Expose
    private Integer insightful;
    @SerializedName("satisfaction")
    @Expose
    private Integer satisfaction;
    @SerializedName("anxiety")
    @Expose
    private Integer anxiety;
    @SerializedName("worryness")
    @Expose
    private Integer worryness;
    @SerializedName("depression")
    @Expose
    private Integer depression;
    @SerializedName("fear")
    @Expose
    private Integer fear;
    @SerializedName("disgust")
    @Expose
    private Integer disgust;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("user")
    @Expose
    private Data user;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private Integer v;

    public Integer getPeace() {
        return peace;
    }

    public void setPeace(Integer peace) {
        this.peace = peace;
    }

    public Integer getInsightful() {
        return insightful;
    }

    public void setInsightful(Integer insightful) {
        this.insightful = insightful;
    }

    public Integer getSatisfaction() {
        return satisfaction;
    }

    public void setSatisfaction(Integer satisfaction) {
        this.satisfaction = satisfaction;
    }

    public Integer getAnxiety() {
        return anxiety;
    }

    public void setAnxiety(Integer anxiety) {
        this.anxiety = anxiety;
    }

    public Integer getWorryness() {
        return worryness;
    }

    public void setWorryness(Integer worryness) {
        this.worryness = worryness;
    }

    public Integer getDepression() {
        return depression;
    }

    public void setDepression(Integer depression) {
        this.depression = depression;
    }

    public Integer getFear() {
        return fear;
    }

    public void setFear(Integer fear) {
        this.fear = fear;
    }

    public Integer getDisgust() {
        return disgust;
    }

    public void setDisgust(Integer disgust) {
        this.disgust = disgust;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Data getUser() {
        return user;
    }

    public void setUser(Data user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

}