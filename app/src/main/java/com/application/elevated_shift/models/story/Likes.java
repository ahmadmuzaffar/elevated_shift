package com.application.elevated_shift.models.story;

import com.application.elevated_shift.models.user.Data;

import java.io.Serializable;

public class Likes implements Serializable {

    private Data likeTo;
    private Data likeFrom;
    private int itemPosition;

    public int getItemPosition() {
        return itemPosition;
    }

    public void setItemPosition(int itemPosition) {
        this.itemPosition = itemPosition;
    }

    public Data getLikeTo() {
        return likeTo;
    }

    public void setLikeTo(Data likeTo) {
        this.likeTo = likeTo;
    }

    public Data getLikeFrom() {
        return likeFrom;
    }

    public void setLikeFrom(Data likeFrom) {
        this.likeFrom = likeFrom;
    }
}
