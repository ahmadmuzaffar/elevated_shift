package com.application.elevated_shift.models.feedback;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SingleFeedback {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Datum data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Datum getData() {
        return data;
    }

    public void setData(Datum data) {
        this.data = data;
    }

}
