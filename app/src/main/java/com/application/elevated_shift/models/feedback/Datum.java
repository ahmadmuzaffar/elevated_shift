package com.application.elevated_shift.models.feedback;

import java.io.Serializable;
import java.util.List;

import com.application.elevated_shift.models.user.Data;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum implements Serializable {

    @SerializedName("comments")
    @Expose
    private List<Comment> comments = null;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("feedbackGivenTo")
    @Expose
    private Data feedbackGivenTo;
    @SerializedName("feedbackGivenBy")
    @Expose
    private Data feedbackGivenBy;
    @SerializedName("feedbackImageUrl")
    @Expose
    private String feedbackImageUrl;
    @SerializedName("feedbackText")
    @Expose
    private String feedbackText;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private Integer v;

    public String getFeedbackImageUrl() {
        return feedbackImageUrl;
    }

    public void setFeedbackImageUrl(String feedbackImageUrl) {
        this.feedbackImageUrl = feedbackImageUrl;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Data getFeedbackGivenTo() {
        return feedbackGivenTo;
    }

    public void setFeedbackGivenTo(Data feedbackGivenTo) {
        this.feedbackGivenTo = feedbackGivenTo;
    }

    public Data getFeedbackGivenBy() {
        return feedbackGivenBy;
    }

    public void setFeedbackGivenBy(Data feedbackGivenBy) {
        this.feedbackGivenBy = feedbackGivenBy;
    }

    public String getFeedbackText() {
        return feedbackText;
    }

    public void setFeedbackText(String feedbackText) {
        this.feedbackText = feedbackText;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

}
