package com.application.elevated_shift.models;

import com.application.elevated_shift.models.user.Data;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Story {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("collectionId")
    @Expose
    private String collectionId;
    @SerializedName("user")
    @Expose
    private Data user;
    @SerializedName("storyText")
    @Expose
    private String storyText;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("likes")
    @Expose
    private List<String> likes;
    @SerializedName("shares")
    @Expose
    private List<String> shares;

    public List<String> getLikes() {
        return likes;
    }

    public void setLikes(List<String> likes) {
        this.likes = likes;
    }

    public List<String> getShares() {
        return shares;
    }

    public void setShares(List<String> shares) {
        this.shares = shares;
    }

    public String getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(String collectionId) {
        this.collectionId = collectionId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Data getUser() {
        return user;
    }

    public void setUser(Data user) {
        this.user = user;
    }

    public String getStoryText() {
        return storyText;
    }

    public void setStoryText(String storyText) {
        this.storyText = storyText;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}