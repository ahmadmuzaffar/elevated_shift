package com.application.elevated_shift.models.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Data implements Serializable {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("ratings")
    @Expose
    private Double ratings;
    @SerializedName("isVerified")
    @Expose
    private String isVerified;
    @SerializedName("achievements")
    @Expose
    private List<String> achievements = null;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("displayImage")
    @Expose
    private String displayImage;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("height")
    @Expose
    private String height;
    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("isCoach")
    @Expose
    private Boolean isCoach;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("servicesCharges")
    @Expose
    private Double servicesCharges;
    @SerializedName("subscribedCoaches")
    @Expose
    private List<String> subscribedCoaches = null;
    @SerializedName("coachHashes")
    @Expose
    private List<String> coachHashes = null;
    @SerializedName("socialLinks")
    @Expose
    private List<SocialLinks> socialLinks = null;
    private String password;

    public Boolean getCoach() {
        return isCoach;
    }

    public void setCoach(Boolean coach) {
        isCoach = coach;
    }

    public List<String> getCoachHashes() {
        return coachHashes;
    }

    public void setCoachHashes(List<String> coachHashes) {
        this.coachHashes = coachHashes;
    }

    public List<SocialLinks> getSocialLinks() {
        return socialLinks;
    }

    public void setSocialLinks(List<SocialLinks> socialLinks) {
        this.socialLinks = socialLinks;
    }

    public List<String> getSubscribedCoaches() {
        return subscribedCoaches;
    }

    public void setSubscribedCoaches(List<String> subscribedCoaches) {
        this.subscribedCoaches = subscribedCoaches;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getRatings() {
        return ratings;
    }

    public void setRatings(Double ratings) {
        this.ratings = ratings;
    }

    public String getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(String isVerified) {
        this.isVerified = isVerified;
    }

    public List<String> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<String> achievements) {
        this.achievements = achievements;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDisplayImage() {
        return displayImage;
    }

    public void setDisplayImage(String displayImage) {
        this.displayImage = displayImage;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getServicesCharges() {
        return servicesCharges;
    }

    public void setServicesCharges(Double servicesCharges) {
        this.servicesCharges = servicesCharges;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
