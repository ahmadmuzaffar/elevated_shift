package com.application.elevated_shift.services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import com.application.elevated_shift.R;
import com.application.elevated_shift.models.chat.Message;
import com.application.elevated_shift.models.feedback.Datum;
import com.application.elevated_shift.models.story.Likes;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.sharedPreference.SharedPreferencesEditor;
import com.application.elevated_shift.ui.activities.ChatActivity;
import com.application.elevated_shift.ui.activities.MyFeedbackActivity;
import com.application.elevated_shift.ui.activities.MyFeedbackDetailActivity;
import com.application.elevated_shift.ui.activities.MyNewsFeedActivity;
import com.application.elevated_shift.viewModels.activitesViewModels.ChatViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.MyFeedbackDetailViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.MyFeedbackViewModel;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

public class MessagesService extends Service {
    public int counter = 0;
    private Socket mSocket;

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            String messageString = args[0].toString();
            Message message = new Gson().fromJson(messageString, Message.class);

            ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
            ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
            if (!cn.getClassName().equals(ChatActivity.class.getName()))
                sendMessageNotification(getApplicationContext(), "1", message);
            else
                ChatViewModel.instance.setAdapter();
        }
    };
    private Emitter.Listener onComment = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            String messageString = args[0].toString();
            Datum feedback = new Gson().fromJson(messageString, Datum.class);
            SharedPreferencesEditor sharedPreferencesEditor = new SharedPreferencesEditor(getApplicationContext());

            ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
            ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

            if (!cn.getClassName().equals(MyFeedbackDetailActivity.class.getName()) && !feedback.getFeedbackGivenBy().getId().equals(sharedPreferencesEditor.getUser().getId()) && feedback.getFeedbackGivenTo().getId().equals(sharedPreferencesEditor.getUser().getId()))
                sendCommentNotification(getApplicationContext(), "1", feedback);
            else
                MyFeedbackDetailViewModel.instance.setAdapter(feedback.getComments());
        }
    };
    private Emitter.Listener onFeedback = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            String messageString = args[0].toString();
            Datum feedback = new Gson().fromJson(messageString, Datum.class);
            SharedPreferencesEditor sharedPreferencesEditor = new SharedPreferencesEditor(getApplicationContext());

            ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
            ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

            if (!cn.getClassName().equals(MyFeedbackActivity.class.getName()) && !feedback.getFeedbackGivenBy().getId().equals(sharedPreferencesEditor.getUser().getId()) && feedback.getFeedbackGivenTo().getId().equals(sharedPreferencesEditor.getUser().getId()))
                sendFeedbackNotification(getApplicationContext(), "1", feedback);
            else
                MyFeedbackViewModel.instance.setAdapter();

        }
    };
    private Emitter.Listener onLike = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            String messageString = args[0].toString();
            Likes likes = new Gson().fromJson(messageString, Likes.class);
            SharedPreferencesEditor sharedPreferencesEditor = new SharedPreferencesEditor(getApplicationContext());

            if (!likes.getLikeFrom().getId().equals(sharedPreferencesEditor.getUser().getId()) && likes.getLikeTo().getId().equals(sharedPreferencesEditor.getUser().getId()))
                sendLikeNotification(getApplicationContext(), "1", likes, "Liked");
        }
    };

    private Emitter.Listener onShare = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            String messageString = args[0].toString();
            Likes likes = new Gson().fromJson(messageString, Likes.class);
            SharedPreferencesEditor sharedPreferencesEditor = new SharedPreferencesEditor(getApplicationContext());

            if (!likes.getLikeFrom().getId().equals(sharedPreferencesEditor.getUser().getId()) && likes.getLikeTo().getId().equals(sharedPreferencesEditor.getUser().getId()))
                sendLikeNotification(getApplicationContext(), "1", likes, "Shared");
        }
    };
    private InputStream in;

    private void sendCommentNotification(Context applicationContext, String s, Datum feedback) {
        NotificationManager notificationManagerCompat = (NotificationManager) applicationContext.getSystemService(NOTIFICATION_SERVICE);

        String CHANNEL_ID = "comments";
        CharSequence name = "comments_room";
        int importance = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            importance = NotificationManager.IMPORTANCE_DEFAULT;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManagerCompat.createNotificationChannel(mChannel);
        }

        Intent requestsViewIntent = new Intent(getBaseContext(), MyFeedbackDetailActivity.class);
        requestsViewIntent.putExtra("feedback", feedback);
        PendingIntent requestsViewPending = PendingIntent.getActivity(getBaseContext(), Integer.valueOf(s), requestsViewIntent, 0);

        Notification notification = getNotification(feedback, CHANNEL_ID, requestsViewPending);
        notificationManagerCompat.notify(Integer.valueOf(s), notification);
    }

    private void sendFeedbackNotification(Context applicationContext, String s, Datum feedback) {
        NotificationManager notificationManagerCompat = (NotificationManager) applicationContext.getSystemService(NOTIFICATION_SERVICE);

        String CHANNEL_ID = "feedbacks";
        CharSequence name = "feedbacks_room";
        int importance = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            importance = NotificationManager.IMPORTANCE_DEFAULT;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManagerCompat.createNotificationChannel(mChannel);
        }

        Intent requestsViewIntent = new Intent(getBaseContext(), MyFeedbackActivity.class);
        PendingIntent requestsViewPending = PendingIntent.getActivity(getBaseContext(), Integer.valueOf(s), requestsViewIntent, 0);

        Notification notification = getNotification(feedback, CHANNEL_ID, requestsViewPending);
        notificationManagerCompat.notify(Integer.valueOf(s), notification);
    }

    private void sendLikeNotification(Context applicationContext, String s, Likes user, String event) {
        NotificationManager notificationManagerCompat = (NotificationManager) applicationContext.getSystemService(NOTIFICATION_SERVICE);

        String CHANNEL_ID = "likes";
        CharSequence name = "likes_room";
        int importance = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            importance = NotificationManager.IMPORTANCE_DEFAULT;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManagerCompat.createNotificationChannel(mChannel);
        }

        Intent requestsViewIntent = new Intent(getBaseContext(), MyNewsFeedActivity.class);
//                .putExtra("position", user);
        PendingIntent requestsViewPending = PendingIntent.getActivity(getBaseContext(), Integer.valueOf(s), requestsViewIntent, 0);

        Notification notification = getNotification(user.getLikeFrom(), event, CHANNEL_ID, requestsViewPending);
        notificationManagerCompat.notify(Integer.valueOf(s), notification);
    }

    private void sendMessageNotification(Context context, String uniqueId, Message message) {

        NotificationManager notificationManagerCompat = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        String CHANNEL_ID = "chat";
        CharSequence name = "chat_room";
        int importance = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            importance = NotificationManager.IMPORTANCE_DEFAULT;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManagerCompat.createNotificationChannel(mChannel);
        }

        Intent requestsViewIntent = new Intent(getBaseContext(), ChatActivity.class)
                .putExtra("chatId", message.getChatId());
        PendingIntent requestsViewPending = PendingIntent.getActivity(getBaseContext(), Integer.valueOf(uniqueId), requestsViewIntent, 0);

        Notification notification = getNotification(message, CHANNEL_ID, requestsViewPending);
        notificationManagerCompat.notify(Integer.valueOf(uniqueId), notification);
    }

    private Notification getNotification(Data user, String event, String CHANNEL_ID, PendingIntent requestsViewPending) {
        Notification notification;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setContentTitle("Story " + event);
        builder.setContentText(user.getName() + event.toLowerCase() + " your story");
        builder.setChannelId(CHANNEL_ID);
        builder.setDefaults(android.app.Notification.DEFAULT_ALL);
        builder.setContentIntent(requestsViewPending);
        builder.setSmallIcon(R.mipmap.app_logo_main);
        builder.setShowWhen(true);
        notification = builder.build();
        notification.flags = Notification.FLAG_AUTO_CANCEL;

        return notification;
    }

    private Notification getNotification(Message message, String CHANNEL_ID, PendingIntent requestsViewPending) {
        Notification notification;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setContentTitle(message.getUser().getFirstName() + " " + message.getUser().getLastName());
        if (message.getType().equals("text"))
            builder.setContentText(message.getText());
        else {
            builder.setContentText("You have received an attachment");
            URL url = null;
            try {
                url = new URL(message.getText());
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                in = connection.getInputStream();
                builder.setLargeIcon(BitmapFactory.decodeStream(in));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        builder.setChannelId(CHANNEL_ID);
        builder.setDefaults(android.app.Notification.DEFAULT_ALL);
        builder.setContentIntent(requestsViewPending);
        builder.setSmallIcon(R.mipmap.app_logo_main);
        builder.setShowWhen(true);
        notification = builder.build();
        notification.flags = Notification.FLAG_AUTO_CANCEL;

        return notification;
    }

    private Notification getNotification(Datum feedback, String CHANNEL_ID, PendingIntent requestsViewPending) {
        Notification notification;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        if (CHANNEL_ID.equals("comments")) {
            builder.setContentTitle(feedback.getComments().get(feedback.getComments().size() - 1).getCommentor().getName() + " posted a comment");
            builder.setContentText(feedback.getComments().get(feedback.getComments().size() - 1).getCommentText());
        } else {
            builder.setContentTitle(feedback.getFeedbackGivenBy().getName() + " gave you a feedback");
            builder.setContentText(feedback.getFeedbackText());
            if (feedback.getFeedbackImageUrl() != null) {
                URL url = null;
                try {
                    url = new URL(feedback.getFeedbackImageUrl());
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    in = connection.getInputStream();
                    builder.setLargeIcon(BitmapFactory.decodeStream(in));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        builder.setChannelId(CHANNEL_ID);
        builder.setDefaults(android.app.Notification.DEFAULT_ALL);
        builder.setContentIntent(requestsViewPending);
        builder.setSmallIcon(R.drawable.ic_launcher_background);
        builder.setShowWhen(true);
        notification = builder.build();
        notification.flags = Notification.FLAG_AUTO_CANCEL;

        return notification;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        checkForNewMessage();
//        startTimer();
        return START_NOT_STICKY;
    }

    private void checkForNewMessage() {
        try {
            mSocket = IO.socket("https://gymbe.herokuapp.com/");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        mSocket.on("onNewMessage", onNewMessage);
        mSocket.on("onComment", onComment);
        mSocket.on("onFeedback", onFeedback);
        mSocket.on("onLike", onLike);
        mSocket.on("onShare", onShare);
        mSocket.connect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mSocket.off("onNewMessage", onNewMessage);
        mSocket.off("onComment", onComment);
        mSocket.off("onFeedback", onFeedback);
        mSocket.off("onLike", onLike);
        mSocket.off("onShare", onShare);
        mSocket.disconnect();
//        Intent broadcastIntent = new Intent(this, RestartService.class);
//        sendBroadcast(broadcastIntent);
        stoptimertask();
    }

    private Timer timer;
    private TimerTask timerTask;

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 1000, 1000); //
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                Log.i("in timer", "in timer ++++  " + (counter++));
            }
        };
    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

//    @Override
//    public void onTaskRemoved(Intent rootIntent) {
//
//        if (Globals.isOnline(this)) {
//            Intent dialogIntent = new Intent(this, RestartServiceActivity.class);
//            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            dialogIntent.putExtra("method", "restart");
//            startActivity(dialogIntent);
//        }
//
//        super.onTaskRemoved(rootIntent);
//    }
}
