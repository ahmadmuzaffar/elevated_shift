package com.application.elevated_shift.interfaces;

public interface CallBackFragment {
    void changeFragment(int id);
}
