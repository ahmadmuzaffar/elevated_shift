package com.application.elevated_shift.helperClasses;

public class Constants {

    public static final String CHAT_SEARCH = "chat_search";
    public static final String CHAT_ITEMS = "chat_items";
    public static final String COLLECTION_ITEMS = "collection_items";
    public static final String COLLECTION_ADD = "collection_add";
    public static final String STORIES_ITEM = "stories_items";
    public static final String STORIES_ADD = "stories_add";
    public static final String GOALS_ITEM = "goals_items";
    public static final String GOALS_ADD = "goals_add";
    public static final String GOALS_DETAILS = "goals_details";
    public static final String MENU_SHOW = "menu_show";
    public static final String ACCOUNT_UPDATE = "account_update";

    public static final String FORMAT_TIME_APP = "HH:mm:ss";
    public static final String FORMAT_TIME = "HH:mm a";
    public static final String FORMAT_TIME_DATA = "HH:mm:ss.SSS'Z'";
    public static final String FORMAT_DATE_MONTH = "MM/yyyy";
    public static final String FORMAT_DATE_APP = "yyyy-MM-dd";
    public static final String FORMAT_DATE_API = "MM/dd/yyyy";
    public static final String FORMAT_DATE_TIME = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String VIEW_FORMAT_DATE_TIME = "dd-MM-yyyy hh:mm a";
    public static final String CALENDAR_FORMAT_DATE_TIME = "E d MMMM, yyyy";

}
