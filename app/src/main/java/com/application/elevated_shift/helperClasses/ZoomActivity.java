package com.application.elevated_shift.helperClasses;

import android.os.Bundle;

import com.application.elevated_shift.utils.Zoom;

import androidx.appcompat.app.AppCompatActivity;

public class ZoomActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new Zoom(this, getIntent().getStringExtra("image")));
    }
}
