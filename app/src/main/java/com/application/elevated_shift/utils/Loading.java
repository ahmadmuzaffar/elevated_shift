package com.application.elevated_shift.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;


import com.application.elevated_shift.R;

import dmax.dialog.SpotsDialog;

public class Loading {
    private static AlertDialog spotsDialog = null;

    public static boolean isVisible = false;

    public static void show(Context context, boolean cancelable, String message) {
        cancel();
        try {
            spotsDialog = new SpotsDialog.Builder()
                    .setContext(context)
                    .setTheme(R.style.Custom).build();
            spotsDialog.setTitle(message);
            spotsDialog.setCancelable(cancelable);
            if (!isVisible) {
                isVisible = true;
                spotsDialog.show();
            }
        } catch (Exception e) {
            Log.d("Loading", "show");
        }
    }

    public static void cancel() {
        try {
            if (spotsDialog != null && spotsDialog.isShowing()) {
                isVisible = false;
                spotsDialog.dismiss();
                spotsDialog.cancel();
            }else{
                isVisible = false;
            }
        } catch (Exception e) {
            Log.d("Loading", "cancel");
        }
    }

    public static void updateMessage(final String message) {
        Runnable changeMessage = new Runnable() {
            @Override
            public void run() {
                spotsDialog.setMessage(message);
            }
        };
        changeMessage.run();
    }

}
