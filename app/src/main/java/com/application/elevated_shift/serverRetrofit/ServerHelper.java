package com.application.elevated_shift.serverRetrofit;

import android.content.Context;
import android.widget.Toast;

import com.application.elevated_shift.R;
import com.application.elevated_shift.adapters.CommentsAdapter;
import com.application.elevated_shift.adapters.NewsFeedAdapter;
import com.application.elevated_shift.handler.HandlerDataSync;
import com.application.elevated_shift.helperClasses.Globals;
import com.application.elevated_shift.models.ClientIdResponse;
import com.application.elevated_shift.models.Goal;
import com.application.elevated_shift.models.Review;
import com.application.elevated_shift.models.Story;
import com.application.elevated_shift.models.chat.ChatBody;
import com.application.elevated_shift.models.chat.ChatResponse;
import com.application.elevated_shift.models.chat.GetChatResponse;
import com.application.elevated_shift.models.chat.GetChatsResponse;
import com.application.elevated_shift.models.chat.Message;
import com.application.elevated_shift.models.collection.CollectionResponse;
import com.application.elevated_shift.models.collection.CollectionsResponse;
import com.application.elevated_shift.models.feedback.Comment;
import com.application.elevated_shift.models.feedback.CommentResponse;
import com.application.elevated_shift.models.feedback.Datum;
import com.application.elevated_shift.models.feedback.FeedbackResponse;
import com.application.elevated_shift.models.feedback.SingleFeedback;
import com.application.elevated_shift.models.gallery.GalleryResponse;
import com.application.elevated_shift.models.goal.GoalResponse;
import com.application.elevated_shift.models.payment.InvoicesRespone;
import com.application.elevated_shift.models.payment.PaymentResponse;
import com.application.elevated_shift.models.review.ReviewResponse;
import com.application.elevated_shift.models.sentiment.SentimentResponse;
import com.application.elevated_shift.models.story.SingleStory;
import com.application.elevated_shift.models.story.StoryResponse;
import com.application.elevated_shift.models.stress.StressResponse;
import com.application.elevated_shift.models.user.ChangePasswordBody;
import com.application.elevated_shift.models.user.Data;
import com.application.elevated_shift.models.user.ForgotPasswordBody;
import com.application.elevated_shift.models.user.GeneralResponse;
import com.application.elevated_shift.models.user.GetCoachesRespone;
import com.application.elevated_shift.models.user.GetUserResponse;
import com.application.elevated_shift.models.user.LoginBody;
import com.application.elevated_shift.models.user.SignupResponse;
import com.application.elevated_shift.models.user.SocialLinks;
import com.application.elevated_shift.models.user.User;
import com.application.elevated_shift.models.weight.WeightResponse;
import com.application.elevated_shift.ui.activities.CheckoutActivity;
import com.application.elevated_shift.utils.Loading;
import com.application.elevated_shift.viewModels.activitesViewModels.AllChatsViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.ChatViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.CoachCategoryViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.CoachProfileViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.MentalWellnessViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.MyAchievementViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.MyFeedbackDetailViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.MyFeedbackViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.MyGoalsViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.MyNewsFeedViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.MyPaymentsViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.MyPerformanceViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.MyReviewsViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.MyStoriesMainViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.MyStoriesViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.ProfileViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.ReportViewModel;
import com.application.elevated_shift.viewModels.activitesViewModels.SearchViewViewModel;
import com.application.elevated_shift.viewModels.fragmentsViewModels.AccountViewModel;
import com.application.elevated_shift.viewModels.fragmentsViewModels.CoachesViewModel;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ServerHelper {

    private Context mContext;

    public ServerHelper(Context mContext) {
        this.mContext = mContext;
    }

    public void signup(User user, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().signup(user)
                .enqueue(new Callback<SignupResponse>() {
                    @Override
                    public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                        if (response.isSuccessful()) {
                            Data data = response.body().getData();
                            Globals.getUsage().sharedPreferencesEditor.setUser(data);
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            if (response.code() == 409)
                                Toast.makeText(mContext, "Email Address already registered", Toast.LENGTH_SHORT).show();
                            else
                                handlerDataSync.onSyncSuccessful(false, true);
                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<SignupResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void login(LoginBody body, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().login(body)
                .enqueue(new Callback<SignupResponse>() {
                    @Override
                    public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                        if (response.isSuccessful()) {
                            Data data = response.body().getData();
                            Globals.getUsage().sharedPreferencesEditor.setUser(data);
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            if (response.code() == 409)
                                Toast.makeText(mContext, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                            else
                                handlerDataSync.onSyncSuccessful(false, true);
                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<SignupResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void getUser(String userId, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getUser("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), userId)
                .enqueue(new Callback<GetUserResponse>() {
                    @Override
                    public void onResponse(Call<GetUserResponse> call, Response<GetUserResponse> response) {
                        if (response.isSuccessful()) {
                            if (AccountViewModel.instance != null)
                                AccountViewModel.instance.data = response.body().getData();
                            if (CommentsAdapter.instance != null)
                                CommentsAdapter.instance.data = response.body().getData();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GetUserResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void getUsers(HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getUsers("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken())
                .enqueue(new Callback<GetCoachesRespone>() {
                    @Override
                    public void onResponse(Call<GetCoachesRespone> call, Response<GetCoachesRespone> response) {
                        if (response.isSuccessful()) {
                            GetCoachesRespone respone = response.body();
                            Globals.getUsage().sharedPreferencesEditor.setAllUser(respone.getData());
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GetCoachesRespone> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message) + t.getMessage(), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void getCoaches(HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getCoaches("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken())
                .enqueue(new Callback<GetCoachesRespone>() {
                    @Override
                    public void onResponse(Call<GetCoachesRespone> call, Response<GetCoachesRespone> response) {
                        if (response.isSuccessful()) {
                            if (CoachesViewModel.instance != null)
                                CoachesViewModel.instance.getCoachesRespone = response.body();
                            if (CoachCategoryViewModel.instance != null)
                                CoachCategoryViewModel.instance.getCoachesRespone = response.body();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GetCoachesRespone> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message) + t.getMessage(), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void searchUsers(String query, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().searchUsers("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), query)
                .enqueue(new Callback<GetCoachesRespone>() {
                    @Override
                    public void onResponse(Call<GetCoachesRespone> call, Response<GetCoachesRespone> response) {
                        if (response.isSuccessful()) {
                            if (SearchViewViewModel.instance != null)
                                SearchViewViewModel.instance.getCoachesRespone = response.body();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GetCoachesRespone> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message) + t.getMessage(), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void updateUser(JsonObject body, String userId, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().updateUser("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), body, userId)
                .enqueue(new Callback<SignupResponse>() {
                    @Override
                    public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                        if (response.isSuccessful()) {
                            Data data = response.body().getData();
                            data.setPassword(Globals.getUsage().sharedPreferencesEditor.getUser().getPassword());
                            Globals.getUsage().sharedPreferencesEditor.setUser(data);
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<SignupResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void updateUser(HashMap<String, List<String>> body, String userId, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().updateUser("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), body, userId)
                .enqueue(new Callback<SignupResponse>() {
                    @Override
                    public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                        if (response.isSuccessful()) {
                            Data data = response.body().getData();
                            data.setPassword(Globals.getUsage().sharedPreferencesEditor.getUser().getPassword());
                            Globals.getUsage().sharedPreferencesEditor.setUser(data);
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<SignupResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void updateUserLinks(HashMap<String, List<SocialLinks>> body, String userId, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().updateUserLinks("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), body, userId)
                .enqueue(new Callback<SignupResponse>() {
                    @Override
                    public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                        if (response.isSuccessful()) {
                            Data data = response.body().getData();
                            data.setPassword(Globals.getUsage().sharedPreferencesEditor.getUser().getPassword());
                            Globals.getUsage().sharedPreferencesEditor.setUser(data);
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<SignupResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void updateGoals(JsonObject body, String goalId, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().updateGoals("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), body, goalId)
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void forgotPassword(ForgotPasswordBody email, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().forgotPassword(email)
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void changePassword(ChangePasswordBody body, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().changePassword(body)
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void getChats(HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getChats("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken())
                .enqueue(new Callback<GetChatsResponse>() {
                    @Override
                    public void onResponse(Call<GetChatsResponse> call, Response<GetChatsResponse> response) {
                        if (response.isSuccessful()) {
                            if (AllChatsViewModel.instance != null)
                                AllChatsViewModel.instance.chatsList = response.body().getData();
                            if (CoachProfileViewModel.instance != null)
                                CoachProfileViewModel.instance.chatList = response.body().getData();
                            if (ProfileViewModel.instance != null)
                                ProfileViewModel.instance.chatsList = response.body().getData();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GetChatsResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void saveMessage(Message message, HandlerDataSync handlerDataSync) {
//        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().saveMessage("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), message)
                .enqueue(new Callback<ChatResponse>() {
                    @Override
                    public void onResponse(Call<ChatResponse> call, Response<ChatResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
//                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<ChatResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
//                        Loading.cancel();
                    }
                });
    }

    public void getChat(String chatId, HandlerDataSync handlerDataSync) {
//        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getChat("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), chatId)
                .enqueue(new Callback<GetChatResponse>() {
                    @Override
                    public void onResponse(Call<GetChatResponse> call, Response<GetChatResponse> response) {
                        if (response.isSuccessful()) {
                            ChatViewModel.instance.getChatResponse = response.body();
//                            ChatViewModel.instance.messageList = response.body().getData().getMessages();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
//                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GetChatResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
//                        Loading.cancel();
                    }
                });
    }

    public void getClientId(JsonObject body, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getClientId("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), body)
                .enqueue(new Callback<ClientIdResponse>() {
                    @Override
                    public void onResponse(Call<ClientIdResponse> call, Response<ClientIdResponse> response) {
                        if (response.isSuccessful()) {
                            CheckoutActivity.instance.clientIdResponse = response.body();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<ClientIdResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void createChat(ChatBody chatBody, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().createChat("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), chatBody)
                .enqueue(new Callback<ChatResponse>() {
                    @Override
                    public void onResponse(Call<ChatResponse> call, Response<ChatResponse> response) {
                        if (response.isSuccessful()) {
                            if (CoachProfileViewModel.instance != null)
                                CoachProfileViewModel.instance.chatResponse = response.body();
                            if (ProfileViewModel.instance != null)
                                ProfileViewModel.instance.chatResponse = response.body();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<ChatResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void savePayment(JsonObject body, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().savePayment("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), body)
                .enqueue(new Callback<PaymentResponse>() {
                    @Override
                    public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<PaymentResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void getInvoices(HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getInvoices("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken())
                .enqueue(new Callback<InvoicesRespone>() {
                    @Override
                    public void onResponse(Call<InvoicesRespone> call, Response<InvoicesRespone> response) {
                        if (response.isSuccessful()) {
                            MyPaymentsViewModel.instance.invoicesRespone = response.body();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<InvoicesRespone> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void deleteChat(String chatId, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().deleteChat("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), chatId)
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void saveStory(Story body, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().saveStory("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), body)
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void updateStory(HashMap<String, List<String>> body, String storyId, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().updateStory("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), body, storyId)
                .enqueue(new Callback<SignupResponse>() {
                    @Override
                    public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);
                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<SignupResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void saveCollection(JsonObject body, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().saveCollection("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), body)
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void getStories(String collectionId, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getCollection("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), collectionId)
                .enqueue(new Callback<CollectionResponse>() {
                    @Override
                    public void onResponse(Call<CollectionResponse> call, Response<CollectionResponse> response) {
                        if (response.isSuccessful()) {
                            MyStoriesViewModel.instance.collectionResponse = response.body();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<CollectionResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void getStories(HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getStories("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken())
                .enqueue(new Callback<StoryResponse>() {
                    @Override
                    public void onResponse(Call<StoryResponse> call, Response<StoryResponse> response) {
                        if (response.isSuccessful()) {
                            if (MyNewsFeedViewModel.instance != null)
                                MyNewsFeedViewModel.instance.storyResponse = response.body();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<StoryResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void getStory(String storyId, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getStory("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), storyId)
                .enqueue(new Callback<SingleStory>() {
                    @Override
                    public void onResponse(Call<SingleStory> call, Response<SingleStory> response) {
                        if (response.isSuccessful()) {
                            if (NewsFeedAdapter.instance != null)
                                NewsFeedAdapter.instance.singleStory = response.body();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<SingleStory> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void getCollections(HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getCollections("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken())
                .enqueue(new Callback<CollectionsResponse>() {
                    @Override
                    public void onResponse(Call<CollectionsResponse> call, Response<CollectionsResponse> response) {
                        if (response.isSuccessful()) {
                            if (MyStoriesMainViewModel.instance != null)
                                MyStoriesMainViewModel.instance.collectionsResponse = response.body();
                            if (MyNewsFeedViewModel.instance != null)
                                MyNewsFeedViewModel.instance.collectionsResponse = response.body();
                            if (NewsFeedAdapter.instance != null)
                                NewsFeedAdapter.instance.collectionsResponse = response.body();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<CollectionsResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void deleteStory(String storyId, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().deleteStory("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), storyId)
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void deleteCollection(String collectionId, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().deleteCollection("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), collectionId)
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void saveGoal(Goal body, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().saveGoal("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), body)
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void getGoals(HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getGoals("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken())
                .enqueue(new Callback<GoalResponse>() {
                    @Override
                    public void onResponse(Call<GoalResponse> call, Response<GoalResponse> response) {
                        if (response.isSuccessful()) {
                            if (MyGoalsViewModel.instance != null)
                                MyGoalsViewModel.instance.goalResponse = response.body();
                            if (MyPerformanceViewModel.instance != null)
                                MyPerformanceViewModel.instance.goalResponse = response.body();
                            if (ReportViewModel.instance != null)
                                ReportViewModel.instance.goalResponse = response.body();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GoalResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void deleteGoal(String goalId, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().deleteGoal("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), goalId)
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void saveReview(Review body, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().saveReview("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), body)
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void getReviews(HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getReviews("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken())
                .enqueue(new Callback<ReviewResponse>() {
                    @Override
                    public void onResponse(Call<ReviewResponse> call, Response<ReviewResponse> response) {
                        if (response.isSuccessful()) {
                            if (MyReviewsViewModel.instance != null)
                                MyReviewsViewModel.instance.reviewResponse = response.body();
                            if (CoachProfileViewModel.instance != null)
                                CoachProfileViewModel.instance.reviewResponse = response.body();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<ReviewResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void deleteReview(HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().deleteReview("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken())
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void saveFeedback(Datum body, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().saveFeedback("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), body)
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void getFeedbacks(HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getFeedbacks("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken())
                .enqueue(new Callback<FeedbackResponse>() {
                    @Override
                    public void onResponse(Call<FeedbackResponse> call, Response<FeedbackResponse> response) {
                        if (response.isSuccessful()) {
                            MyFeedbackViewModel.instance.feedbackResponse = response.body();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<FeedbackResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void getFeedback(String feedbackId, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getFeedback("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), feedbackId)
                .enqueue(new Callback<SingleFeedback>() {
                    @Override
                    public void onResponse(Call<SingleFeedback> call, Response<SingleFeedback> response) {
                        if (response.isSuccessful()) {
                            MyFeedbackDetailViewModel.instance.singleFeedback = response.body();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<SingleFeedback> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void addComment(Comment body, String feedbackId, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().addComment("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), body, feedbackId)
                .enqueue(new Callback<CommentResponse>() {
                    @Override
                    public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<CommentResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void updateFeedback(HashMap<String, List<Comment>> map, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().updateFeedback("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), map)
                .enqueue(new Callback<SignupResponse>() {
                    @Override
                    public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<SignupResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void deleteFeedback(HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().deleteFeedback("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken())
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void createGallery(HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().createGallery("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken())
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void addPicture(JsonObject body, String userId, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().addPicture("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), body, userId)
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void getGallery(HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getGallery("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken())
                .enqueue(new Callback<GalleryResponse>() {
                    @Override
                    public void onResponse(Call<GalleryResponse> call, Response<GalleryResponse> response) {
                        if (response.isSuccessful()) {
                            if (CoachProfileViewModel.instance != null)
                                CoachProfileViewModel.instance.galleryResponse = response.body();
                            if (MyAchievementViewModel.instance != null)
                                MyAchievementViewModel.instance.galleryResponse = response.body();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GalleryResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void getWeights(String from, String to, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getWeights("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), from, to)
                .enqueue(new Callback<WeightResponse>() {
                    @Override
                    public void onResponse(Call<WeightResponse> call, Response<WeightResponse> response) {
                        if (response.isSuccessful()) {
                            if (MentalWellnessViewModel.instance != null)
                                MentalWellnessViewModel.instance.weightResponse = response.body();
                            if (ReportViewModel.instance != null)
                                ReportViewModel.instance.weightResponse = response.body();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<WeightResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void addWeight(JsonObject body, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().addWeight("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), body)
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void getSentiments(String from, String to, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getSentiments("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), from, to)
                .enqueue(new Callback<SentimentResponse>() {
                    @Override
                    public void onResponse(Call<SentimentResponse> call, Response<SentimentResponse> response) {
                        if (response.isSuccessful()) {
                            if (MentalWellnessViewModel.instance != null)
                                MentalWellnessViewModel.instance.sentimentResponse = response.body();
                            if (ReportViewModel.instance != null)
                                ReportViewModel.instance.sentimentResponse = response.body();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<SentimentResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void addSentiments(JsonObject body, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().addSentiment("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), body)
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void getStress(String from, String to, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().getStress("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), from, to)
                .enqueue(new Callback<StressResponse>() {
                    @Override
                    public void onResponse(Call<StressResponse> call, Response<StressResponse> response) {
                        if (response.isSuccessful()) {
                            if (MentalWellnessViewModel.instance != null)
                                MentalWellnessViewModel.instance.stressResponse = response.body();
                            if (ReportViewModel.instance != null)
                                ReportViewModel.instance.stressResponse = response.body();
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<StressResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

    public void addStress(JsonObject body, HandlerDataSync handlerDataSync) {
        Loading.show(mContext, false, mContext.getString(R.string.pleasewait));

        ApiUtils.getAPIService().addStress("Bearer " + Globals.getUsage().sharedPreferencesEditor.getUser().getToken(), body)
                .enqueue(new Callback<GeneralResponse>() {
                    @Override
                    public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                        if (response.isSuccessful()) {
                            handlerDataSync.onSyncSuccessful(true, false);
                        } else {
                            handlerDataSync.onSyncSuccessful(false, true);

                        }
                        Loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<GeneralResponse> call, Throwable t) {
                        if (t instanceof IOException)
                            Toast.makeText(mContext, "Something went wrong\n" + mContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(mContext, mContext.getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                        handlerDataSync.onSyncSuccessful(false, true);
                        Loading.cancel();
                    }
                });
    }

}
