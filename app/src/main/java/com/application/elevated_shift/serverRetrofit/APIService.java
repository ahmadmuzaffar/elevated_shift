package com.application.elevated_shift.serverRetrofit;

import com.application.elevated_shift.models.ClientIdResponse;
import com.application.elevated_shift.models.Goal;
import com.application.elevated_shift.models.Review;
import com.application.elevated_shift.models.Story;
import com.application.elevated_shift.models.chat.ChatBody;
import com.application.elevated_shift.models.chat.ChatResponse;
import com.application.elevated_shift.models.chat.GetChatResponse;
import com.application.elevated_shift.models.chat.GetChatsResponse;
import com.application.elevated_shift.models.chat.Message;
import com.application.elevated_shift.models.collection.CollectionResponse;
import com.application.elevated_shift.models.collection.CollectionsResponse;
import com.application.elevated_shift.models.feedback.Comment;
import com.application.elevated_shift.models.feedback.CommentResponse;
import com.application.elevated_shift.models.feedback.Datum;
import com.application.elevated_shift.models.feedback.FeedbackResponse;
import com.application.elevated_shift.models.feedback.SingleFeedback;
import com.application.elevated_shift.models.gallery.GalleryResponse;
import com.application.elevated_shift.models.payment.InvoicesRespone;
import com.application.elevated_shift.models.payment.PaymentResponse;
import com.application.elevated_shift.models.goal.GoalResponse;
import com.application.elevated_shift.models.review.ReviewResponse;
import com.application.elevated_shift.models.sentiment.SentimentResponse;
import com.application.elevated_shift.models.story.SingleStory;
import com.application.elevated_shift.models.story.StoryResponse;
import com.application.elevated_shift.models.stress.StressResponse;
import com.application.elevated_shift.models.user.ChangePasswordBody;
import com.application.elevated_shift.models.user.ForgotPasswordBody;
import com.application.elevated_shift.models.user.GeneralResponse;
import com.application.elevated_shift.models.user.GetCoachesRespone;
import com.application.elevated_shift.models.user.GetUserResponse;
import com.application.elevated_shift.models.user.LoginBody;
import com.application.elevated_shift.models.user.SignupResponse;
import com.application.elevated_shift.models.user.SocialLinks;
import com.application.elevated_shift.models.user.User;
import com.application.elevated_shift.models.weight.WeightResponse;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {

    @POST("users")
    Call<SignupResponse> signup(@Body User body);

    @POST("users/login")
    Call<SignupResponse> login(@Body LoginBody body);

    @GET("users/{userId}")
    Call<GetUserResponse> getUser(@Header("Authorization") String token, @Path("userId") String userId);

    @GET("users")
    Call<GetCoachesRespone> getUsers(@Header("Authorization") String token);

    @GET("users/search")
    Call<GetCoachesRespone> searchUsers(@Header("Authorization") String token, @Query("searchText") String searchText);

    @GET("users/coaches")
    Call<GetCoachesRespone> getCoaches(@Header("Authorization") String token);

    @PATCH("users/{userId}")
    Call<SignupResponse> updateUser(@Header("Authorization") String token, @Body JsonObject body, @Path("userId") String userId);

    @PATCH("users/{userId}")
    Call<SignupResponse> updateUser(@Header("Authorization") String token, @Body HashMap<String, List<String>> body, @Path("userId") String userId);

    @PATCH("users/{userId}")
    Call<SignupResponse> updateUserLinks(@Header("Authorization") String token, @Body HashMap<String, List<SocialLinks>> body, @Path("userId") String userId);

    @POST("users/forgotpassword")
    Call<GeneralResponse> forgotPassword(@Body ForgotPasswordBody email);

    @POST("users/changepassword")
    Call<GeneralResponse> changePassword(@Body ChangePasswordBody body);

    @POST("chats")
    Call<ChatResponse> createChat(@Header("Authorization") String token, @Body ChatBody body);

    @GET("chats")
    Call<GetChatsResponse> getChats(@Header("Authorization") String token);

    @GET("chats/{chatId}")
    Call<GetChatResponse> getChat(@Header("Authorization") String token, @Path("chatId") String chatId);

    @DELETE("chats/{chatId}")
    Call<GeneralResponse> deleteChat(@Header("Authorization") String token, @Path("chatId") String chatId);

    @POST("messages")
    Call<ChatResponse> saveMessage(@Header("Authorization") String token, @Body Message body);

    @POST("invoices/clientid")
    Call<ClientIdResponse> getClientId(@Header("Authorization") String token, @Body JsonObject body);

    @POST("invoices")
    Call<PaymentResponse> savePayment(@Header("Authorization") String token, @Body JsonObject body);

    @GET("invoices")
    Call<InvoicesRespone> getInvoices(@Header("Authorization") String token);

    @POST("stories")
    Call<GeneralResponse> saveStory(@Header("Authorization") String token, @Body Story body);

    @POST("collections")
    Call<GeneralResponse> saveCollection(@Header("Authorization") String token, @Body JsonObject body);

    @GET("collections/{collectionId}")
    Call<CollectionResponse> getCollection(@Header("Authorization") String token, @Path("collectionId") String collectionId);

    @GET("collections")
    Call<CollectionsResponse> getCollections(@Header("Authorization") String token);

    @GET("stories")
    Call<StoryResponse> getStories(@Header("Authorization") String token);

    @GET("stories/{storyId}")
    Call<SingleStory> getStory(@Header("Authorization") String token, @Path("storyId") String storyId);

    @PATCH("stories/{storyId}")
    Call<SignupResponse> updateStory(@Header("Authorization") String token, @Body HashMap<String, List<String>> body, @Path("storyId") String storyId);

    @DELETE("stories/{storyId}")
    Call<GeneralResponse> deleteStory(@Header("Authorization") String token, @Path("storyId") String storyId);

    @DELETE("collections/{collectionId}")
    Call<GeneralResponse> deleteCollection(@Header("Authorization") String token, @Path("collectionId") String collectionId);

    @POST("goals")
    Call<GeneralResponse> saveGoal(@Header("Authorization") String token, @Body Goal body);

    @GET("goals")
    Call<GoalResponse> getGoals(@Header("Authorization") String token);

    @PATCH("goals/{goalId}")
    Call<GeneralResponse> updateGoals(@Header("Authorization") String token, @Body JsonObject body, @Path("goalId") String goalId);

    @DELETE("goals/{goalId}")
    Call<GeneralResponse> deleteGoal(@Header("Authorization") String token, @Path("goalId") String goalId);

    @POST("reviews")
    Call<GeneralResponse> saveReview(@Header("Authorization") String token, @Body Review body);

    @GET("reviews")
    Call<ReviewResponse> getReviews(@Header("Authorization") String token);

    @DELETE("reviews")
    Call<GeneralResponse> deleteReview(@Header("Authorization") String token);

    @POST("feedback")
    Call<GeneralResponse> saveFeedback(@Header("Authorization") String token, @Body Datum body);

    @GET("feedback")
    Call<FeedbackResponse> getFeedbacks(@Header("Authorization") String token);

    @GET("feedback/{feedbackId}")
    Call<SingleFeedback> getFeedback(@Header("Authorization") String token, @Path("feedbackId") String feedbackId);

    @PATCH("feedback")
    Call<SignupResponse> updateFeedback(@Header("Authorization") String token, @Body HashMap<String, List<Comment>> map);

    @POST("feedback/comment/{feedbackId}")
    Call<CommentResponse> addComment(@Header("Authorization") String token, @Body Comment body, @Path("feedbackId") String feedbackId);

    @DELETE("feedback")
    Call<GeneralResponse> deleteFeedback(@Header("Authorization") String token);

    @POST("gallery")
    Call<GeneralResponse> createGallery(@Header("Authorization") String token);

    @PATCH("gallery/{userId}")
    Call<GeneralResponse> addPicture(@Header("Authorization") String token, @Body JsonObject body, @Path("userId") String userId);

    @GET("gallery")
    Call<GalleryResponse> getGallery(@Header("Authorization") String token);

    @GET("weight")
    Call<WeightResponse> getWeights(@Header("Authorization") String token, @Query("from") String to, @Query("to") String from);

    @POST("weight")
    Call<GeneralResponse> addWeight(@Header("Authorization") String token, @Body JsonObject body);

    @GET("sentiments")
    Call<SentimentResponse> getSentiments(@Header("Authorization") String token, @Query("from") String to, @Query("to") String from);

    @POST("sentiments")
    Call<GeneralResponse> addSentiment(@Header("Authorization") String token, @Body JsonObject body);

    @GET("stress")
    Call<StressResponse> getStress(@Header("Authorization") String token, @Query("from") String to, @Query("to") String from);

    @POST("stress")
    Call<GeneralResponse> addStress(@Header("Authorization") String token, @Body JsonObject body);

}
