#include <jni.h>
#include <string>


extern "C"
JNIEXPORT jstring
JNICALL
Java_com_application_elevated_1shift_ui_activities_SplashScreen_baseURL(
        JNIEnv *env,
        jclass clazz) {
    std::string url = "https://gymbe.herokuapp.com/api/";
    return env->NewStringUTF(url.c_str());
}
extern "C"
JNIEXPORT jstring
JNICALL
Java_com_application_elevated_1shift_ui_activities_SplashScreen_publishableKey(
        JNIEnv *env,
        jclass clazz) {
    std::string url = "pk_test_51HQP81GcnTohjOZFKOAKICcv7hF4rvDMqDGk1cU93b8O3YbSGcPjurYGAJv6vQyRkFOvUW3XstQG8cStSUmpX2dp00vYYbKnWU";
    return env->NewStringUTF(url.c_str());
}